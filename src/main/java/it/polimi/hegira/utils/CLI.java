/**
 * 
 */
package it.polimi.hegira.utils;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;

/**
 * Defines the parameters needed to launch the program.
 * @author Marco Scavuzzo
 *
 */
public class CLI {
	
	@Parameter(names = {"--src_database", "-ds"}, description = "The name of the source database",
			required = true, validateWith = DatabaseValidator.class)
	public String dbName_src;
	
	@Parameter(names = {"--dst_database", "-dd"}, description = "The name of the target database",
			required = true, validateWith = DatabaseValidator.class)
	public String dbName_dst;
	
	@Parameter(names = {"--zookeeper", "-z"}, description = "ZooKeeper cluster connect string <host>:<port>")
	public String zkConnectString = "localhost:2181";
	
	@Parameter(names = {"--cleanOffset", "-co"}, description = "Clean Kafka consumer offset")
	public boolean cleanOffset = false;
	
	@Parameter(names = {"--kafka", "-k"}, description = "Kafka brokers list <host1>:<port1>,<host2>:<port2>")
	public String kafkaString = "localhost:9092";
	
	@Parameter(names = {"--threads", "-t"}, description = "Maximum number of thread couples for processing user operations.",
			validateWith = PositiveIntegerValidator.class)
	public int thread_no = 1;
	
	@Parameter(names = {"--id", "-i"}, description = "The ongoing data migration id.",
			required=true, validateWith = PositiveIntegerValidator.class)
	public int migration_id = 1;
	
	@Parameter(names = {"--help","-h"}, help = true,
			description = "Prints this help screen")
    public boolean help = false;
	
	@Parameter(names = {"--dlmOff", "-nd"}, 
			description = "If set, it disable the distributed lock manager when processing single operations, i.e., operations not in txs. "
					+ "NOTICE that if the commit log doesn't properly handle ops on the same entity, by disabling the dlm, anomalies may occurr (non-serializable executions)!!")
	public boolean disableDlm = false;
	
	@Parameters(commandDescription = "Send positive or negative acknowledgments to remote applications issuing DMQs")
	public class SendDMQsResponseCommnad{
		public static final String commandName = "dmqsRes";

		@Parameter(names = {"--httpRes", "-hs"}, description = "Get HTTP responses for each DMQ ack sent to remote applications")
		public boolean getHttpResponse = false;
	}
}
