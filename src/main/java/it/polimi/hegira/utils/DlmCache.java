/**
 * 
 */
package it.polimi.hegira.utils;

import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author Marco Scavuzzo
 *
 */
public class DlmCache<K,V> {
	private ConcurrentHashMap<K, V> map;
	
	public DlmCache() {
		map = new ConcurrentHashMap<K, V>();
	}
	
	public DlmCache(int capacity) {
		map = new ConcurrentHashMap<K, V>(capacity);
	}
	
	/**
	 * Puts a lock in the cache. It assumes that if you were able to obtain it,
	 * then it must be unique. So any old value, for this Id is replaced with the new one.
	 * @param id The Id of the element
	 * @param mutex The lock on the given Id
	 */
	public void putLock(K id, V mutex){
		if(id==null || mutex==null)
			throw new IllegalArgumentException("Wrong parameters");
		map.put(id, mutex);
	}
	
	/**
	 * Simply removes a lock from the cache given its id.
	 * **It does NOT release the lock**
	 * If the lock is removed without releasing it, then it will be impossible to release it afterwards!
	 * @param id The Id
	 * @return Returns the lock just removed
	 */
	public V removeLock(K id){
		if(id==null)
			throw new IllegalArgumentException("Wrong parameter");
		return map.remove(id);
	}
	
	/**
	 * Returns a lock if present, without removing it from the cache.
	 * @param id The id
	 * @return The mutex if present, otherwise null.
	 */
	public V getLock(K id) {
		if(id==null)
			throw new IllegalArgumentException("Wrong parameter");
		
		return map.get(id);
	}
	
	public boolean containsLock(K id){
		if(id==null)
			throw new IllegalArgumentException("Wrong parameter");
		
		return map.containsKey(id);
	}
	
	public Set<K> getKeySet(){
		return map.keySet();
	}
}
