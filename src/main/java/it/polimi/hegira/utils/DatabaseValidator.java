/**
 * 
 */
package it.polimi.hegira.utils;

import com.beust.jcommander.IParameterValidator;
import com.beust.jcommander.ParameterException;

/**
 * Class validating the parameter dbName passed, in the command-line, 
 * to the entry class.
 * @author Marco Scavuzzo
 *
 */
public class DatabaseValidator implements IParameterValidator{

	@Override
	public void validate(String name, String value) throws ParameterException {
		switch(value){
			case "DATASTORE": break;
			case "TABLES" : break;
			case "VOID" : break;
			case "CASSANDRA" : break;
			case "HBASE" : break;
			default : 
				throw new ParameterException("Parameter "+name+" can be DATASTORE or TABLES");
		}
		
	}
}
