package it.polimi.hegira.utils;

public class DefaultErrors {
	public static String alreadyConnected = "Error 1: A connection is already in place. Disconnect first.";
	public static String connectionError = "Error 2: Unable to connect to the destination.";
	public static String notConnected = "Error 3: No active connection.";
	public static String credentialsStorageError = "Error 4: Unable to save credentials";
	public static String credentialsGetError = "Error 5: Credential not found";
	public static String credentialsDeleteError = "Error 6: Unable to delete credentials";
	public static String databaseNotSupported = "Error 7: One or all of the selected databases are not supported";
	public static String fewParameters = "Error 8: Too few parameters. Check the documentation";
	public static String queueError = "Error 9: Impossible to access the queue.";
	public static String queryError = "Error 10: Couldn't execute the query";
	
	
	private static String threadsInformation = "The maximum number of supported writing threads is %s. The application is going to use %s writing threads.";
	
	public static String getErrorNumber(String error){
		int start = error.lastIndexOf("Error ");
		int col = error.lastIndexOf(": ");
		return error.substring(start+6, col);
	}
	public static String getErrorMessage(String error){
		int col = error.lastIndexOf(": ");
		return error.substring(col+1);
	}
	
	public static String getThreadsInformation(int MAX_THREADS_NO){
		String replace = threadsInformation.replace("%s", ""+MAX_THREADS_NO);
		return replace;
	}
}
