package it.polimi.hegira.utils;

import java.util.LinkedHashMap;

/**
 * A simple cache implementation, based on LinkedHashMap (i.e., NOT SYNCHRONIZED!) 
 * and implementing a LRU eviction policy.
 * 
 * @author Marco Scavuzzo
 */
public class LRUCache<K, V> extends LinkedHashMap<K, V> {

	private static float defaultLoadFactor = 0.75f;
	private static int defaultInitialCapacity = 20;
	private int cacheSize = defaultInitialCapacity/2;
	
	/**
	 * 
	 */
	public LRUCache() {
		super(defaultInitialCapacity, defaultLoadFactor, true);
	}

	/**
	 * @param initialCapacity
	 */
	public LRUCache(int initialCapacity) {
		super(initialCapacity, defaultLoadFactor, true);
		this.cacheSize = initialCapacity/2;
	}


	/**
	 * @param initialCapacity
	 * @param loadFactor
	 */
	public LRUCache(int initialCapacity, float loadFactor) {
		super(initialCapacity, loadFactor, true);
		this.cacheSize = initialCapacity/2;
	}

	/**
	 * @param initialCapacity
	 * @param loadFactor
	 * @param cacheSize
	 */
	public LRUCache(int initialCapacity, float loadFactor, int cacheSize) {
		super(initialCapacity, loadFactor, true);
		this.cacheSize = cacheSize;
	}

	/**
	 * @return the cacheSize
	 */
	public int getCacheSize() {
		return cacheSize;
	}

	/**
	 * @param cacheSize the cacheSize to set
	 */
	public void setCacheSize(int cacheSize) {
		if(cacheSize>0)
			this.cacheSize = cacheSize;
	}
	
	@Override
	protected boolean removeEldestEntry(java.util.Map.Entry<K, V> eldest) {
		return size()>=cacheSize;
	}
	
}
