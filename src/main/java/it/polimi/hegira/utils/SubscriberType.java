/**
 * 
 */
package it.polimi.hegira.utils;

/**
 * @author Marco Scavuzzo
 *
 */
public enum SubscriberType {
	SOURCE, DESTINATION, SYNC_ONLY;
}
