package it.polimi.hegira.utils;

import java.util.List;

import org.apache.curator.CuratorZookeeperClient;
import org.apache.curator.RetryLoop;
import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.CuratorFrameworkFactory;
import org.apache.curator.retry.ExponentialBackoffRetry;
import org.apache.curator.utils.ZKPaths;
import org.apache.zookeeper.ZooKeeper;

/**
 * 
 * @author  Marco Scavuzzo
 *
 */
public class ZooKeeperUtils {
	public static void cleanOffsets(String connectString, String groupId, String topic) throws Exception{
		String path = "/consumers/"+groupId+"/offsets/"+topic;
		CuratorFramework client = CuratorFrameworkFactory.newClient(connectString,
				new ExponentialBackoffRetry(1000, 3));
		client.start();
		CuratorZookeeperClient zClient = client.getZookeeperClient();
		RetryLoop retryLoop = zClient.newRetryLoop();
		
		while ( retryLoop.shouldContinue() )
		{
		   try{
			   ZooKeeper zk = zClient.getZooKeeper();
			   List<String> children = ZKPaths.getSortedChildren(zk, path);
			   for(String child : children){
				   zk.setData(path+"/"+child,"0".getBytes(), -1);
				   System.out.println("Clearing data for path "+path+"/"+child);
			   }
		       // it's important to re-get the ZK instance as there may have been an error and the instance was re-created
		       zk = zClient.getZooKeeper();
		       retryLoop.markComplete();
		       if(zClient!=null){
		    	   zClient.close();
		    	   zClient=null;
		       }
		   }catch ( Exception e ){
		       retryLoop.takeException(e);
		   }
		}
		if(zClient!=null){
			zClient.close();
	    	zClient=null;
	    }
		if(client!=null)
			client.close();
			
	}
}
