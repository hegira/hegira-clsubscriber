/**
 * 
 */
package it.polimi.hegira.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Marco Scavuzzo
 *
 */
public class PropertiesManager {
	private transient static Logger log = LoggerFactory.getLogger(PropertiesManager.class);
	private static String defaultPath = System.getProperty("user.home")+File.separator+
			"hegira"+File.separator+
			"replication"+File.separator;
	
	/**
	 * Gets the value of a given property stored inside the given file.
	 * @param fileName the file name in the default path (user.home)
	 * @param property	The name of the property to retrieve.
	 * @return	The value associated to the given property name.
	 */
	public static String getProperty(String fileName, String property){
		Properties props = new Properties();
		InputStream isr = null;
		try {
			isr = new FileInputStream(createFileIfNotExists(defaultPath, fileName));	
			props.load(isr);
			return props.getProperty(property);
		} catch (IOException e) {
			log.error("{} - Error reading file {}.\nStack Trace:",
					Thread.currentThread().getName(),
					fileName != null ? defaultPath+fileName : "thath should be located in "+defaultPath,
					e);
		} catch (Exception e) {
			log.error("{} - Error reading file {}.\nStack Trace:",
					Thread.currentThread().getName(),
					fileName != null ? defaultPath+fileName : "thath should be located in "+defaultPath,
					e);
		} finally {
			try {
				if(isr!=null)
					isr.close();
			} catch (IOException e) {}
			props=null;
		}
		return null;
	}
	
	/**
	 * Gets several properties from the same file at the same time
	 * @param fileName the file name in the default path (user.home)
	 * @param properties The names of the properties to retrieve.
	 * @return The values associated to the given properties names.
	 */
	public static Map<String,String> getProperties(String fileName, String... properties){
		Properties props = new Properties();
		InputStream isr = null;
		try {
			isr = new FileInputStream(createFileIfNotExists(defaultPath, fileName));	
			props.load(isr);
			HashMap<String,String> map = new HashMap<String,String>();
			for(String pName : properties){
				String prop = props.getProperty(pName);
				if(prop!=null)
					map.put(pName, prop);
			}
			return map;
		} catch (IOException e) {
			log.error("{} - Error reading file {}.\nStack Trace:",
					Thread.currentThread().getName(),
					fileName != null ? defaultPath+fileName : "thath should be located in "+defaultPath,
					e);
		} catch (Exception e) {
			log.error("{} - Error reading file {}.\nStack Trace:",
					Thread.currentThread().getName(),
					fileName != null ? defaultPath+fileName : "thath should be located in "+defaultPath,
					e);
		} finally {
			try {
				if(isr!=null)
					isr.close();
			} catch (IOException e) {}
			props=null;
		}
		return null;
	}
	
	public static File createFileIfNotExists(String path, String fileName) 
			throws NullPointerException, IOException{
		if(fileName == null)
			throw new NullPointerException("fileName cannot be null");
		if(path==null)
			path=defaultPath;
		
		File file = new File(path+fileName);
		if (!file.getParentFile().exists())
		    file.getParentFile().mkdirs();
		if (!file.exists())
		    file.createNewFile();
		return file;
	}
	
	/**
	 * Gets a property stored in a property file within the same JAR package of this code.
	 * @param file The file name.
	 * @param property The property name to retrieve.
	 * @return The retrieved value associated with the given property name.
	 */
	private static String getPropertyFromJar(String file, String propertyKey){
		
		Properties props = new Properties();
		
		try {
			InputStream isr = PropertiesManager.class.getResourceAsStream("/"+file);
			props.load(isr);
			isr.close();
			return props.getProperty(propertyKey);
		} catch (FileNotFoundException | NullPointerException e) {
			log.error(file+" file must exist!");
		} catch (IOException e) {
			log.error("Unable to read file "+file+"!");
		} finally {
			props=null;
		}
		
		return null;
	}
	
	public static String getZKproperty(String propertyKey){
		return getProperty(Constants.ZK_PATH, propertyKey);
	}
	
	public static String getCredentialsProperty(String propertyKey){
		return getProperty(Constants.CREDENTIALS_PATH, propertyKey);
	}
	
	public static Properties getPropertiesFromJar(String file){
		Properties props = new Properties();
		
		try {
			InputStream isr = PropertiesManager.class.getResourceAsStream("/"+file);
			props.load(isr);
			isr.close();
			return props;
		} catch (FileNotFoundException | NullPointerException e) {
			log.error(file+" file must exist!",e);
		} catch (IOException e) {
			log.error("Unable to read file "+file+"!");
		} finally {
			props=null;
		}
		
		return null;
	}
	
	public static Properties getPropertiesFile(String fileName){
		Properties props = new Properties();
		InputStream isr = null;
		try {
			isr = new FileInputStream(createFileIfNotExists(defaultPath, fileName));	
			props.load(isr);
			return props;
		} catch (IOException e) {
			log.error("{} - Error reading file {}.\nStack Trace:",
					Thread.currentThread().getName(),
					fileName != null ? defaultPath+fileName : "thath should be located in "+defaultPath,
					e);
		} catch (Exception e) {
			log.error("{} - Error reading file {}.\nStack Trace:",
					Thread.currentThread().getName(),
					fileName != null ? defaultPath+fileName : "thath should be located in "+defaultPath,
					e);
		} finally {
			try {
				if(isr!=null)
					isr.close();
			} catch (IOException e) {}
			props=null;
		}
		return null;
	}
	
	/**
	 * Gets the ZooKeeper connect string from the credentials file.
	 * @return	ZooKeeper connect string (i.e., ip_address:port).
	 */
	public static String getZooKeeperConnectString(){
		return getProperty(Constants.CREDENTIALS_PATH, Constants.ZK_CONNECTSTRING);
	}
	
	public static String getHBaseQuorum(){
		return getProperty(Constants.HBASE_CONFIGURATION_FILE,
				"hbase.zookeeper.quorum");
	}
	
	public static String getHBaseProperty(String propertyName){
		return getProperty(Constants.HBASE_CONFIGURATION_FILE,
				propertyName);
	}
	
	public static org.apache.hadoop.hbase.client.Consistency getHBaseConsistencyType() {
		boolean isTimeline = Boolean.parseBoolean(getProperty(Constants.HBASE_CONFIGURATION_FILE,
				"consistency.timeline"));
		if (isTimeline)
			return org.apache.hadoop.hbase.client.Consistency.TIMELINE;
		else
			return org.apache.hadoop.hbase.client.Consistency.STRONG;
	}

	public static String getDefaultPath() {
		return defaultPath;
	}
}
