package it.polimi.hegira.utils;

import com.beust.jcommander.IParameterValidator;
import com.beust.jcommander.ParameterException;

public class PositiveIntegerValidator implements IParameterValidator {

	@Override
	public void validate(String name, String value) throws ParameterException {
		try{
			int longValue = Integer.parseInt(value);
			if(longValue<1)
				throw new ParameterException("Parameter "+name+" should be positive!");
		}catch(NumberFormatException e){
			throw new ParameterException("Parameter "+name+" should be positive number!");
		}
	}

}
