package it.polimi.hegira.utils;

import java.util.ArrayList;
import java.util.List;

/**
 * Class containing all the constants used throughout the project. 
 * @author Marco Scavuzzo
 */
public class Constants {
	
	/**
	* LOGS FILE PATH
	*/
	public static final String LOGS_PATH = "log.properties";
	
	/**
	 * ZOOKEEPER CONFIGURATION FILE PATH
	 */
	public static final String ZK_PATH = "zookeeper.properties";
	
	/**
	 * CREDENTIALS FILE PATH
	 */
	public static final String CREDENTIALS_PATH = "credentials.properties";
	
	/**
	 * CASSANDRA CONFIGURATION FILE PATH
	 */
	public static final String CASSANDRA_CONFIGURATION_FILE="cassandraConfiguration.properties";
	
	/**
	 * HBase CONFIGURATION FILE PATH
	 */
	public static final String HBASE_CONFIGURATION_FILE="hbaseConfiguration.properties";
	
	
	/**
	* STATUS RESPONSE
	*/
	public static final String STATUS_SUCCESS = "OK";
	public static final String STATUS_ERROR = "ERROR";
	public static final String STATUS_WARNING = "WARNING";
	
	/**
	* SUPPORTED DATABASE IDENTIFIERS
	*/
	public static final String GAE_DATASTORE="DATASTORE";
	public static final String AZURE_TABLES="TABLES";
	public static final String AMAZON_DYNAMODB="DYNAMODB";
	public static final String VOID="VOID";
	public static final String CASSANDRA="CASSANDRA";
	public static final String HBASE="HBASE";
	public static List<String> getSupportedDatabseList(){
		ArrayList<String> list = new ArrayList<String>();
		list.add(GAE_DATASTORE);
		list.add(AZURE_TABLES);
		list.add(AMAZON_DYNAMODB);
		list.add(VOID);
		list.add(HBASE);
		list.add(CASSANDRA);
		return list;
	}
	public static boolean isSupported(String database){
		List<String> supportedList = getSupportedDatabseList();
		return supportedList.contains(database);
	}
	public static List<String> getSupportedDBfromList(List<String> databases){
		List<String> supportedList = getSupportedDatabseList();
		databases.retainAll(supportedList);
		return databases;
	}
	
	/**
	* CREDENTIALS PROPERTIES
	* Properties names stored in CREDENTIALS FILE
	*/
	public static final String AZURE_PROP = "azure.storageConnectionString";
	
	//Deprecated: Old Api
	public static final String DATASTORE_USERNAME = "datastore.username";
	//Deprecated: Old Api
	public static final String DATASTORE_PASSWORD = "datastore.password";
	//Datastore Service Account Client ID
	public static final String DATASTORE_CLIENTID = "datastore.SAclientId";
	//Datastore Service Account Private Key file name "something.p12"
	public static final String DATASTORE_PKNAME = "datastore.pkname";
	public static final String DATASTORE_SERVER = "datastore.server";
	//lets give a chance (until completely deprecated) to use both login methods
	public static final String DATASTORE_LOGIN = "datastore.login";
	
	public static final String CASSANDRA_SERVER = "cassandra.server";
	public static final String CASSANDRA_USERNAME = "cassandra.username";
	public static final String CASSANDRA_PASSWORD = "cassandra.password";
	
	public static List<String> getSupportedCredentials(){
		ArrayList<String> list = new ArrayList<String>();
		list.add(AZURE_PROP);
		list.add(DATASTORE_USERNAME);
		list.add(DATASTORE_PASSWORD);
		list.add(DATASTORE_SERVER);
		list.add(CASSANDRA_SERVER);
		list.add(CASSANDRA_USERNAME);
		list.add(CASSANDRA_PASSWORD);
		return list;
	}
	
	/**
	 * CASSANDRA CONFIFURATION PROPERTIES
	 * Properties stored in the CASSANDRA CONFIGURATION file
	 */
	public static final String KEYSPACE = "cassandra.keyspace";
	public static final String READ_CONSISTENCY= "cassandra.readConsistency";
	public static final String PRIMARY_KEY_NAME="cassandra.primarKey";
	public static final String C_PRIMARY_KEY_TYPE="cassandra.primarKey.type";
	
	/**
	 * CASSANDRA CONSISTENCY LEVELS
	 */
	public static final String CONSISTENCY_EVENTUAL="eventual";
	public static final String CONSISTENCY_STRONG="strong";
	
	/**
	 * KAFKA CONSTANTS
	 */
	public static final String SYNC_TOPIC = "synch";
	public static enum CONSUMER_GROUPS {
		ISOLATION, hegira;
	}

	/**
	 * CASSANDRA DEFAULT TABLE NAME
	 * (used during the migration of data to Cassandra when the metamodel does NOT specify any column family)
	 */
	public static final String DEFAULT_TABLE_CASSANDRA="defaultTableCassandra";
	
	public static final String DEFAULT_COMPOUNDED_PK_SEPARATOR = "|Hgr|";

	public static final String ZK_CONNECTSTRING = "zookeeper.connectString";
	
	
	/**
	 * VOID DATABASE PROPERTIES
	 */
	public static final String VOID_MIN_PROPTIME_MS="void.minPropagationTime.ms";
	public static final String VOID_MAX_PROPTIME_MS="void.maxPropagationTime.ms";
	public static final String VOID_STORAGE_TYPE="void.storage";

	public static final String LOCK_REUSE = "vdp.lockreuse.number";

	public static enum voidAllowedStorages {NULL, MEMORY, FILE;};
}