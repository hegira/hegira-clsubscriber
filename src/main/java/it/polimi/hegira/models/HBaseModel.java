/**
 * Copyright 2015 Marco Scavuzzo
 * Contact: Marco Scavuzzo <marco.scavuzzo@polimi.it>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package it.polimi.hegira.models;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author Marco Scavuzzo
 *
 */
public class HBaseModel implements Iterable<HBaseModel.HBaseRow> {
	private String table;
	private Map<String, HBaseRow> rows;
	
	private static final Logger log = LoggerFactory.getLogger(HBaseModel.class);
	
	public HBaseModel() {
		initRows();
	}
	
	public HBaseModel(String table) {
		this.table = table;
		initRows();
	}
	
	public String getKey() {
		return table;
	}
	
	public void setKey(String key) {
		this.table = key;
	}
	
	public Map<String, HBaseRow> getRows() {
		return rows;
	}
	
	public void setRows(Map<String, HBaseRow> rows) {
		this.rows = rows;
	}
	
	public HBaseRow addRow(String rowKey) {
		return addRow(new HBaseRow(getKey(), rowKey));
	}
	
	public HBaseRow addRow(HBaseRow row) {
		rows.put(row.getKey(), row);
		return row;
	}
	
	public void initRows() {
		rows = new HashMap<String, HBaseRow>();
	}
	
	public static class HBaseRow implements Iterable<HBaseModel.HBaseColumn> {
		
		private String key;
		private String tableKey;
		private Map<String, HBaseColumn> columns;
		
		public HBaseRow() {
			initColumns();
		}

		public HBaseRow(String tableKey, String key) {
			this.tableKey = tableKey;
			this.key = key;
			initColumns();
		}

		public String getKey() {
			return key;
		}
		
		public void setKey(String key) {
			this.key = key;
		}
		
		public String getTableKey() {
			return tableKey;
		}
		
		public void setTableKey(String tableKey) {
			this.tableKey = tableKey;
		}
		
		public Map<String, HBaseColumn> getColumns() {
			return columns;
		}
		
		public void setColumns(Map<String, HBaseColumn> columns) {
			this.columns = columns;
		}
		
		public HBaseColumn addColumn(String columnFamily, String columnQualifier) {
			return addColumn(new HBaseColumn(getTableKey(), getKey(), columnFamily, columnQualifier));
		}
		
		public HBaseColumn addColumn(HBaseColumn column) {
			columns.put(column.getKey(), column);
			return column;
		}
		
		public void initColumns() {
			columns = new HashMap<String, HBaseColumn>();
		}
		
		public String toString() {
			return String.format("{ %s, -, -, - } [%d cells]", getKey(), size());
		}
		
		public int size() {
			int total = 0;
			for (String key : columns.keySet())
				total += columns.get(key).size();
			return total;
		}
		
		@Override
		public Iterator<HBaseColumn> iterator() {
			return getColumns().values().iterator();
		}

	}
	
	public static class HBaseColumn implements Iterable<HBaseCell> {
		
		private String rowKey;
		private String family;
		private String qualifier;
		private Map<Calendar, HBaseCell> cells;
		private String tableKey;
		
		public HBaseColumn() {
			initCells();
		}

		public HBaseColumn(String tableKey, String rowKey, String family, String qualifier) {
			this.tableKey = tableKey;
			this.rowKey = rowKey;
			this.family = family;
			this.qualifier = qualifier;
			initCells();
		}

		public String getRowKey() {
			return rowKey;
		}
		
		public void setRowKey(String rowKey) {
			this.rowKey = rowKey;
		}
		
		public String getTableKey() {
			return tableKey;
		}
		
		public void setTableKey(String tableKey) {
			this.tableKey = tableKey;
		}
		
		public String getFamily() {
			return family;
		}

		public void setFamily(String family) {
			this.family = family;
		}

		public String getQualifier() {
			return qualifier;
		}

		public void setQualifier(String qualifier) {
			this.qualifier = qualifier;
		}
		
		public String getKey() {
			return String.format("%s:%s", family, qualifier);
		}
		
		public Map<Calendar, HBaseCell> getCells() {
			return cells;
		}
		
		public void setCells(Map<Calendar, HBaseCell> cells) {
			this.cells = cells;
		}
		
		public HBaseCell addCell(byte[] value, Calendar timestamp) {
			return addCell(new HBaseCell(getTableKey(), getRowKey(), getKey(), value, timestamp));
		}
		
		public HBaseCell addCell(HBaseCell cell) {
			Calendar timestamp = cell.getTimestamp();
			if (timestamp == null)
				timestamp = new GregorianCalendar();
			
			cells.put(timestamp, cell);
			return cell;
		}
		
		public void initCells() {
			cells = new TreeMap<Calendar, HBaseCell>();
		}
		
		public String toString() {
			return String.format("{ %s, %s, -, - } [%d cells]", getRowKey(), getKey(), size());
		}
		
		public int size() {
			return cells.size();
		}
		
		@Override
		public Iterator<HBaseCell> iterator() {
			return getCells().values().iterator();
		}
		
	}
	
	public static class HBaseCell {
		
		private String rowKey;
		private String columnKey;
		private byte[] value;
		private Calendar timestamp;
		private String tableKey;
		
		public HBaseCell() { }

		public HBaseCell(String tableKey, String rowKey, String columnKey, byte[] value, Calendar timestamp) {
			this.tableKey = tableKey;
			this.rowKey = rowKey;
			this.columnKey = columnKey;
			this.value = value;
			this.timestamp = timestamp;
		}

		public String getColumnKey() {
			return columnKey;
		}
		
		public void setColumnKey(String columnKey) {
			this.columnKey = columnKey;
		}

		public String getRowKey() {
			return rowKey;
		}

		public void setRowKey(String rowKey) {
			this.rowKey = rowKey;
		}
		
		public String getTableKey() {
			return tableKey;
		}
		
		public void setTableKey(String tableKey) {
			this.tableKey = tableKey;
		}

		public byte[] getValue() {
			return value;
		}
		
		public String getValueAsString() {
			return new String(value);
		}

		public void setValue(byte[] value) {
			this.value = value;
		}

		public Calendar getTimestamp() {
			return timestamp;
		}

		public void setTimestamp(Calendar timestamp) {
			this.timestamp = timestamp;
		}
		
		public String toString() {
			return String.format("{ %s, %s, %s, %s }", getRowKey(), getColumnKey(), getTimestamp() != null ? getTimestamp().getTime().toString() : "<no timestamp>", getValueAsString());
		}
		
		public int size() {
			return 1;
		}

	}
	
	public static void main(String[] args) throws Exception {
		{
			HBaseModel m = new HBaseModel("test");
			HBaseRow r = m.addRow("row");
			HBaseColumn c = r.addColumn("content", "txt");
			c.addCell("5".getBytes(), new GregorianCalendar());
			Thread.sleep(1000);
			c.addCell("6".getBytes(), new GregorianCalendar());
			Thread.sleep(1000);
			c.addCell("7".getBytes(), new GregorianCalendar());
			Thread.sleep(1000);
			c.addCell("8".getBytes(), new GregorianCalendar());
			
			HBaseColumn c2 = r.addColumn("content", "pdf");
			c2.addCell("5".getBytes(), new GregorianCalendar());
			
			log.debug(m.toString());
			
			log.debug(r.toString());
			log.debug(c.toString());
			
			log.debug(c2.toString());
			
			log.debug("{}", m.size());
		}
		
		{
			List<HBaseCell> cells = new ArrayList<HBaseCell>();
			cells.add(new HBaseCell("table", "row", "column:key", "10".getBytes(), new GregorianCalendar()));
			Thread.sleep(1000);
			cells.add(new HBaseCell("tablae", "row", "column:key", "10".getBytes(), new GregorianCalendar()));
			Thread.sleep(1000);
			cells.add(new HBaseCell("table", "row2", "column:key", "10".getBytes(), new GregorianCalendar()));
			Thread.sleep(1000);
			cells.add(new HBaseCell("table", "row", "column:key", "5".getBytes(), new GregorianCalendar()));
			Thread.sleep(1000);
			cells.add(new HBaseCell("table", "row", "column:ciao", "10".getBytes(), new GregorianCalendar()));
			Thread.sleep(1000);
			cells.add(new HBaseCell("table", "row", "column:ciao", "5".getBytes(), new GregorianCalendar()));
			Thread.sleep(1000);
			cells.add(new HBaseCell("table", "row", "column:key", "8".getBytes(), new GregorianCalendar()));
			Thread.sleep(1000);
			cells.add(new HBaseCell("tablae", "row", "column:key", "1".getBytes(), new GregorianCalendar()));
			Thread.sleep(1000);
			cells.add(new HBaseCell("table", "row", "column:key", "20".getBytes(), new GregorianCalendar()));
			
			Map<String, HBaseModel> ms = getModelsByCells(cells);
			for (HBaseModel m : ms.values()) {
				for (HBaseRow r : m) {
					log.debug(r.toString());
					for (HBaseColumn c : r)
						log.debug(c.toString());
				}
				
				log.debug("{}", m.toString());
			}
		}
	}
	
	public String toString() {
		StringBuilder sb = new StringBuilder();
		
		for (HBaseCell v : getCells())
			sb.append(String.format("\t%s,\n", v.toString()));
		
		if (sb.length() > 0) {
			sb.deleteCharAt(sb.length() - 2);
			sb.insert(0, ":\n{\n");
			sb.insert(0, getKey());
			sb.insert(0, "Table ");
		} else {
			sb.append("Table ");
			sb.append(getKey());
			sb.append(": { ");
		}
		
		sb.append("}");
		
		return sb.toString();
	}
	
	public int size() {
		int total = 0;
		for (String key : rows.keySet())
			total += rows.get(key).size();
		return total;
	}
	
	// TODO: rimuovere
	public static Map<String, HBaseModel> getModelsByCells(List<HBaseCell> cells) {
		Map<String, HBaseModel> res = new HashMap<String, HBaseModel>();
		
		for (HBaseCell cell : cells) {
			String tableKey = cell.getTableKey();
			HBaseModel m = res.get(tableKey);
			if (m == null) {
				m = new HBaseModel(tableKey);
				res.put(tableKey, m);
			}
			String rowKey = cell.getRowKey();
			HBaseRow r = m.getRows().get(rowKey);
			if (r == null)
				r = m.addRow(rowKey);
			String columnKey = cell.getColumnKey();
			HBaseColumn c = r.getColumns().get(columnKey);
			if (c == null) {
				String[] split = columnKey.split(":");
				c = r.addColumn(split[0], split[1]);
			}
			c.addCell(cell);
		}
		
		return res;
	}
	
	public List<HBaseCell> getCells() {
		List<HBaseCell> res = new ArrayList<HBaseCell>();
		
		for (HBaseRow r : this) {
			for (HBaseColumn c : r) {
				for (HBaseCell v : c) {
					res.add(v);
				}
			}
		}
		
		return res;
	}
	
	public static List<HBaseCell> getCellsByModels(Map<String, HBaseModel> models) {
		List<HBaseCell> res = new ArrayList<HBaseCell>();
		
		for (String key : models.keySet()) {
			HBaseModel m = models.get(key);
			for (HBaseCell v : m.getCells())
				res.add(v);
		}
		
		return res;
	}

	@Override
	public Iterator<HBaseRow> iterator() {
		return getRows().values().iterator();
	}

}
