/**
 * 
 */
package it.polimi.hegira.models;

import it.polimi.hegira.hegira_metamodel.Metamodel;
import it.polimi.hegira.hegira_metamodel.sync.Operations;

/**
 * @author Marco Scavuzzo
 *
 */
public class MetamodelOperationWrapper {
	private Operations operation;
	private Metamodel mmEntity;
	
	public MetamodelOperationWrapper(Operations operation, Metamodel mmEntity) {
		this.operation = operation;
		this.mmEntity = mmEntity;
	}
	
	public String getLockId() {
		return mmEntity.rowKey;
	}
	
	/**
	 * @return the operation
	 */
	public synchronized Operations getOperation() {
		return operation;
	}
	/**
	 * @param operation the operation to set
	 */
	public synchronized void setOperation(Operations operation) {
		this.operation = operation;
	}
	/**
	 * @return the mmEntity
	 */
	public synchronized Metamodel getMmEntity() {
		return mmEntity;
	}
	/**
	 * @param mmEntity the mmEntity to set
	 */
	public synchronized void setMmEntity(Metamodel mmEntity) {
		this.mmEntity = mmEntity;
	}
}
