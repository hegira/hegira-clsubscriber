/**
 * 
 */
package it.polimi.hegira;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import it.polimi.hegira.listener.AbstractListener;
import it.polimi.hegira.utils.CLI;
import it.polimi.hegira.utils.CLI.SendDMQsResponseCommnad;
import it.polimi.hegira.utils.Constants;
import it.polimi.hegira.utils.Constants.CONSUMER_GROUPS;
import it.polimi.hegira.utils.ZooKeeperUtils;

import org.apache.curator.utils.CloseableUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.beust.jcommander.JCommander;

/**
 * @author Marco Scavuzzo
 *
 */
public class EntryClass {
	private final transient static Logger log = LoggerFactory.getLogger(EntryClass.class);

	/**
	 * @param args type of database subscriber, zookeeper connect string
	 */
	public static void main(String[] args) {
		CLI cli = new CLI();
		JCommander jc = new JCommander(cli);
		jc.setProgramName("Hegira: synchronization system");
		
//		try{
//			jc = new JCommander(cli,args);
//		}catch(Exception e){
//			//log.error(e.getMessage());
//			StringBuilder sb = new StringBuilder(e.getMessage()+"\n");
//			jc.usage(sb);
//			System.out.println(sb.toString());
//			return;
//		}
		
		SendDMQsResponseCommnad dmqsCli = cli.new SendDMQsResponseCommnad();
		jc.addCommand(SendDMQsResponseCommnad.commandName, dmqsCli);
		
		try{
			jc.parse(args);
			String command = jc.getParsedCommand();
			
			if(cli.help){
				jc.usage();
				return;
			}
			
			if(command!=null){
				switch(command){
					case SendDMQsResponseCommnad.commandName:
						EnvironmentCreator.init(true, dmqsCli.getHttpResponse);
						break;
					default:
						jc.usage();
						return;
				}
			}
		}catch(Exception e){
			log.error(e.getMessage());
			if(jc.getParsedCommand()!=null){
				jc.usage(jc.getParsedCommand());
			} else {
				jc.usage();
			}
			return;
		}
		
		log.debug("Selected databases. SOURCE:"+cli.dbName_src+" DEST: "+cli.dbName_dst+
				". zkConnectString: "+cli.zkConnectString);
		
		if(cli.cleanOffset){
			try {
				for(CONSUMER_GROUPS cg : Constants.CONSUMER_GROUPS.values()){
					ZooKeeperUtils.cleanOffsets(cli.zkConnectString, cg.name(), Constants.SYNC_TOPIC);
				}
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		}
		
		final List<AbstractListener> listeners = EnvironmentCreator.getInstance(cli).getListeners();
//		if(listener!=null){
//			try {
//				listener.consume();
//			} catch (Exception e) {
//				log.error("Error consuming from the commit log", e);
//			}
//		}else{
//			log.error("Commit Log listener not properly set during environment initialization.");
//		}
		final ExecutorService executor = Executors.newFixedThreadPool(cli.thread_no);
		if(listeners!=null){
			for(AbstractListener listener : listeners){
				executor.submit(listener);
			}
		}else{
			log.error("Commit log listeners were not properly instatiated");
			return;
		}
		
		final CLI finalcli = cli;
		Runtime.getRuntime().addShutdownHook(new Thread() {
			@Override
			public void run() {
				for(AbstractListener listener : listeners){
					listener.shutdown();
					CloseableUtils.closeQuietly(listener);
				}
				executor.shutdown();
				
				try {
					EnvironmentCreator.getInstance(finalcli).close();
				} catch (Exception e1) {
					e1.printStackTrace();
				}
				
				try {
					executor.awaitTermination(5000, TimeUnit.MILLISECONDS);
				} catch (InterruptedException e) {
					log.error("",e);
				}
			}
		});
	}

}
