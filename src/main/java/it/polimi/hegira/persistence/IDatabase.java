package it.polimi.hegira.persistence;

import it.polimi.hegira.exceptions.ConnectException;
import it.polimi.hegira.exceptions.OperationException;
import it.polimi.hegira.hegira_metamodel.Metamodel;

public interface IDatabase {
	public void connect() throws ConnectException;
	public void disconnect();
	/**
	 * Checks if a connection to this database has already been established.
	 * @return true if a connection is already in place, false otherwise.
	 */
	public boolean isConnected();
	public void insert(Metamodel mm) throws OperationException;
	public void update(Metamodel mm) throws OperationException;
	public void delete(Metamodel mm) throws OperationException;
	public String getName();
}
