/**
 * Copyright 2015 Marco Scavuzzo
 * Contact: Marco Scavuzzo <marco.scavuzzo@polimi.it>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package it.polimi.hegira.persistence.cassandra;


import it.polimi.hegira.exceptions.ConnectException;
import it.polimi.hegira.models.CassandraColumn;
import it.polimi.hegira.models.CassandraModel;
import it.polimi.hegira.utils.CassandraTypesUtils;
import it.polimi.hegira.utils.ConfigurationManagerCassandra;
import it.polimi.hegira.utils.Constants;

import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.datastax.driver.core.BoundStatement;
import com.datastax.driver.core.ColumnMetadata;
import com.datastax.driver.core.PreparedStatement;
import com.datastax.driver.core.Session;
import com.datastax.driver.core.Statement;
import com.datastax.driver.core.TableMetadata;
import com.datastax.driver.core.querybuilder.Clause;
import com.datastax.driver.core.querybuilder.QueryBuilder;
import com.datastax.driver.core.querybuilder.Update;
import com.datastax.driver.core.querybuilder.Update.Assignments;
import com.datastax.driver.core.querybuilder.Update.Where;
import com.datastax.driver.core.schemabuilder.SchemaBuilder;
import com.datastax.driver.core.schemabuilder.SchemaStatement;

/**
 * This class manages a single Table.
 * Used to check columns, alter the table if necessary, and perform inserts into Cassandra.
 * @author Andrea Celli
 * @author Marco Scavuzzo
 *
 */
public class Table {
	private static Logger log = LoggerFactory.getLogger(Table.class);

	private String tableName;
	private List<String> columns;
	//the current session
	private Session session;
	//flag that signals if the table has been altered
	private boolean changed;
	//the prepared statement for the actual table configuration (with all its columns)
	private PreparedStatement defaultPrepared;
	//the name of the primary key
	private String primaryKeyName;
	private String primaryKeyType;
	
	private String keyspace;
	/**
	 * Initialize all the variables and creates the new table in cassandra.
	 * When created the table contains only the column for the id values.
	 * @param tableName
	 * @Throws ConnectException (raised if the session manager is not able to connect to Cassandra)
	 */
	public Table(String tableName) throws ConnectException{
		this.tableName=tableName;
		columns=Collections.synchronizedList(new ArrayList<String>());
		
		//retrieves the unique session from the session manager
		session=SessionManager.getSessionManager().getSession();
		setChanged(false);
		primaryKeyName=ConfigurationManagerCassandra.getConfigurationProperties(Constants.PRIMARY_KEY_NAME);
		String pkType = ConfigurationManagerCassandra.getConfigurationProperties(Constants.C_PRIMARY_KEY_TYPE);
		if(pkType!=null){
			pkType = pkType.toLowerCase();
		}
		this.primaryKeyType=pkType;
		createInitialTable(tableName);
	}
	
	public synchronized void update(CassandraModel row) throws ClassNotFoundException,InvalidParameterException {
		//log.debug(Thread.currentThread().getName()+" starting using table: "+tableName);
		List<CassandraColumn> colsToInsert=row.getColumns();
		
		checkTable(row);
		
		Assignments updateAssignments = QueryBuilder.update(keyspace, tableName).with();
		for(CassandraColumn col : colsToInsert){
			updateAssignments.and(QueryBuilder.set("\""+col.getColumnName()+"\"", col.getColumnValue()));
		}
		Where where;
		if(primaryKeyType==null)
			where = updateAssignments.where(QueryBuilder.eq(primaryKeyName, ""+row.getKeyValue()));
		else{
			if(primaryKeyType.equals("int") || primaryKeyType.equals("integer"))
				where = updateAssignments.where(QueryBuilder.eq(primaryKeyName, Integer.parseInt(row.getKeyValue())));
			else
				where = updateAssignments.where(QueryBuilder.eq(primaryKeyName, ""+row.getKeyValue()));
		}
		session.execute(where);
		
		//log.debug(Thread.currentThread().getName()+" finished using table: "+tableName);
	}
	
	public synchronized void delete(CassandraModel row) throws ClassNotFoundException,InvalidParameterException {
		try {
			if(!SessionManager.getSessionManager().tableExists(keyspace, tableName))
				return;
		} catch (ConnectException e) {
			return;
		}
		Clause where;
		if(primaryKeyType==null)
			where = QueryBuilder.eq(primaryKeyName, ""+row.getKeyValue());
		else{
			if(primaryKeyType.equals("int") || primaryKeyType.equals("integer"))
				where = QueryBuilder.eq(primaryKeyName, Integer.parseInt(row.getKeyValue()));
			else
				where = QueryBuilder.eq(primaryKeyName, ""+row.getKeyValue());
		}	
		Statement stmt = QueryBuilder.delete().from(keyspace, tableName)
			.where(where);
		session.execute(stmt);
	}
	
	/**
	 * This methods inserts a row in the table.
	 * This methods is synchronized in order to avoid conflicts due to different threads changing the same table.
	 * 
	 * @param row - the row to be insterted
	 * @throws ClassNotFoundException
	 * @throws InvalidParameterException
	 */
	public synchronized void insert(CassandraModel row) throws ClassNotFoundException,InvalidParameterException{
		//log.debug(Thread.currentThread().getName()+" starting using table: "+tableName);
		
		HashMap<String, Object> checkTable = checkTable(row);
		if(checkTable==null){
			log.error("{} - Error inserting data in cassandra",
					Thread.currentThread().getName());
			return;
		}
		
		Object rowSizeObj = checkTable.get("rowSize");
		String statementString = (String) checkTable.get("statementString");
		Object[] values = (Object[]) checkTable.get("values");
		
		if(rowSizeObj==null || statementString==null || values==null){
			log.error("{} - Error inserting data in cassandra",
					Thread.currentThread().getName());
			return;
		}
		int rowSize = (int) rowSizeObj;
		//retrieve the statement to be executed
		PreparedStatement execStatement=getPreparedStatement(statementString,rowSize);
		//bing the statement with the array of objects that have to be inserted
		BoundStatement bind=execStatement.bind(values);
		//insert
		session.execute(bind);
		
		//log.debug(Thread.currentThread().getName()+" finished using table: "+tableName);
	}
	
	private HashMap<String, Object> checkTable(CassandraModel row) throws InvalidParameterException, ClassNotFoundException{
		HashMap<String, Object> returnMap = new HashMap<String, Object>(3);
		List<CassandraColumn> colsToInsert=row.getColumns();
		
		//number of column contained in the row
		int rowSize=colsToInsert.size();
		
		//the string used to build the prepared statement
		//it contains columns names and gets progressively update
		//the String will have the format: name1, name2, name3, name4....
		String statementString;
		
		//the array of objects to be inserted into the db
		//the size of the array is rowSize+1 because we have to include the key
		Object[] values=new Object[rowSize+1];
		
		//set the primary key
		statementString=primaryKeyName;
		if(primaryKeyType==null)
			values[0]=row.getKeyValue();
		else{
			if(primaryKeyType.equals("int") || primaryKeyType.equals("integer"))
				values[0]=Integer.parseInt(row.getKeyValue());
			else
				values[0]=row.getKeyValue();
		}
		
		//CHECK id the table contains the other columns
		//at the same time build the names string and the object array
		for(int i=1;i<rowSize+1;i++){
			
			//update names string
			String name=colsToInsert.get(i-1).getColumnName();
			statementString=statementString + ", \""+name+"\"";
			//update objects array
			values[i]=colsToInsert.get(i-1).getColumnValue();
			
			//check if the table already contains the column
			if(!columns.contains(name)){
				//THE TABLE NEEDS TO BE UPDATED
				alterTable(name,colsToInsert.get(i-1).getValueType(),colsToInsert.get(i-1).isIndexed());
				setChanged(true);
			}
		}
		
		returnMap.put("rowSize", rowSize);
		returnMap.put("statementString", statementString);
		returnMap.put("values", values);
		return returnMap;
	}

	/**
	 * This method creates the table in the cassandra db.
	 * the initial table will contain only an id column
	 * @param tableName2
	 */
	private void createInitialTable(String tableName) {
		if(primaryKeyType==null)
			session.execute(
			      "CREATE TABLE IF NOT EXISTS " + tableName + " ( " + primaryKeyName + " varchar PRIMARY KEY );");
		else{
			if(primaryKeyType.equals("int") || primaryKeyType.equals("integer"))
				session.execute(
					      "CREATE TABLE IF NOT EXISTS " + tableName + " ( " + primaryKeyName + " int PRIMARY KEY );");
			else
				session.execute(
					      "CREATE TABLE IF NOT EXISTS " + tableName + " ( " + primaryKeyName + " varchar PRIMARY KEY );");
		}
		//update the list of column names contained in the table
		String columnsNames=initColumnList(tableName);
		defaultPrepared=createPreparedStatement(columnsNames,columns.size()-1);
	}
	
	/**
	 *  initialize the list of columns
	 *	it's needed to manage a table that already exists in the database
	 * @param tableName
	 * @return string of names of the initial columns
	 */
	private String initColumnList(String tableName){
		this.keyspace=ConfigurationManagerCassandra.getConfigurationProperties(Constants.KEYSPACE);
		//string of column names to build the first prepared statement
		String columnNames=" ";
		TableMetadata tableMetadata=null;
		//it goes on only when it has retrieved the data for the table (it has to exist given that it's created before calling
		//this method)
		while(tableMetadata==null){
			tableMetadata=session
				.getCluster()
				.getMetadata()
				.getKeyspace(keyspace)
				.getTable(tableName);
		}
		List<ColumnMetadata> existingColumns=tableMetadata.getColumns();
		for(int i=0;i<existingColumns.size()-1;i++){
				String name=existingColumns.get(i).getName();
				columns.add(name);
				columnNames=columnNames+"\""+name+"\""+", ";
		}
		//last element
		String name=existingColumns.get(existingColumns.size()-1).getName();
		columns.add(name);
		columnNames=columnNames+"\""+name+"\"";
		log.info("{} - Columnames: {} exist", Thread.currentThread().getName(), columnNames);
		
		return columnNames;
	}

	/**
	 * Returns a new prepared statement to insert a new row with the given columns.
	 * @param columnNames
	 * @param rowSize - number of columns contained in the row (without id)
	 * @return PreparedStatement
	 */
	private PreparedStatement createPreparedStatement(
			String columnNames,int rowSize) {
		String completeStatementString=packString(columnNames,rowSize);
		/*log.debug("{} - Creating prepared statement: {}",
				Thread.currentThread().getName(),
				completeStatementString);*/
		return session.prepare(completeStatementString);
	}
	
	/**
	 * Takes as input the string containing column names in the format:
	 * name1, name2, name3...
	 * Pack the string with the remaining parts in order to use it to build a preparedStatement to perform an insert.
	 * The final string will be in the form:
	 * "INSERT INTO <tableName> (<name1>, <name2>, <name3>) VALUES (?,?,?)"
	 * @param columnNames
	 * @param rowSize - number of columns contained in the row (without id)
	 * @return the complete string for a prepared statement that performs an insert
	 */
	private String packString(String columnNames,int rowSize) {
		String completeString="INSERT INTO "+tableName+" ( "+columnNames+" ) VALUES ( ";
		//there's at least a ? for the id
		String questionMarks="?";
		//add other values question marks for remaining columns
		for(int i=0;i<rowSize;i++){
			questionMarks=questionMarks+",?";
		}
		completeString=completeString+questionMarks+" ) ";
		return completeString;
	}

	/**
	 * Returns a prepared statement and eventually updates the default prepared statement.
	 * 1) table has been altered-->create and set a new default statement, return the default statement
	 * 2) table has not been altered
	 *  2.1)same number of columns-->return the default PreparedStatement (reuse to increase efficiency)
	 *  2.2)lower number of columns-->compute and return a new prepared statement (without setting it as default)
	 * @param columnNames
	 * @param rowSize
	 * @return PreparedStatement
	 */
	private PreparedStatement getPreparedStatement(String columnNames,
			int rowSize) {
		//table has been changed
		if(isChanged()){
			defaultPrepared=createPreparedStatement(columnNames, rowSize);
			return defaultPrepared;
		}else{
			//not changed and same number of columns
			//+1 takes into account the id
			if(rowSize+1==columns.size()){
				return defaultPrepared;
			}else{
				//lower number of columns but no table changes
				return createPreparedStatement(columnNames, rowSize);
			}
		}
	}
	
	/**
	 * This method performs an alter on the table in order to introduce a new column.
	 * 
	 * @param name - name of the new column
	 * @param valueType - the string representing the type of the new column
	 * @param indexed - true if the column has to be indexed
	 */
	private void alterTable(String name, String valueType, boolean indexed) throws ClassNotFoundException,InvalidParameterException{
		SchemaStatement alter;
		log.debug("Start altering table: "+tableName+" Columns: "+columns.toString());
		log.debug("Alter table to add column: "+name);
		try{
			//build the alter
			alter=SchemaBuilder
					.alterTable(tableName)
					.addColumn("\""+name+"\"")
					.type(CassandraTypesUtils.getCQLDataType(valueType));
			//execute the alter
			session.execute(alter);
			
			//create a new index if needed
			if(indexed){
				SchemaStatement createIndex=SchemaBuilder
						.createIndex(tableName+"_"+"\""+name+"\""+"_index")
						.onTable(tableName)
						.andColumn("\""+name+"\"");
				
				session.execute(createIndex);
			}
			
			//add the column to the columns list
			columns.add(name);
			
			//TODO: altre catch
		}catch(ClassNotFoundException | InvalidParameterException ex){
			log.error("Error while altering table: "+tableName+"\nStackTrace:\n"+ex.getStackTrace());
			ex.printStackTrace();
			throw  ex;
		}
	}


	public String getTableName() {
		return tableName;
	}

	public List<String> getColumns() {
		return columns;
	}

	public Session getSession() {
		return session;
	}

	private boolean isChanged() {
		return changed;
	}


	private void setChanged(boolean changed) {
		this.changed = changed;
	}
}
