/**
 * Copyright 2015 Marco Scavuzzo
 * Contact: Marco Scavuzzo <marco.scavuzzo@polimi.it>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package it.polimi.hegira.persistence.cassandra;

import it.polimi.hegira.exceptions.ConnectException;
import it.polimi.hegira.utils.ConfigurationManagerCassandra;
import it.polimi.hegira.utils.Constants;
import it.polimi.hegira.utils.PropertiesManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.PoolingOptions;
import com.datastax.driver.core.Session;
import com.datastax.driver.core.SocketOptions;
import com.datastax.driver.core.exceptions.AuthenticationException;
import com.datastax.driver.core.exceptions.NoHostAvailableException;

/**
 * This class implements the singleton pattern.
 * In this way all threads are forced to share the same session.
 * This is a recomended practice when dealing with a single keyspace.)
 * 
 * @author Andrea Celli
 *
 */
public class SessionManager {

	private static Logger log = LoggerFactory.getLogger(SessionManager.class);
	
	//the instance of the manager
	private static SessionManager manager;
	//the session shared among differend threads
	private static Session session;
	private static Cluster cluster;
	private static final int defaultReadTimeOutMillis=60000;
	private static final int defaultConnectTimeOutMillis=30000;
	private static final int defaultPort=9042;
	
	/*
	 * version 2
	private String server;
	private String username;
	private String password;
	private String keyspace;*/
	
	private SessionManager() throws ConnectException{
		//credentials
		String server=PropertiesManager.getCredentialsProperty(Constants.CASSANDRA_SERVER);
		String portStr=PropertiesManager.getCredentialsProperty("cassandra.port");
		String username=PropertiesManager.getCredentialsProperty(Constants.CASSANDRA_USERNAME);
		String password=PropertiesManager.getCredentialsProperty(Constants.CASSANDRA_PASSWORD);
		String keyspace=ConfigurationManagerCassandra.getConfigurationProperties(Constants.KEYSPACE);
		
		if(server==null || portStr==null || username==null || password==null || keyspace==null){
			log.error("Cassandra properties were not properly configured!\n"
					+ "server: {}\n"
					+ "port: {}\n"
					+ "username: {}\n"
					+ "password: {}\n"
					+ "keyspace: {}\n",server,portStr,username,password,keyspace);
			throw new IllegalArgumentException("Cassandra properties were not properly configured!");
		}
		
		int port = defaultPort;
		if(portStr!=null && portStr.length()>0){
			try{
				port = Integer.parseInt(portStr);
			}catch(Exception e){}
		}
		
		//build the session
			try{
				SocketOptions socketOptions = configureSocket();
				PoolingOptions poolingOptions = configurePoolingOptions(socketOptions);
				Cluster.Builder clusterBuilder=Cluster.builder()
						.addContactPoint(server)
						.withPort(port)
						.withCredentials(username, password)
						.withSocketOptions(socketOptions)
						.withPoolingOptions(poolingOptions);
				cluster=clusterBuilder.build();
				this.session=cluster.connect(keyspace);
				return;
			}catch(NoHostAvailableException | 
					AuthenticationException |
					IllegalStateException ex){
				log.error(Thread.currentThread().getName() + " - Not able to connect to Cassandra ",ex);
				throw new ConnectException(ex);
			}
	}
	
	private void createKeyspaceIfnotExists(){
		
	}
	
	private SocketOptions configureSocket(){
		String readTimeOutStr = 
				ConfigurationManagerCassandra.getConfigurationProperties("cassandra.readTimeOutMillis");
		String connectTimeOutStr = 
				ConfigurationManagerCassandra.getConfigurationProperties("cassandra.connectTimeOutMillis");
		
		int readTimeOutMillis = defaultReadTimeOutMillis;
		if(readTimeOutStr!=null && readTimeOutStr.length()>0){
			try{
				readTimeOutMillis = Integer.parseInt(readTimeOutStr);
			}catch(Exception e){}
		}
		
		int connectTimeOutMillis = defaultConnectTimeOutMillis;
		if(readTimeOutStr!=null && readTimeOutStr.length()>0){
			try{
				connectTimeOutMillis = Integer.parseInt(connectTimeOutStr);
			}catch(Exception e){}
		}
		
		SocketOptions socketOptions = new SocketOptions();
		socketOptions.setReadTimeoutMillis(readTimeOutMillis);
		socketOptions.setConnectTimeoutMillis(connectTimeOutMillis);
		socketOptions.setKeepAlive(true);
		
		return socketOptions;
	}
	
	private PoolingOptions configurePoolingOptions(SocketOptions opts){
		PoolingOptions poolingOptions = new PoolingOptions();
		if(opts==null)
			return poolingOptions;
		
		int connectionTimeoutSecs = opts.getConnectTimeoutMillis()/1000;
		String hbIntStr = 
				ConfigurationManagerCassandra.getConfigurationProperties("cassandra.heartbeatIntervalSeconds");
		if(hbIntStr==null){
			hbIntStr = new String("30");
		} 
		
		int hbInt;
		try{
			hbInt = Integer.parseInt(hbIntStr);
			if(hbInt>connectionTimeoutSecs)
				poolingOptions.setHeartbeatIntervalSeconds(hbInt);
			else
				poolingOptions.setHeartbeatIntervalSeconds(connectionTimeoutSecs+1);
		}catch(Exception e){
			poolingOptions.setHeartbeatIntervalSeconds(connectionTimeoutSecs+1);
		}
		
		return poolingOptions;
	}
	
	/**
	 * returns the unique instance of the session manager.
	 * creates the instance if it does not already exist.
	 * @return SessionManager
	 * @Throws ConnectException (raised if the session manager is not able to connect to Cassandra)
	 */
	public static SessionManager getSessionManager() throws ConnectException{
		if(manager==null){
			manager=new SessionManager();
		}
		return manager;
	}
	
	/**
	 * get the unique session
	 * 
	 * @return session
	 */
	public Session getSession() {//throws ConnectException{
		/*
		 *version 2 
		 *
		Session session;
		try{
			Cluster.Builder clusterBuilder=Cluster.builder()
					.addContactPoint(server)
					.withCredentials(username, password);
			Cluster cluster=clusterBuilder.build();
			session=cluster.connect(keyspace);
			log.debug("new session created for thread: "+Thread.currentThread().getName());
			return session;
		}catch(NoHostAvailableException | 
				AuthenticationException |
				IllegalStateException ex){
			log.error(Thread.currentThread().getName() + " - Not able to connect to Cassandra ",ex);
			throw new ConnectException(ex);
	}*/
		return session;
	}

	public static void close(){
		try{
			if(session!=null)
				session.close();
			if(cluster!=null)
				cluster.close();
		}catch(Exception e){}
	}
	
	public boolean keyspaceExists(String ks){
		if(cluster==null)
			return false;
		try{
			if(cluster.getMetadata().getKeyspace(ks)!=null)
				return true;
			else
				return false;
		}catch(Exception e){
			return false;
		}
	}
	
	public boolean tableExists(String ks, String table){
		if(cluster==null)
			return false;
		try{
			if(cluster.getMetadata().getKeyspace(ks)!=null &&
					cluster.getMetadata().getKeyspace(ks).getTable(table)!=null)
				return true;
			else
				return false;
		}catch(Exception e){
			return false;
		}
	}
}
