package it.polimi.hegira.persistence;

import java.net.URISyntaxException;
import java.security.InvalidKeyException;
import java.util.List;

import org.apache.log4j.Logger;

import com.microsoft.windowsazure.services.core.storage.CloudStorageAccount;
import com.microsoft.windowsazure.services.core.storage.StorageException;
import com.microsoft.windowsazure.services.table.client.CloudTable;
import com.microsoft.windowsazure.services.table.client.CloudTableClient;
import com.microsoft.windowsazure.services.table.client.DynamicTableEntity;
import com.microsoft.windowsazure.services.table.client.TableOperation;
import com.microsoft.windowsazure.services.table.client.TableQuery;
import com.microsoft.windowsazure.services.table.client.TableResult;

import it.polimi.hegira.exceptions.ConnectException;
import it.polimi.hegira.models.AzureTablesModel;
import it.polimi.hegira.hegira_metamodel.Metamodel;
import it.polimi.hegira.transformers.AzureTablesTransformer;
import it.polimi.hegira.utils.Constants;
import it.polimi.hegira.utils.DefaultErrors;
import it.polimi.hegira.utils.PropertiesManager;

public class Tables implements IDatabase {
	private transient Logger log = Logger.getLogger(Tables.class);
	
	private class ConnectionObject{
		public ConnectionObject(){}
		public ConnectionObject(CloudStorageAccount account, CloudTableClient tableClient){
			this.account=account;
			this.tableClient=tableClient;
		}
		protected CloudStorageAccount account;
		protected CloudTableClient tableClient;
	}
	
	ConnectionObject co;
	Object co_lock = new Object();
	
	/**
	 * Checks if a connection has already been established
	 * @return true if connected, false if not.
	 */
	@Override
	public boolean isConnected(){
		return (co==null|| co.account==null || co.tableClient==null)
				? false : true;
	}

	@Override
	public void connect() throws ConnectException {
		if(!isConnected()){
			String credentials = PropertiesManager.getCredentialsProperty(Constants.AZURE_PROP
					+".UNOFFICIAL");
			try {
				CloudStorageAccount account = CloudStorageAccount.parse(credentials);
				CloudTableClient tableClient = account.createCloudTableClient();
				synchronized(co_lock){
					co = new ConnectionObject(account, tableClient);
				}
				log.debug(Thread.currentThread().getName()+" - Connected");
			} catch (InvalidKeyException | URISyntaxException e) {
				e.printStackTrace();
				throw new ConnectException(DefaultErrors.connectionError);
			}
		}
	}

	@Override
	public void disconnect() {
		if(isConnected()){
			synchronized(co_lock){
				co.account=null;
				co.tableClient=null;
				co=null;
			}
			log.debug(Thread.currentThread().getName()+" - Disconnected");
		}else{
			log.warn(DefaultErrors.notConnected);
		}
	}

	@Override
	public void insert(Metamodel mm) {
		AzureTablesModel aModel = fromMetamodel(mm);
		String tableName = aModel.getTableName();
		try {
			CloudTable tbl = createTable(tableName);
			if(tbl==null)
				throw new URISyntaxException("Unable to create table ", tableName);
			
			List<DynamicTableEntity> entities = aModel.getEntities();
			for(DynamicTableEntity entity : entities){
				TableResult res = putEntity(tableName, entity);
			}
		} catch (URISyntaxException e) {
			log.error("Unable to create table", e);
		} catch (StorageException e) {
			log.error("Unable to insert entity", e);
		}
	}
	
	/**
	 * Insert a new entity (or updates if it already exists) in the given table, provided that the table exists
	 * @param tableName Name of the table where to insert the entity
	 * @param entity An instance of the DynamicEntity
	 * @throws StorageException - if an error occurs accessing the storage service, or the operation fails.
	 * 
	 * @return A TableResult containing the result of executing the TableOperation on the table. The TableResult class encapsulates the HTTP response and any table entity results returned by the Storage Service REST API operation called for a particular TableOperation.
	 */
	private TableResult putEntity(String tableName, DynamicTableEntity entity) throws StorageException{
		if(tableName.indexOf("@")==0)
			tableName=tableName.substring(1);
		//log.debug("Inserting entity: "+entity.getRowKey()+" into "+tableName);
		if(isConnected()){
			/**
			 * Bullet proof write
			 */
			int retries = 0;
			
			while(true){
				try{
					// Create an operation to add the new entity.
					TableOperation insertion = TableOperation.insertOrMerge(entity);
					
					// Submit the operation to the table service.
					return co.tableClient.execute(tableName, insertion);
				}catch(Exception e){
					retries++;
					log.error(Thread.currentThread().getName() + 
							" ERROR -> insertEntity : "+e.getMessage());
					
					log.error("Entity "+entity.getRowKey(), e);
				}finally{
					if(retries>=5){
						log.error(Thread.currentThread().getName() + 
								"SKIPPING entity: "+entity.getRowKey());
						return null;
					}
				}
			}
			
		}else{
			log.info(Thread.currentThread().getName()+" - "+DefaultErrors.notConnected);
			return null;
		}
	}

	/**
	 * Creates the table in the storage service if it does not already exist.
	 *     
	 * @param tableName A String that represents the table name.
	 * @return The created table or null
	 * @throws URISyntaxException If the resource URI is invalid
	 */
	private CloudTable createTable(String tableName) throws URISyntaxException{
		if(tableName.indexOf("@")==0)
			tableName=tableName.substring(1);
		//log.debug(Thread.currentThread().getName()+" with thread_id="+thread_id+" Creating table: "+tableName);
		if(isConnected()){
			CloudTable cloudTable = new CloudTable(tableName, co.tableClient);
			try {
				cloudTable.createIfNotExist();
			} catch (StorageException e) {
				e.printStackTrace();
			}
			return cloudTable;
		}else{
			log.error(Thread.currentThread().getName()+" - "+DefaultErrors.notConnected);
			try {
				connect();
				createTable(tableName);
			} catch (ConnectException e) {
				return null;
			}
			return null;
		}
	}
	
	@Override
	public void update(Metamodel mm) {
		insert(mm);
	}

	@Override
	public void delete(Metamodel mm) {
		if(isConnected()){
			AzureTablesModel atm = fromMetamodel(mm);
			for(DynamicTableEntity entity : atm.getEntities()){
				TableQuery<DynamicTableEntity> myQuery = TableQuery.from(atm.getTableName(), DynamicTableEntity.class)
					    .where("(PartitionKey eq '"+entity.getPartitionKey()+"') and (RowKey eq '"+
					    		entity.getRowKey()+"')");
				//CloudTable cloudTable = co.tableClient.getTableReference(atm.getTableName());
				Iterable<DynamicTableEntity> fullEntity = co.tableClient.execute(myQuery);
				if(fullEntity == null) return;
				try{
					for(DynamicTableEntity e : fullEntity){
						try {
							co.tableClient.execute(atm.getTableName(), TableOperation.delete(e));
						} catch (StorageException e1) {
							log.error(Thread.currentThread().getName()+" - "+DefaultErrors.queryError);
						}
					}
				}catch(java.util.NoSuchElementException e){
					return;
				}
			}
		} else{
			log.error(Thread.currentThread().getName()+" - "+DefaultErrors.notConnected);
		}

	}
	
	
	private AzureTablesModel fromMetamodel(Metamodel mm){
		AzureTablesTransformer att = new AzureTablesTransformer();
		 return att.fromMyModel(mm);
	}

	@Override
	public String getName() {
		return Constants.AZURE_TABLES;
	}

}
