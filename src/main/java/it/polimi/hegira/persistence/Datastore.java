/**
 * 
 */
package it.polimi.hegira.persistence;

import java.io.IOException;
import java.util.ArrayList;
import java.util.ConcurrentModificationException;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entities;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Transaction;
import com.google.appengine.tools.remoteapi.RemoteApiInstaller;
import com.google.appengine.tools.remoteapi.RemoteApiOptions;

import it.polimi.hegira.exceptions.ConnectException;
import it.polimi.hegira.models.DatastoreModel;
import it.polimi.hegira.hegira_metamodel.Metamodel;
import it.polimi.hegira.transformers.DatastoreTransformer;
import it.polimi.hegira.utils.CertificatesManager;
import it.polimi.hegira.utils.Constants;
import it.polimi.hegira.utils.DefaultErrors;
import it.polimi.hegira.utils.PropertiesManager;
import it.polimi.hegira.utils.CertificatesManager.ManagedCertificates;

/**
 * @author Marco Scavuzzo
 *
 */
public class Datastore implements IDatabase {
	private transient static Logger log = LoggerFactory.getLogger(Datastore.class);
	private boolean debug=true;
	private boolean OAuth = true;
	
	private class ConnectionObject{
		public ConnectionObject(){}
		public ConnectionObject(RemoteApiInstaller installer, DatastoreService ds){
			this.installer = installer;
			this.ds = ds;
		}
		protected RemoteApiInstaller installer;
		protected DatastoreService ds;
	}
	
	ConnectionObject co;
	Object co_lock = new Object();

	/* (non-Javadoc)
	 * @see it.polimi.hegira.persistence.IDatabase#connect()
	 */
	@Override
	public void connect() throws ConnectException {
		if(!isConnected()){
			setConnectionAuthentication();
			String server = PropertiesManager.getCredentialsProperty(Constants.DATASTORE_SERVER);
			RemoteApiOptions options;
			if(!OAuth){
				String username = PropertiesManager.getCredentialsProperty(Constants.DATASTORE_USERNAME);
				String password = PropertiesManager.getCredentialsProperty(Constants.DATASTORE_PASSWORD);
				options = new RemoteApiOptions()
        				.server(server, 443)
        				.credentials(username, password);
				log.debug(Thread.currentThread().getName()+" - Logging into "+server+
						" with ClientLogin (deprecated credentials)");
			}else{
				String clientId = PropertiesManager.getCredentialsProperty(Constants.DATASTORE_CLIENTID);
				String pkName = PropertiesManager.getCredentialsProperty(Constants.DATASTORE_PKNAME);
				String privateKeyPath = CertificatesManager.getPrivateKeyPath(ManagedCertificates.DATASTORE, 
						pkName);
				options = new RemoteApiOptions()
					.server(server, 443)
					.useServiceAccountCredential(clientId, privateKeyPath);
				log.debug(Thread.currentThread().getName()+" - Logging into "+server+
						" with OAuth Service Login and clientId: "+clientId);
			}
			try {
				RemoteApiInstaller installer = new RemoteApiInstaller();
				installer.install(options);
				DatastoreService ds = DatastoreServiceFactory.getDatastoreService();
				synchronized(co_lock){
					this.co = new ConnectionObject(installer, ds);
				}
			} catch (IOException e) {
				log.error(DefaultErrors.connectionError+"\nStackTrace:\n"+e.getStackTrace());
				throw new ConnectException(DefaultErrors.connectionError);
			}
		}else{
			if(!debug){
				log.warn(DefaultErrors.alreadyConnected);
			}
		}
	}
	
	private void setConnectionAuthentication(){
		String login = PropertiesManager.getCredentialsProperty(Constants.DATASTORE_LOGIN);
		if(login==null) return;
		
		if(login.toLowerCase().equals("oauth")){
			this.OAuth=true;
		}else if(login.toLowerCase().equals("client")){
			this.OAuth=false;
		}
	}
	
	/**
	 * Checks if a connection has already been established
	 * @return true if connected, false if not.
	 */
	@Override
	public boolean isConnected(){
			if(co==null) return false;
			return (co.installer==null || 
					co.ds==null) ? false : true;
	}

	private DatastoreModel fromMetamodel(Metamodel metamodel){
		DatastoreTransformer dt = new DatastoreTransformer(co.ds);
		return dt.fromMyModel(metamodel);
	}
	
	/* (non-Javadoc)
	 * @see it.polimi.hegira.persistence.IDatabase#disconnect()
	 */
	@Override
	public void disconnect() {
		if(debug) return;
		
		if(isConnected()){
			synchronized(co_lock){
				if(co.installer!=null)
					co.installer.uninstall();
				co.installer = null;
				co.ds = null;
			}
			log.debug(Thread.currentThread().getName() + " Disconnected");
		}else{
			log.warn(DefaultErrors.notConnected);
		}
	}

	/* (non-Javadoc)
	 * @see it.polimi.hegira.persistence.IDatabase#insert(it.polimi.hegira.models.Metamodel)
	 */
	@Override
	public void insert(Metamodel mm) {
		DatastoreModel dsModel = fromMetamodel(mm);
		if(isConnected()){
			ArrayList<Entity> batch = new ArrayList<Entity>(2);
			batch.addAll(dsModel.getEntities());
			if(dsModel.getFictitiousEntity()!=null)
				batch.add(dsModel.getFictitiousEntity());
			
			boolean proof = true;
			while(proof){
				try{
					co.ds.put(batch);
					proof = false;
				}catch(ConcurrentModificationException ex){
					log.error(ex.getMessage()+"...retry");
				}
			}
		}
	}

	/* (non-Javadoc)
	 * @see it.polimi.hegira.persistence.IDatabase#update(it.polimi.hegira.models.Metamodel)
	 */
	@Override
	public void update(Metamodel mm) {
		DatastoreModel dsModel = fromMetamodel(mm);
		if(isConnected()){
			for(Entity ent : dsModel.getEntities()){
				int retries = 3;
				while(true){
					try {
						Entity oldEntity = co.ds.get(ent.getKey());
						co.ds.put(updateEntity(oldEntity, ent));
				        break;
					} catch (EntityNotFoundException e) {
						log.info("Update: Entity not found! Inserting it");
						insert(mm);
					} catch (ConcurrentModificationException e) {   
						if (retries == 0) {
							throw e;
					    }
					    // Allow retry to occur
					    --retries;
					}
					
				}
			}
		}

	}
	
	/**
	 * Check for changes (just in properties at the moment) between the two entities and performs the proper updates.
	 * Then, returns the updated version of the entity.
	 * @param oldE The old Entity retrieved from the Datastore inside a transaction.
	 * @param newE The new Entity as received from the commit-log.
	 * @return
	 */
	private Entity updateEntity(Entity oldE, Entity newE){
		Map<String, Object> newProps = newE.getProperties();
		Set<String> newKeys = newProps.keySet();
		for(String k : newKeys){
			Object newProp = newE.getProperty(k);
			boolean unidexed = newE.isUnindexedProperty(k);
			if(unidexed)
				oldE.setUnindexedProperty(k, newProp);
			else
				oldE.setProperty(k, newProp);
		}
		
		return oldE;
	}

	/* (non-Javadoc)
	 * @see it.polimi.hegira.persistence.IDatabase#delete(it.polimi.hegira.models.Metamodel)
	 */
	@Override
	public void delete(Metamodel mm) {
		if(mm==null) return;
		DatastoreModel dsModel = fromMetamodel(mm);
		if(isConnected()){
			//isConnected checked if co, co.ds are null
			for(Entity ent : dsModel.getEntities()){
				boolean proof = true;
				while(proof){
					try{
						co.ds.delete(ent.getKey());
						proof = false;
					}catch(ConcurrentModificationException ex){
						log.error(ex.getMessage()+"...retry");
					}
				}
			}
		}
	}
	
	@Override
	public String getName() {
		return Constants.GAE_DATASTORE;
	}
	
	protected List<String> getAllKinds(){
		if(isConnected()){
			Iterable<Entity> results = co.ds.prepare(new Query(Entities.KIND_METADATA_KIND)).asIterable();
			ArrayList<String> kinds = new ArrayList<String>();
		   	for(Entity globalStat : results){
		   		Key key2 = globalStat.getKey();
		   		String name = key2.getName();
			    	if(name.indexOf("_")!=0){
			    		kinds.add(name);
			    	}
		   	}
		   	return kinds;
		}else{
			log.error("{} - Not connected",
					Thread.currentThread().getName());
			return null;
		}
	}
}
