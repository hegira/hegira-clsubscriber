package it.polimi.hegira.persistence;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ThreadLocalRandom;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import it.polimi.hegira.exceptions.ConnectException;
import it.polimi.hegira.hegira_metamodel.Metamodel;
import it.polimi.hegira.utils.Constants;
import it.polimi.hegira.utils.DefaultSerializer;
import it.polimi.hegira.utils.PropertiesManager;

/**
 * Stub class representing a database connector.
 * It just logs each operation it performs.
 * @author Marco Scavuzzo
 */
public class VoidDatabase implements IDatabase {
	private static final transient Logger log = LoggerFactory.getLogger(VoidDatabase.class);
	private static final String propFile = "Void.properties";
	private String outputFileName;
	private Integer minSleepMs, maxSleepMs;
	private String storageType;
	
	private ConcurrentHashMap<String,byte[]> memStorage;
	private File outputFile;
	private FileOutputStream outFileStream;
	private ObjectOutputStream outObjFileStream;
	
	private String dbName;
	
	public VoidDatabase(String name) {
		this.dbName= (name!=null) ? name : "NULL";
		Map<String, String> confProps = PropertiesManager.getProperties(propFile, 
				Constants.VOID_MIN_PROPTIME_MS,
				Constants.VOID_MAX_PROPTIME_MS,
				Constants.VOID_STORAGE_TYPE);
		if(confProps!=null){
			Integer minSleepMs = null, maxSleepMs = null;
			try{
				minSleepMs = Integer.parseInt(confProps.get(Constants.VOID_MIN_PROPTIME_MS));
				maxSleepMs = Integer.parseInt(confProps.get(Constants.VOID_MAX_PROPTIME_MS));
			}catch(NumberFormatException e){
				log.warn("Unparsable value(s)");
			}
			
			String storageType = confProps.get(Constants.VOID_STORAGE_TYPE);
			
			validateInputs(minSleepMs, maxSleepMs, storageType);
			initStorage();
		}
	}

	private void validateInputs(Integer minSleepMs2, Integer maxSleepMs2, String storageType2) {
		this.minSleepMs=(minSleepMs2!=null && minSleepMs2>0) ? minSleepMs2.intValue() : 1;
		this.maxSleepMs=(maxSleepMs2!=null && maxSleepMs2>0) ? maxSleepMs2.intValue() : null;
		try{
			if(storageType2!=null)
				this.storageType=
						(Arrays.binarySearch(Constants.voidAllowedStorages.values(), Constants.voidAllowedStorages.valueOf(storageType2))>=0) ?
								storageType2 :
								Constants.voidAllowedStorages.NULL.name();
		}catch(IllegalArgumentException e){}
		log.debug("{} - Setting minimum_sleep_time_MS={}, max_sleep_time_MS={} and storage_type={}",
				Thread.currentThread().getName(),
				this.minSleepMs, this.maxSleepMs, this.storageType);
	}
	
	private void initStorage(){
		if(storageType==null || storageType.equals(Constants.voidAllowedStorages.NULL.name()))
			return;
		
		memStorage = new ConcurrentHashMap<String,byte[]>(100);
		if(storageType.equals(Constants.voidAllowedStorages.FILE.name())){
			this.outputFileName=this.dbName+"-output.txt";
			try {
				outputFile = PropertiesManager.createFileIfNotExists(PropertiesManager.getDefaultPath(), this.outputFileName);
				outFileStream = new FileOutputStream(outputFile);
				outObjFileStream = new ObjectOutputStream(outFileStream);
				outObjFileStream.writeObject(memStorage);
			} catch (NullPointerException | IOException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void connect() throws ConnectException {
		//log.debug("Connected");
	}

	@Override
	public void disconnect() {
		closeStorage();
	}

	@Override
	public void insert(Metamodel mm) {
		if(memStorage!=null){
			memStorage.put(mm.rowKey,
					getMD5(mm));
			applyToFile();
		}
		randomSleep();
		//log.debug("{} - Inserted entity with PK "+mm.getRowKey(),
		//		Thread.currentThread().getName());
	}

	@Override
	public void update(Metamodel mm) {
		if(memStorage!=null){
			memStorage.put(mm.rowKey,
					getMD5(mm));
			applyToFile();
		}
		randomSleep();
		//log.debug("{} - Updated entity with PK "+mm.getRowKey(),
		//		Thread.currentThread().getName());
	}

	@Override
	public void delete(Metamodel mm) {
		if(memStorage!=null){
			memStorage.remove(mm.rowKey);
			applyToFile();
		}
		randomSleep();
		//log.debug("{} - Deleted entity with PK "+mm.getRowKey(),
		//		Thread.currentThread().getName());
	}

	@Override
	public String getName() {
		return Constants.VOID;
	}

	@Override
	public boolean isConnected() {
		return true;
	}
	
	private void randomSleep(){
		if(minSleepMs==null || maxSleepMs==null)
			return;
		try {
			Thread.sleep(ThreadLocalRandom.current().nextInt(minSleepMs, maxSleepMs + 1));
		} catch (InterruptedException e) {
			log.error("",e);
		}
	}
	
	private void applyToFile() {
		if(outObjFileStream!=null){
			try {
				outObjFileStream.writeObject(memStorage);
				outObjFileStream.flush();
			} catch (IOException e) {
				log.error("{} - Cannot stream changes to file",
						Thread.currentThread().getName());
			}
		}
	}
	
	private void closeStorage(){
		if(outObjFileStream!=null){
			try {
				outObjFileStream.flush();
				outObjFileStream.close();
			} catch (IOException e) {}
		}
		
		if(outFileStream!=null){
			try {
				outFileStream.close();
			} catch (IOException e) {}
		}
	}
	
	private byte[] getMD5(Object obj){
		MessageDigest digest;
		try {
			digest = java.security.MessageDigest.getInstance("MD5");
			digest.update(DefaultSerializer.serialize(obj));
		    return digest.digest();
		} catch (NoSuchAlgorithmException e) {
			log.error("{} - Cannot find hash algorithm", Thread.currentThread().getName());
		} catch (IOException e) {
			log.error("{} - Error serializing the data", Thread.currentThread().getName());
		}
	    return null;
	}
}
