package it.polimi.hegira.persistence;

import java.security.InvalidParameterException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.datastax.driver.core.Session;
import com.datastax.driver.core.exceptions.AuthenticationException;
import com.datastax.driver.core.exceptions.NoHostAvailableException;

import it.polimi.hegira.exceptions.ConnectException;
import it.polimi.hegira.exceptions.OperationException;
import it.polimi.hegira.hegira_metamodel.Metamodel;
import it.polimi.hegira.hegira_metamodel.sync.Operations;
import it.polimi.hegira.models.CassandraModel;
import it.polimi.hegira.models.MetamodelOperationWrapper;
import it.polimi.hegira.persistence.cassandra.SessionManager;
import it.polimi.hegira.persistence.cassandra.Table;
import it.polimi.hegira.persistence.cassandra.TablesManager;
import it.polimi.hegira.transformers.CassandraTransformer;
import it.polimi.hegira.utils.Constants;
import it.polimi.hegira.utils.DefaultErrors;

/**
 * 
 * @author Marco Scavuzzo
 *
 */
public class Cassandra implements IDatabase {
	private static Logger log = LoggerFactory.getLogger(Cassandra.class);

	private class ConnectionObject{
		protected Session session;
		public ConnectionObject(){}
		public ConnectionObject(Session session){
			this();
			this.session=session;
		}
	}
	
	ConnectionObject co;
	Object co_lock = new Object();

	@Override
	public void connect() throws ConnectException {
		if(!isConnected()){		
			try{
				log.debug(Thread.currentThread().getName()+" - Logging into server");
				
				//retrieves the unique session from the session manager
				synchronized(co_lock){
					Session session=SessionManager.getSessionManager().getSession();
					this.co = new ConnectionObject(session);
				}
				
				log.debug(Thread.currentThread().getName()+" - Connected ");
			}catch(NoHostAvailableException | AuthenticationException | IllegalStateException ex){
				log.error(DefaultErrors.connectionError+"\nStackTrace:\n"+ex.getStackTrace());
				throw new ConnectException(DefaultErrors.connectionError);
			}
		}else{
			log.warn(DefaultErrors.alreadyConnected);
		}
	}

	@Override
	public void disconnect() {
		if(isConnected()){
			synchronized(co_lock){
				SessionManager.close();
				this.co.session = null;
				this.co=null;
			}
			log.debug(Thread.currentThread().getName() + " Disconnected");
		}else
			log.warn(DefaultErrors.notConnected);
	}

	@Override
	public boolean isConnected() {
		return (co!=null && co.session!=null) ? true : false;
	}
	
	private CassandraModel fromMetamodel(Metamodel mm){
		//instantiate the cassandra transformer
		//the consistency level is not needed. Entity inserted with eventual consistency
		CassandraTransformer transformer = new CassandraTransformer();
		CassandraModel cassandraModel = transformer.fromMyModel(mm);
		return cassandraModel;
	}

	@Override
	public void insert(Metamodel mm) throws OperationException {
		try {
			doOperation(mm, Operations.INSERT);
		} catch (InvalidParameterException | ClassNotFoundException e) {
			log.error("{} - Error performing {} operation",
					Thread.currentThread().getName(),
					Operations.INSERT,
					e);
			throw new OperationException(this, new MetamodelOperationWrapper(Operations.INSERT, mm));
		}
	}

	@Override
	public void update(Metamodel mm) throws OperationException {
		try {
			doOperation(mm, Operations.UPDATE);
		} catch (InvalidParameterException | ClassNotFoundException e) {
			log.error("{} - Error performing {} operation",
					Thread.currentThread().getName(),
					Operations.UPDATE,
					e);
			throw new OperationException(this, new MetamodelOperationWrapper(Operations.UPDATE, mm));
		}
	}

	@Override
	public void delete(Metamodel mm) throws OperationException {
		try {
			doOperation(mm, Operations.DELETE);
		} catch (InvalidParameterException | ClassNotFoundException e) {
			log.error("{} - Error performing {} operation",
					Thread.currentThread().getName(),
					Operations.DELETE,
					e);
			throw new OperationException(this, new MetamodelOperationWrapper(Operations.DELETE, mm));
		}
	}

	private void doOperation(Metamodel mm, Operations operation) throws InvalidParameterException, ClassNotFoundException{
		CassandraModel cassandraModel = fromMetamodel(mm);
		Table table;
		try {
			table = TablesManager.getTablesManager().getTable(cassandraModel.getTable());
		} catch (ConnectException e) {
			log.error(Thread.currentThread().getName() + " - Not able to connect to Cassandra", e);
			return;
		}
		if(table==null){
			log.error(Thread.currentThread().getName() + " - Not able to connect to Cassandra");
			return;
		}
		switch(operation){
			case INSERT:
				table.insert(cassandraModel);
				break;
			case DELETE:
				table.delete(cassandraModel);
				break;
			case UPDATE:
				table.update(cassandraModel);
				break;
			default:
				log.error("{} - Unrecognized operation: {}!",
						Thread.currentThread().getName(),
						operation.toString());
		}
	}
	
	@Override
	public String getName() {
		return Constants.CASSANDRA;
	}

}
