/**
 * 
 */
package it.polimi.hegira.persistence;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.HColumnDescriptor;
import org.apache.hadoop.hbase.HTableDescriptor;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.Admin;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.ConnectionFactory;
import org.apache.hadoop.hbase.client.Consistency;
import org.apache.hadoop.hbase.client.Delete;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.client.Table;
import org.apache.hadoop.hbase.util.Bytes;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import it.polimi.hegira.exceptions.ConnectException;
import it.polimi.hegira.exceptions.OperationException;
import it.polimi.hegira.hegira_metamodel.Metamodel;
import it.polimi.hegira.hegira_metamodel.sync.Operations;
import it.polimi.hegira.models.HBaseModel;
import it.polimi.hegira.models.HBaseModel.HBaseCell;
import it.polimi.hegira.persistence.hbase.SchemaCache;
import it.polimi.hegira.transformers.HBaseTransformer;
import it.polimi.hegira.utils.Constants;
import it.polimi.hegira.utils.DefaultErrors;
import it.polimi.hegira.utils.PropertiesManager;

/**
 * @author Marco Scavuzzo
 *
 */
public class HBase implements IDatabase {
	private static final Logger log = LoggerFactory.getLogger(HBase.class);
	private Configuration config;
	private Consistency consistencyType;
	
	private class ConnectionObject {
		public ConnectionObject() {
		}

		@SuppressWarnings("unused")
		public ConnectionObject(Connection connection) {
			this.connection = connection;
		}

		private Connection connection;
	}
	
	private ConnectionObject co;
	Object co_lock = new Object();
	
	public HBase(){
		initConfiguration();
		consistencyType = PropertiesManager.getHBaseConsistencyType();
	}
	
	private void initConfiguration() {
		config = HBaseConfiguration.create();
		/*String tmp = PropertiesManager.getZooKeeperConnectString();
		int i;
		if (tmp != null && tmp.length() > 0 && (i = tmp.indexOf(':')) > -1)
			config.set("hbase.zookeeper.quorum", tmp.substring(0, i));*/
		
		String hBaseQuorum = PropertiesManager.getHBaseQuorum();
		if(hBaseQuorum != null && hBaseQuorum.length() > 0){
			config.set("hbase.zookeeper.quorum", hBaseQuorum);
		}
		
		String zkCs = PropertiesManager.getZooKeeperConnectString();
		int i;
		if(zkCs != null && zkCs.length() > 0 && (i = zkCs.indexOf(":")) > -1){
			int end;
			int port;
			if((end = zkCs.indexOf(','))>-1){
				port = Integer.parseInt(zkCs.substring(i+1,end));
			}else{
				port = Integer.parseInt(zkCs.substring(i+1));
			}
			config.setInt("hbase.zookeeper.property.clientPort", port);
		}else{
			config.setInt("hbase.zookeeper.property.clientPort", 2181);
		}
	}
	
	@Override
	public synchronized void connect() throws ConnectException {
		if (!isConnected()) {
			try {
				this.co = new ConnectionObject();
				co.connection = ConnectionFactory.createConnection(config);
			} catch (IOException e) {
				log.error(DefaultErrors.connectionError+"\nStackTrace:\n"+e.getStackTrace());
				throw new ConnectException(DefaultErrors.connectionError);
			}
		}else{
			//log.warn(DefaultErrors.alreadyConnected);
		}
	}

	@Override
	public void disconnect() {
		if(isConnected()){
			if(co.connection!=null){
				try {
					co.connection.close();
				} catch (IOException e) {
					log.error("Error while disconnecting the connection.", e);
				}
			}
		}else{
			log.warn(DefaultErrors.notConnected);
		}
	}

	@Override
	public boolean isConnected() {
		return (co != null && co.connection != null && !co.connection
				.isClosed());
	}
	
	private HBaseModel fromMetamodel(Metamodel mm){
		HBaseTransformer transformer = new HBaseTransformer();
		return transformer.fromMyModel(mm);
	}
	
	private List<HBaseCell> getHBaseCells(Metamodel mm){
		return fromMetamodel(mm).getCells();
	}
	
	private List<HBaseCell> getHBaseCells(HBaseModel hbm){
		return hbm.getCells();
	}

	@Override
	public synchronized void insert(Metamodel mm) throws OperationException {
		doInsertOrUpdate(mm, Operations.INSERT);
	}

	@Override
	public synchronized void update(Metamodel mm) throws OperationException {
		doInsertOrUpdate(mm, Operations.UPDATE);
	}
	
	private void doInsertOrUpdate(Metamodel mm, Operations op) throws OperationException{
		List<HBaseCell> hBaseCells = getHBaseCells(mm);
		for(HBaseCell cell : hBaseCells){
			try {
				insertOrUpdateCell(cell);
			} catch (IOException e) {
				log.error("{} - Error performing {} operation",
						Thread.currentThread().getName(),
						op,e);
				throw new OperationException(this);
			}
		}
	}
	
	private void insertOrUpdateCell(HBaseCell cell) throws IOException{
		String tableKey = cell.getTableKey();
		String rowKey = cell.getRowKey();
		String columnKey = cell.getColumnKey();
		byte[] value = cell.getValue();
		Calendar timestamp = cell.getTimestamp();
		String[] cKeyParts = columnKey.split(":");
		String columnFamily = cKeyParts[0];
		String columnQualifier = cKeyParts[1];
		
		SchemaCache schemaCache = SchemaCache.getInstance();
		if(!schemaCache.containsTable(tableKey)){
			synchronized(this){
				createTable(tableKey, columnFamily);
				schemaCache.putColumnFamily(tableKey, columnFamily);
			}
		} else if(!schemaCache.containsColumnFamily(tableKey, columnFamily)){
			synchronized(this){
				createColumnFamily(tableKey, columnFamily);
				schemaCache.putColumnFamily(tableKey, columnFamily);
			}
		}
		
		//at this point both the table and the cf are present
		Put put = new Put(Bytes.toBytes(rowKey));

		try (Table table = getConnection().getTable(TableName.valueOf(tableKey))) {
			if (timestamp != null)
				put.addColumn(Bytes.toBytes(columnFamily), Bytes.toBytes(columnQualifier),
						timestamp.getTimeInMillis(), value);
			else
				put.addColumn(Bytes.toBytes(columnFamily), Bytes.toBytes(columnQualifier),
						value);
			table.put(put);
			table.close();
		}
	
	}

	@Override
	public synchronized void delete(Metamodel mm) throws OperationException {
		List<HBaseCell> hBaseCells = getHBaseCells(mm);
		for(HBaseCell cell : hBaseCells){
			String rowKey = cell.getRowKey();
			String tableKey = cell.getTableKey();
			boolean warnFirstDelete=false;
			if(!SchemaCache.getInstance().containsTable(tableKey)){
				log.warn("Received a DELETE operation on a table ({}) "
						+"that may not exist (i.e., is not in the cache)! The error that may follow "
						+"is expected", tableKey);
				warnFirstDelete=true;
			}
			Delete delete = new Delete(Bytes.toBytes(rowKey));
			try (Table table = getConnection().getTable(TableName.valueOf(tableKey))) {
				table.delete(delete);
				table.close();
			} catch (Exception e){
				log.error("{} - Error performing {} operation",
						Thread.currentThread().getName(),
						Operations.DELETE,e);
				if(!warnFirstDelete)
					throw new OperationException(this);
			}
		}
	}

	@Override
	public String getName() {
		return Constants.HBASE;
	}
	
	private void createColumnFamily(String tableKey, String columnFamily) throws IOException {
		try (Admin admin = getConnection().getAdmin()) {
			TableName name = TableName.valueOf(tableKey);
			HColumnDescriptor desc = new HColumnDescriptor(columnFamily);
			secureDisableTable(name, admin);
			admin.addColumn(name, desc);
			secureEnableTable(name, admin);
			admin.close();
		} 
	}

	private void createTable(String tableKey, String columnFamily) throws IOException {
		try (Admin admin = getConnection().getAdmin()) {
			TableName name = TableName.valueOf(tableKey);
			
			HColumnDescriptor desc = new HColumnDescriptor(columnFamily);
			if (!admin.tableExists(name)) {
				HTableDescriptor descTable = new HTableDescriptor(name);
				descTable.addFamily(desc);
				try {
					admin.createTable(descTable);
				} catch (org.apache.hadoop.hbase.TableExistsException e) {
					secureDisableTable(name, admin);
					
					try{
						admin.addColumn(name, desc);
					}catch(org.apache.hadoop.hbase.InvalidFamilyOperationException e1){
						log.debug("Column Family {} already exists for table {}", 
								desc.getNameAsString(), name);
					}
					
					secureEnableTable(name, admin);
				}
			}
			admin.close();
		}
	}

	private List<String> listColumnFamiliesNames(String tableKey)
			throws IOException {
		List<String> res = new ArrayList<String>();

		Connection connection = getConnection();

		try (Table table = connection.getTable(TableName.valueOf(tableKey))) {
			HTableDescriptor desc = table.getTableDescriptor();
			HColumnDescriptor[] families = desc.getColumnFamilies();
			for (HColumnDescriptor family : families)
				res.add(family.getNameAsString());
		} catch (Exception e) { }

		return res;
	}
	
	private void addColumnFamily(String tableKey, String columnKey)
			throws IOException {
		String[] parts = columnKey.split(":");
		
		List<String> columns = listColumnFamiliesNames(tableKey);
		if (columns.contains(parts[0]))
			return;
		
		Connection connection = getConnection();

		try (Admin admin = connection.getAdmin()) {
			TableName name = TableName.valueOf(tableKey);
			
			HColumnDescriptor desc = new HColumnDescriptor(parts[0]);
			if (!admin.tableExists(name)) {
				HTableDescriptor descTable = new HTableDescriptor(name);
				descTable.addFamily(desc);
				try {
					admin.createTable(descTable);
				} catch (org.apache.hadoop.hbase.TableExistsException e) {
					secureDisableTable(name, admin);
					
					try{
						admin.addColumn(name, desc);
					}catch(org.apache.hadoop.hbase.InvalidFamilyOperationException e1){
						log.debug("Column Family {} already exists for table {}", 
								desc.getNameAsString(), name);
					}
					
					secureEnableTable(name, admin);
				}
			} else {
				secureDisableTable(name, admin);
				admin.addColumn(name, desc);
				secureEnableTable(name, admin);
			}
		}
	}

	private void secureDisableTable(TableName name, Admin admin){
		try {
			do{
				try{
					admin.disableTable(name);
					Thread.sleep(1);
				}catch(Exception e1){
					log.debug("Unable to disable table {}. Retrying...", name);
				}
			}while(!admin.isTableDisabled(name));
		} catch (IOException e) {
			// in case also the check on table returns an error
			log.error("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n"
					+ "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n"
					+ "Cannot check if table {} is disabled. \n",name, e);
		}
	}
	
	private void secureEnableTable(TableName name, Admin admin){
		try {
			do{
				try{
					admin.enableTable(name);
				}catch(Exception e1){
					log.debug("Unable to enable table {}. Retrying...", name);
				}
			}while(admin.isTableDisabled(name));
		} catch (IOException e) {
			// in case also the check on table returns an error
			log.error("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n"
					+ "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n"
					+ "Cannot check if table {} is enabled. \n",name, e);
		}
	}
	
	private Connection getConnection(){
		return co.connection;
	}
}
