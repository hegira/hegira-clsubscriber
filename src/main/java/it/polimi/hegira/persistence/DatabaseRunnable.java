/**
 * 
 */
package it.polimi.hegira.persistence;

import it.polimi.hegira.exceptions.ConnectException;
import it.polimi.hegira.exceptions.OperationException;
import it.polimi.hegira.hegira_metamodel.Metamodel;
import it.polimi.hegira.hegira_metamodel.sync.Operations;
import it.polimi.hegira.models.MetamodelOperationWrapper;
import it.polimi.hegira.utils.DefaultErrors;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Marco Scavuzzo
 *
 */
public class DatabaseRunnable implements Runnable {
	private static final transient Logger log = LoggerFactory.getLogger(DatabaseRunnable.class);
	private IDatabase database;
	private CyclicBarrier startBarrier;
	private CyclicBarrier endBarrier;
	private ArrayBlockingQueue<MetamodelOperationWrapper> queue;
	private ArrayBlockingQueue<Metamodel> rollbackQueue;
	private boolean stop = false;
	
	public DatabaseRunnable(IDatabase database, CyclicBarrier startBarrier, CyclicBarrier endBarrier, ArrayBlockingQueue<MetamodelOperationWrapper> dstQueue){
		this.database = database;
		this.startBarrier = startBarrier;
		this.endBarrier = endBarrier;
		this.queue = dstQueue;
	}
	
	@Override
	public void run() {
		while(!stop){
			try {
				startBarrier.await();
				consumeOperation();
				endBarrier.await();
			} catch (InterruptedException | BrokenBarrierException e) {
				log.error("{} - Error with barriers. Stack Trace:\n{}",
						Thread.currentThread().getName(),e);
			}
		}
	}
	
	private void consumeOperation(){
		if(queue == null){
			log.error("{} - Error accessing the local operations queue.",
						Thread.currentThread().getName());
			return;
		}
		
		MetamodelOperationWrapper operation = queue.poll();
		
		if(operation!=null){
			switch(operation.getOperation()){
				case DELETE:
					doDelete(operation);
					break;
				case INSERT:
					doInsert(operation);
					break;
				case UPDATE:
					doUpdate(operation);
					break;
				default:
					log.error("{} - AbstractListener put an UNKNOWN operation in the DatabaseRunable: {} local queue.",
					Thread.currentThread().getName(),
					getDatabase().getName());
					return;
			}
		}
	}
	
	public void rollback(MetamodelOperationWrapper op){
		doDelete(op);
	}
	
	public void rollback(Metamodel mm){
		MetamodelOperationWrapper operation = new MetamodelOperationWrapper(Operations.DELETE, mm);
		doDelete(operation);
	}
	
	private void doInsert(MetamodelOperationWrapper operation){
		if(operation==null) return;
		try{
			if(!getDatabase().isConnected())
				getDatabase().connect();
			getDatabase().insert(operation.getMmEntity());
		} catch (ConnectException e) {
			log.error(DefaultErrors.connectionError, e);
			rollbackQueue.offer(operation.getMmEntity());
		} catch(NullPointerException e) {
			log.error("{} - Database wasn't set. Operation WON'T be propagated!",
						Thread.currentThread().getName(), e);
			rollbackQueue.offer(operation.getMmEntity());
			throw new IllegalStateException("Database wasn't set. Operation WON'T be propagated!");
		} catch(Exception e) {
			//can it be because of a connection timeout?
			if(e instanceof OperationException){
				((OperationException) e).setOperation(operation);
				log.error("{} - Got an error while performing {}."
						+ "Trying to disconnect, reconnect and perform the operation one more time",
						Thread.currentThread().getName(),
						e.toString(), e);
			}else{
				log.error("{} - Got an error while performing {} operation. "
						+ "Trying to disconnect, reconnect and perform the operation one more time",
						Thread.currentThread().getName(),
						Operations.INSERT, e);
			}
			getDatabase().disconnect();
			try {
				getDatabase().connect();
				getDatabase().insert(operation.getMmEntity());
			} catch (Exception e1) {
				log.error("{} - Got ANOTHER error while performing {} operation again on entity {}.",
						Thread.currentThread().getName(),
						Operations.INSERT, operation.getMmEntity().getRowKey(), e1);
				rollbackQueue.offer(operation.getMmEntity());
			}
		}
	}
	
	private void doUpdate(MetamodelOperationWrapper operation){
		if(operation==null) return;
		try{
			if(!getDatabase().isConnected())
				getDatabase().connect();
			getDatabase().update(operation.getMmEntity());
		} catch (ConnectException e) {
			log.error(DefaultErrors.connectionError, e);
			rollbackQueue.offer(operation.getMmEntity());
		} catch(NullPointerException e) {
			log.error("{} - Database wasn't set. Operation WON'T be propagated!",
					Thread.currentThread().getName(), e);
			rollbackQueue.offer(operation.getMmEntity());
			throw new IllegalStateException("Database wasn't set. Operation WON'T be propagated!");
		} catch(Exception e) {
			//can it be because of a connection timeout?
			if(e instanceof OperationException){
				((OperationException) e).setOperation(operation);
				log.error("{} - Got an error while performing {}."
						+ "Trying to disconnect, reconnect and perform the operation one more time",
						Thread.currentThread().getName(),
						e.toString(), e);
			}else{
				log.error("{} - Got an error while performing {} operation. "
						+ "Trying to disconnect, reconnect and perform the operation one more time",
						Thread.currentThread().getName(),
						Operations.UPDATE, e);
			}
			getDatabase().disconnect();
			try {
				getDatabase().connect();
				getDatabase().update(operation.getMmEntity());
			} catch (Exception e1) {
				log.error("{} - Got ANOTHER error while performing {} operation again on entity {}.",
						Thread.currentThread().getName(),
						Operations.UPDATE, operation.getMmEntity().getRowKey(), e1);
				rollbackQueue.offer(operation.getMmEntity());
			}
		}
	}
	
	
	
	private void doDelete(MetamodelOperationWrapper operation){
		if(operation==null) return;
		try{
			if(!getDatabase().isConnected())
				getDatabase().connect();
			getDatabase().delete(operation.getMmEntity());
		} catch (ConnectException e) {
			log.error(DefaultErrors.connectionError, e);
			rollbackQueue.offer(operation.getMmEntity());
		} catch(NullPointerException e) {
			log.error("{} - Database wasn't set. Operation WON'T be propagated!",
					Thread.currentThread().getName(), e);
			rollbackQueue.offer(operation.getMmEntity());
			throw new IllegalStateException("Database wasn't set. Operation WON'T be propagated!");
		} catch(Exception e) {
			//can it be because of a connection timeout?
			if(e instanceof OperationException){
				((OperationException) e).setOperation(operation);
				log.error("{} - Got an error while performing {}."
						+ "Trying to disconnect, reconnect and perform the operation one more time",
						Thread.currentThread().getName(),
						e.toString(), e);
			}else{
				log.error("{} - Got an error while performing {} operation. "
						+ "Trying to disconnect, reconnect and perform the operation one more time",
						Thread.currentThread().getName(),
						Operations.DELETE, e);
			}
			getDatabase().disconnect();
			try {
				getDatabase().connect();
				getDatabase().delete(operation.getMmEntity());
			} catch (Exception e1) {
				log.error("{} - Got ANOTHER error while performing {} operation again on entity {}.",
						Thread.currentThread().getName(),
						Operations.DELETE, operation.getMmEntity().getRowKey(), e1);
				rollbackQueue.offer(operation.getMmEntity());
			}
		}
	}

	public synchronized void stopThread(){
		log.info("{} - Stopping thread. The following errors are expected!",
						Thread.currentThread().getName());
		this.stop = true;
		int s_awaiting = startBarrier.getNumberWaiting();
		if(s_awaiting>0)
			startBarrier.reset();
		int e_awaiting = endBarrier.getNumberWaiting();
		if(e_awaiting>0)
			endBarrier.reset();
	}
	
	public void setDatabase(IDatabase database){
		this.database = database;
	}
	
	public void setRollbackQueue(ArrayBlockingQueue<Metamodel> q){
		this.rollbackQueue = q;
	}
	
	private IDatabase getDatabase(){
		return this.database;
	}
}
