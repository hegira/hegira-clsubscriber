/**
 * 
 */
package it.polimi.hegira.persistence;

import it.polimi.hegira.utils.Constants;

/**
 * Factory class which creates the proper Database class.
 * @author Marco Scavuzzo
 *
 */
public class DatabasesFactory {
	public static IDatabase[] createDatabasePair(String SourceDatabaseId, String DestDatabaseId){
		IDatabase[] pair = new IDatabase[2];
		switch(SourceDatabaseId){
			case Constants.GAE_DATASTORE:
				pair[0] = new Datastore();
				break;
			case Constants.AZURE_TABLES:
				pair[0] = new Tables();
				break;
			case Constants.VOID:
				pair[0] = new VoidDatabase("Source");
				break;
			case Constants.HBASE:
				pair[0] = new HBase();
				break;
			case Constants.CASSANDRA:
				pair[0] = new Cassandra();
				break;
			default:
				return null;
		}
		
		switch(DestDatabaseId){
			case Constants.GAE_DATASTORE:
				pair[1] = new Datastore();
				break;
			case Constants.AZURE_TABLES:
				pair[1] = new Tables();
				break;
			case Constants.VOID:
				pair[1] = new VoidDatabase("Target");
				break;
			case Constants.HBASE:
				pair[1] = new HBase();
				break;
			case Constants.CASSANDRA:
				pair[1] = new Cassandra();
				break;
			default:
				return null;
		}
		
		return pair;
	}
}
