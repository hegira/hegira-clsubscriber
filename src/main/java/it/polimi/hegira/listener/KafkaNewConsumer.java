package it.polimi.hegira.listener;

import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CyclicBarrier;

import org.apache.kafka.clients.consumer.CommitFailedException;
import org.apache.kafka.clients.consumer.ConsumerRebalanceListener;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.consumer.OffsetAndMetadata;
import org.apache.kafka.clients.consumer.OffsetCommitCallback;
import org.apache.kafka.common.TopicPartition;
import org.apache.kafka.common.errors.RebalanceInProgressException;
import org.apache.kafka.common.errors.WakeupException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import it.polimi.hegira.utils.PropertiesManager;

public class KafkaNewConsumer extends AbstractListener {
	private static Logger log = LoggerFactory.getLogger(KafkaNewConsumer.class);
	private final KafkaConsumer<String, byte[]> consumer;
	private final List<String> topics;
	protected ConcurrentHashMap<TopicPartition, OffsetAndMetadata> currentOffsets;
	private OffsetsManager offsetsManager;
	private Thread offsetManagerThread;
	private Properties kafkaProps;
	private long heartbeatRateMs=5000;
	
	public KafkaNewConsumer(String groupId, List<String> topics, String servers){
		super();
		this.topics = topics;
		initProperties(groupId, servers);
		this.currentOffsets = new ConcurrentHashMap<TopicPartition, OffsetAndMetadata>();
		this.consumer = new KafkaConsumer<>(kafkaProps);
	}
	
	private void initProperties(String groupId, String servers){
		kafkaProps = PropertiesManager.getPropertiesFile("kafka.properties");
		if(kafkaProps==null)
			kafkaProps = new Properties();
		kafkaProps.put("bootstrap.servers", servers);
		kafkaProps.put("group.id", groupId);
		if(kafkaProps.getProperty("max.poll.records")==null){
			kafkaProps.put("max.poll.records", 100);
		}
		kafkaProps.put("enable.auto.commit", "false");
		if(kafkaProps.getProperty("key.deserializer")==null){
			kafkaProps.put("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
		}
		if(kafkaProps.getProperty("value.deserializer")==null){
			kafkaProps.put("value.deserializer", "org.apache.kafka.common.serialization.ByteArrayDeserializer");
		}
		
		String maxPollStr = kafkaProps.getProperty("max.poll.interval.ms");
		if(maxPollStr!=null){
			try{
				long maxPoll = Long.parseLong(maxPollStr);
				this.heartbeatRateMs = maxPoll/2;
			}catch(NumberFormatException e){}
		}
		//should be greater than session.timeout and fetch.max.wait.ms (500 default)
		//props.put("request.timeout.ms", "285000");
		//props.put("session.timeout.ms", "280000");
		//props.put("heartbeat.interval.ms", "120000");
	}
	
	@Override
	public void consume() throws Exception {
		boolean startedConsuming = false;
		try{
			offsetsManager = new OffsetsManager(Thread.currentThread().getName());
			consumer.subscribe(topics, offsetsManager);
			
			offsetManagerThread = new Thread(offsetsManager);
			offsetManagerThread.start();
			
			while(true){
				//TODO: adjust value. See references
				try{
					ConsumerRecords<String, byte[]> records;
					/*long polling_iterations=0;
					do{
						if(polling_iterations>0 && startedConsuming){
							double reb_time=Math.exp(polling_iterations)*2000;
							Thread.sleep(heartbeatRateMs+(long)reb_time);
						}
						synchronized(consumer){
							if(startedConsuming)
								records = consumer.poll(heartbeatRateMs);
							else
								records = consumer.poll(Integer.MAX_VALUE);
						}
						log.debug("{} - Polling iterations: {}",
								Thread.currentThread().getName(),
								++polling_iterations);
					}while(records==null || records.isEmpty());*/
					
					
					if(!startedConsuming){
						//consumer just started, just wait for messages
						offsetsManager.stopHeartBeats(null);
						synchronized(consumer){
							records = consumer.poll(Integer.MAX_VALUE);
						}
						//startedConsuming=true;
					}else{
						int retries=0, max_retries=3;
						do{
							synchronized(consumer){
								records = consumer.poll((long)(heartbeatRateMs*1.7));
								//if nothing is received either nothing is produced or, apparently,
								//there's a bug in kafka that prevents messages to be shipped to consumers.
								//There are 2 workarounds based on forcing a group rebalance:
								//1. pausing the consumer long enough to trigger a rebalance
								//2. unsubscribing and re-subscribing the consumer to trigger a rebalance
								//
								//The first one may take too much time (several minutes) from computation. 
								//The second one (i.e.,this) should be faster.
								if(records==null || records.isEmpty()){
									consumer.unsubscribe();
									consumer.subscribe(topics, offsetsManager);
								}
							}
							Thread.sleep(100); //wait some time, for partitions reassignment, before polling again
							retries++;
						}while(records==null || records.isEmpty() || retries<max_retries);
						
						if(retries>=max_retries)
							startedConsuming=false;
					}
					
					offsetsManager.wakeUp(records.partitions());
					for(TopicPartition partition : records.partitions()){
						List<ConsumerRecord<String, byte[]>> partitionRecords = records.records(partition);
						for(ConsumerRecord<String, byte[]> record : partitionRecords) {
							processMessage(record.value());
							currentOffsets.put(partition,
		                               new OffsetAndMetadata(record.offset()+1));
							
							synchronized(consumer){
								consumer.commitAsync(currentOffsets, new OffsetCommitCallback(){
									@Override
									public void onComplete(Map<TopicPartition, OffsetAndMetadata> arg0,
											Exception arg1) {
										if(arg1!=null)
											log.error("Error commiting offsets",arg1);
									}
									
								});
							}
						}
						//after having processed the message, acknowledge the processing
						//long lastOffset = partitionRecords.get(partitionRecords.size()-1).offset();
						
						//consumer.commitSync(Collections.singletonMap(partition, 
						//		new OffsetAndMetadata(lastOffset+1)));
						
					}
					synchronized(consumer){
						consumer.commitSync(currentOffsets);
						currentOffsets.clear();
					}
					offsetsManager.stopHeartBeats(records.partitions());
					/*log.debug("{} - Trying to commit {} patitions offsets",
							Thread.currentThread().getName(),
							currentOffsets.size());
					synchronized(consumer){
						consumer.commitSync(currentOffsets);
					}
					log.debug("{} - Committed {} partitions offsets",
							Thread.currentThread().getName(),
							currentOffsets.size());
					currentOffsets.clear();*/
				}catch(CommitFailedException e){
					log.warn("{} - Commit failed. Rebalancing.",
							Thread.currentThread().getName(), e);
					//ensure the consumer is not paused
					offsetsManager.stopHeartBeats(null);
				} catch (RebalanceInProgressException e) {
		            log.warn("{} - Rebalance In Progress",
		            		Thread.currentThread().getName(), e);
		          //ensure the consumer is not paused
					offsetsManager.stopHeartBeats(null);
				}
			}
		} catch (WakeupException e) {
			// ignore for shutdown
			log.warn("{} - Received Wakeup exception",
		            	Thread.currentThread().getName());
			if(offsetsManager!=null)
				offsetsManager.stop();
		} finally {
			if(offsetsManager!=null)
				offsetsManager.stop();
			consumer.close();
		} 
	}

	@Override
	public void shutdown() {
		consumer.wakeup();
	}
	
	
	class OffsetsManager implements Runnable, ConsumerRebalanceListener {
		private final Logger log = LoggerFactory.getLogger(OffsetsManager.class);
		private String parentConsumerName;
		private boolean stop = false, pauseHeartbeats=false;
		private CyclicBarrier barrier;
		Collection<TopicPartition> currentPartitions;
		private Object hbWait;
		
		public OffsetsManager(String parentConsumerName){
			this.parentConsumerName = parentConsumerName;
			barrier = new CyclicBarrier(2);
			hbWait = new Object();
		}
		
		@Override
		public void run() {
			try {
				while(true && !stop){
					log.debug("{} - Waiting for partitions to process",
						parentConsumerName!=null ? parentConsumerName : "UNKNOWN");
					barrier.await();
					log.debug("{} - Sending heartbeats while processing {} partitions",
							parentConsumerName!=null ? parentConsumerName : "UNKNOWN",
							currentPartitions!=null ? currentPartitions.size()+"" : "0");
					while(!pauseHeartbeats){
						log.debug("{} - Sending HB", parentConsumerName);
						synchronized(consumer){
							consumer.poll(0);
						}
						//if stopping hbs in between...
						if(pauseHeartbeats) break;
						synchronized(hbWait){
							hbWait.wait(heartbeatRateMs);
						}
					}
					log.debug("{} - Stop sending heartbeats and resetting barrier",
							parentConsumerName!=null ? parentConsumerName : "UNKNOWN");	
					barrier.reset();
					
				}
			} catch (InterruptedException | BrokenBarrierException e) {
				if(currentPartitions!=null)
					stopHeartBeats(currentPartitions);
				e.printStackTrace();
			}
		}

		@Override
		public void onPartitionsAssigned(Collection<TopicPartition> partitions) {
			log.warn("{} - {} partitions are assigned to this consumer client: ",
					parentConsumerName!=null ? parentConsumerName : "UNKNOWN",
					partitions!=null ? partitions.size()+"" : "0",
					Arrays.toString(partitions.toArray()));
            Iterator<TopicPartition> topicPartitionIterator = partitions.iterator();
            while(topicPartitionIterator.hasNext()){
                TopicPartition topicPartition = topicPartitionIterator.next();
                log.warn("{} (onPartitionsAssigned) - Partition {} current offset is: {}, committed offset is: {}",
                		parentConsumerName!=null ? parentConsumerName : "UNKNOWN",
                		topicPartition.partition(),
                		consumer.position(topicPartition),
                		consumer.committed(topicPartition) );
            }
		}

		@Override
		public void onPartitionsRevoked(Collection<TopicPartition> partitions) {
			log.warn("{} - {} partitions were revoked from this consumer client: {}",
					parentConsumerName!=null ? parentConsumerName : "UNKNOWN",
					partitions!=null ? partitions.size()+"" : "0",
					Arrays.toString(partitions.toArray()));
			if(!pauseHeartbeats)
				stopHeartBeats(currentPartitions);
			if(currentOffsets!=null && !currentOffsets.isEmpty()){
				synchronized(consumer){
					try{
						consumer.commitSync(currentOffsets);
					}catch(CommitFailedException e){
						log.error("{} - Unable to commit currentOffsets onPartitionsRevoked!!",
								parentConsumerName!=null ? parentConsumerName : "UNKNOWN");
					}
				}
				currentOffsets.clear();
			}
		}
		
		public void stop(){
			this.stop=true;
		}
		
		public void wakeUp(Collection<TopicPartition> partitions){
			if(barrier.getNumberWaiting()<1)
				return;
			try {
				log.debug("{} - Pausing consumer from {} partitions",
						parentConsumerName,partitions.size());
				synchronized(consumer){
					consumer.pause(partitions);
				}
				pauseHeartbeats = false;
				this.currentPartitions = partitions;
				log.debug("{} - Waking up the thread", parentConsumerName);
				barrier.await();
			} catch (InterruptedException | BrokenBarrierException e) {
				synchronized(consumer){
					consumer.resume(partitions);
				}
			}
		}
		
		public synchronized void stopHeartBeats(Collection<TopicPartition> partitions){
			pauseHeartbeats = true;
			synchronized(hbWait){
				hbWait.notify();
			}
			//waiting for the thread to stop sending acks and to reset the barrier
			/*while(barrier.getNumberWaiting()!=1){
				try{
					Thread.sleep(10);
				}catch(Exception e){}
			}*/
			try{
				if(partitions!=null){
					synchronized(consumer){
						consumer.resume(partitions);
					}
					log.debug("{} - Unpausing {} partitions.",
						parentConsumerName!=null ? parentConsumerName : "UNKNOWN",
						partitions!=null ? partitions.size()+"" : "0");
				}else if(currentPartitions!=null){
					synchronized(consumer){
						consumer.resume(currentPartitions);
					}
					log.debug("{} - Unpausing {} partitions.",
						parentConsumerName!=null ? parentConsumerName : "UNKNOWN",
						currentPartitions!=null ? currentPartitions.size()+"" : "0");
				}
			}catch(IllegalStateException e){
				//cannot resume the partitions
			}
			this.currentPartitions = null;
		}
	}
}
