/**
 * 
 */
package it.polimi.hegira.listener;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.atomic.AtomicBoolean;

import org.apache.log4j.Logger;

//import kafka.consumer.Consumer;
//import kafka.consumer.ConsumerConfig;
//import kafka.consumer.ConsumerIterator;
//import kafka.consumer.KafkaStream;
//import kafka.javaapi.consumer.ConsumerConnector;
import it.polimi.hegira.utils.Constants;
import it.polimi.hegira.utils.PropertiesManager;
import it.polimi.hegira.zkWrapper.util.SubscriberType;

/**
 * @author Marco Scavuzzo
 *
 */
public class KafkaHLconsumer extends AbstractListener{
//	private static Logger log = Logger.getLogger(KafkaHLconsumer.class);
//	private ConsumerConnector consumer;
//	private String topic;
//	private String zkConnectString;
//	private AtomicBoolean stop = new AtomicBoolean(false);
//	//TODO: passare ZKobject???
//	/**
//	 * Instantiates a new High-level Kafka Consumer, with default ZooKeeper parameters (except the host).
//	 * @param zookeeper 	ZooKeeper connect string
//	 * @param groupId	The groupId for the consumer (currently only one is supported)
//	 * @param topic		The messages topic 
//	 */
	public KafkaHLconsumer(String zookeeper, String groupId,
			String topic) {
//		super();
//		Properties props = PropertiesManager.getPropertiesFile(Constants.ZK_PATH);
//		if(props!=null){
//			props.put("zookeeper.connect", zookeeper);
//			props.put("group.id", groupId);
//			props.put("autooffset.reset", "smallest");
//			props.put("zookeeper.connection.timeout.ms", "6000");
//		}else{
//			log.error("Cannot read file "+Constants.ZK_PATH );
//			return;
//		}
//		consumer = Consumer.createJavaConsumerConnector(new ConsumerConfig(props));
//		this.topic = topic;
//		this.zkConnectString = zookeeper;
	}
//
//	/**
//	 * Consumes the messages for the topic assigned to this instance of the object
//	 * @throws UnsupportedEncodingException 
//	 */
	public void consume() throws UnsupportedEncodingException{
//		HashMap<String, Integer> topicCount = new HashMap<String, Integer>(1);
//		//K - topic name
//		//V - threads number
//		topicCount.put(topic, new Integer(1));
//		
//		Map<String, List<KafkaStream<byte[], byte[]>>> consumerStreams = 
//				consumer.createMessageStreams(topicCount);
//		
//		List<KafkaStream<byte[], byte[]>> streams = consumerStreams.get(topic);
//		
//		for(final KafkaStream stream : streams){
//			ConsumerIterator consumerIterator = stream.iterator();
//			while(consumerIterator.hasNext() && !stop.get()){
//				//TODO: change!!
//				//String message = new String((byte[]) consumerIterator.next().message(),"UTF-8");
//				//log.debug("Consumed message: " + message);
//				//processMessage(message, type);
//				//log.debug("Got message from Kafka");
//				processMessage(consumerIterator.next().message());
//			}
//			if(consumer!=null)
//				consumer.shutdown();
//		}
	}
	
	@Override
	public void shutdown() {
//		stop.set(true);
	}

}
