/**
 * 
 */
package it.polimi.hegira.listener;

import java.io.Closeable;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Set;
import java.util.AbstractMap.SimpleEntry;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.tuple.MutablePair;
import org.apache.thrift.TDeserializer;
import org.apache.thrift.TException;
import org.apache.thrift.protocol.TBinaryProtocol;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import it.polimi.hegira.coordination.ICoordinator;
import it.polimi.hegira.dlm.ILockManager;
import it.polimi.hegira.exceptions.CacheLockException;
import it.polimi.hegira.exceptions.LockedException;
import it.polimi.hegira.hegira_metamodel.Metamodel;
import it.polimi.hegira.hegira_metamodel.sync.MsgWrapper;
import it.polimi.hegira.models.MetamodelOperationWrapper;
import it.polimi.hegira.notifications.AbstractNotificationManager;
import it.polimi.hegira.notifications.OperationResponse;
import it.polimi.hegira.notifications.OperationResponse.OperationResult;
import it.polimi.hegira.persistence.DatabaseRunnable;
import it.polimi.hegira.persistence.IDatabase;
import it.polimi.hegira.queries.IQueryInterpreter;
import it.polimi.hegira.zkWrapper.util.SubscriberType;

/**
 * Class representing a generic commit-log listener, created by the Factory: {@link it.polimi.hegira.listener.ListenersFactory}.
 * <br>It uses the template pattern to provide a standard way of processing a message/query got from the cl.
 * <br>The template method is {@link it.polimi.hegira.listener.AbstractListener#processMessage(String, SubscriberType)}.
 * @author Marco Scavuzzo
 *
 */
public abstract class AbstractListener implements Runnable, Closeable {
	private static final transient Logger log = LoggerFactory.getLogger(AbstractListener.class);
	/**
	 * Private Interface/Abstract class Fields
	 * representing the layers that are called by the template method: processMessage 
	 */
	private ICoordinator coordinator;
	private IQueryInterpreter queryInterpreter;
	private IDatabase[] databasePair;
	private TDeserializer deserializer = new TDeserializer(new TBinaryProtocol.Factory());
	
	private CyclicBarrier startBarrier;
	private CyclicBarrier endBarrier;
	private ArrayBlockingQueue<MetamodelOperationWrapper> srcQueue;
	private ArrayBlockingQueue<MetamodelOperationWrapper> dstQueue;
	private ArrayBlockingQueue<Metamodel> srcQueueRollback;
	private ArrayBlockingQueue<Metamodel> dstQueueRollback;
	private DatabaseRunnable srcRunnable;
	private DatabaseRunnable dstRunnable;
	private Thread srcThread;
	private Thread dstThread;
	
	private ILockManager<Integer> entriesLockManager;	
	
	private AbstractNotificationManager<OperationResponse, String> dmqsNotificationManager;
	
	private MutablePair<String, LinkedList<MsgWrapper>> currentTx;
	
	private boolean disableDlm = false;
	
	public AbstractListener(){
		this.startBarrier = new CyclicBarrier(3);
		this.endBarrier = new CyclicBarrier(3);
		this.srcQueue = new ArrayBlockingQueue<MetamodelOperationWrapper>(11);
		this.dstQueue = new ArrayBlockingQueue<MetamodelOperationWrapper>(11);
		this.srcRunnable = new DatabaseRunnable(databasePair!=null ? databasePair[0] : null, startBarrier, endBarrier, srcQueue);
		this.dstRunnable = new DatabaseRunnable(databasePair!=null ? databasePair[1] : null, startBarrier, endBarrier, dstQueue);
		
		this.srcQueueRollback = new ArrayBlockingQueue<Metamodel>(11);
		this.dstQueueRollback = new ArrayBlockingQueue<Metamodel>(11);
		srcRunnable.setRollbackQueue(srcQueueRollback);
		dstRunnable.setRollbackQueue(dstQueueRollback);
		
		srcThread = new Thread(srcRunnable);
		dstThread = new Thread(dstRunnable);
		srcThread.start();
		dstThread.start();
	}
	
	public abstract void consume() throws Exception;
	
	@Override
	public void run() {
		try {
			consume();
		} catch (Exception e) {
			log.error("{} - Error consuming from the commit log.",
					Thread.currentThread().getName(),
					e);
		}
	}
	
	/**
	 * Template method to be called by the specific consume method.
	 * It gets the message and passes it to the other layers (i.e., Coordination Layer,
	 * Interpretation Layer, etc..).
	 * @param message The message read from the generic commit-log.
	 */
	protected void processMessage(Object message){
		if(startBarrier==null || endBarrier == null || srcQueue == null || dstQueue == null
				|| srcRunnable == null || dstRunnable == null || srcThread == null || dstThread == null){
			throw new IllegalStateException("The concrete listener hasn't properly instantiated the AbstractListener. Call super()");
		}
		MsgWrapper msgObj = queryInterpreter.createMessage(message);
		if(!msgObj.isTx())
			handleSingleMessage(msgObj, disableDlm);
		else
			handleTransaction(msgObj);
	}
	
	private void handleTransaction(MsgWrapper msgObj){
		if(currentTx==null)
			currentTx = new MutablePair<String, LinkedList<MsgWrapper>>();
		
		if(msgObj.getTxId().equals(currentTx.getKey())){
			//it is not the first operation of the transaction
			//add operation to the queue
			currentTx.getValue().add(msgObj);
		}else{
			//it is the first operation of a new transaction.
			//store the details of the operation, waiting for other ops.
			currentTx.setLeft(msgObj.getTxId());
			LinkedList<MsgWrapper> ops = new LinkedList<MsgWrapper>();
			ops.add(msgObj);
			currentTx.setRight(ops);
		}
		
		if(msgObj.isLastMsgInTx()){
			//the tx is over, so commit it...
			//(gets the proper locks atomically and, after propagating the tx, release them)
			commitTx();
		}
	}
	
	private void commitTx(){
		LinkedList<MsgWrapper> operations = currentTx.right;
		HashMap<String, SimpleEntry<String,Integer>> tbls_Ids = new HashMap<String, SimpleEntry<String,Integer>>();
		ArrayList<Metamodel> mmList = new ArrayList<Metamodel>(operations.size());
		for(MsgWrapper op : operations){
			Metamodel mm = new Metamodel();
			try {
				deserializer.deserialize(mm, op.getContent());
				mmList.add(mm);
				Set<String> cfs = mm.getColumns().keySet();
				for(String tbl : cfs){
					SimpleEntry<String, Integer> entry = new SimpleEntry<String, Integer>(tbl, Integer.parseInt(mm.getRowKey()));
					tbls_Ids.put(tbl+mm.getRowKey(),entry);
				}
			} catch (TException e) {
				log.error("Couldn't deserialize message from Kafka", e);
				Boolean[] other = new Boolean[2];
				other[0]=false;other[1]=false;
				StringBuilder sb = new StringBuilder("Column families: ");
				for(String tbl:mm.getColumnFamilies())
					sb.append(tbl+".");
				sendNotification(op, other, mm.rowKey,
						sb.substring(0, sb.length()-1), OperationResult.FAIL);
			}
		}
		//acquiring required locks
		log.debug("{} - Trying to acquire {} atomic locks for TX {}",
				Thread.currentThread().getName(),operations.size(),currentTx.left);
		boolean acquired = false;
		while(!acquired){
			try{
				acquired=entriesLockManager.acquireLocks(currentTx.left, tbls_Ids, 1L, TimeUnit.SECONDS);
			}catch (IllegalStateException e){
				try {
					Thread.sleep(100);
				} catch (InterruptedException ex) {
					ex.printStackTrace();
				}
			}
		}
		
		int opSize = operations.size();
		if(acquired){
			for(Metamodel mmOp : mmList){
				MsgWrapper msgObj = findWrapper(operations, mmOp.getRowKey());
				Boolean[] canSynchronize;
				Set<String> cfs = mmOp.getColumns().keySet();
				for(String tbl : cfs){
					try{
						
						MetamodelOperationWrapper metamodelOperation = new MetamodelOperationWrapper(msgObj.getOperation(), mmOp);
						
						// check the partition Status. If NOT_MIGRATED we would need a lock on the partition 
						// which should be released after the status changed back from synchronized to not_migrated
						//(done inside the coordinator in method canSynchronize and released in finish_sync) 
						
						//check on which component we can propagate the operation
						//internally waits if the status is UNDER_MIGRATION
						canSynchronize = coordinator.canSynchronize(new Integer(mmOp.rowKey), tbl);
						
						if(canSynchronize!=null){
							log.debug("{} - Can synchronize PK {}/{} on SRC:{}, DST:{}. Properly propagating...",
									Thread.currentThread().getName(),
									tbl, mmOp.rowKey,
									canSynchronize[0]!=null && canSynchronize[0] ? "true" : "false",
									canSynchronize[1]!=null && canSynchronize[1] ? "true" : "false");
							
							boolean opRes = doOperation(metamodelOperation, canSynchronize);
							if(opRes)
								sendNotification(msgObj, canSynchronize, mmOp.rowKey, tbl, OperationResult.SUCCESS);
							else
								sendNotification(msgObj, canSynchronize, mmOp.rowKey,tbl, OperationResult.FAIL);
							
							
							coordinator.notifyFinishSync(new Integer(mmOp.getRowKey()), tbl);
						}else{
							log.error("{} - CanNOT synchronize on PK "+mmOp.rowKey+". Dropping....",
									Thread.currentThread().getName());
							sendNotification(msgObj, canSynchronize, mmOp.rowKey,tbl, OperationResult.FAIL);
						}
					} catch(LockedException e){
						log.debug("Queries are locked. Waiting ....");
					} catch (InterruptedException e) {
						log.error("{} - Barrier error. Stack Trace:\n{}",
								Thread.currentThread().getName(),e);
						Boolean[] other = new Boolean[2];
						other[0]=false;other[1]=false;
						srcRunnable.rollback(mmOp);
						dstRunnable.rollback(mmOp);
						sendNotification(msgObj, other, mmOp.rowKey, tbl, OperationResult.FAIL);
					} catch (BrokenBarrierException e) {
						log.error("{} - Barrier error. Stack Trace:\n{}",
								Thread.currentThread().getName(),e);
						Boolean[] other = new Boolean[2];
						other[0]=false;other[1]=false;
						srcRunnable.rollback(mmOp);
						dstRunnable.rollback(mmOp);
						sendNotification(msgObj, other, mmOp.rowKey, tbl, OperationResult.FAIL);
					} 
				}
			}
			//release locks
			log.debug("{} - Trying to release {} atomic locks for TX {}",
					Thread.currentThread().getName(),opSize,currentTx.left);
			try {
				entriesLockManager.releaseLocks(currentTx.left);
			} catch (CacheLockException e) {
				log.error("{} - Unable to release locks for TX {}. Possible DEADLOCK!",
						Thread.currentThread().getName(), currentTx.left);
			}
		}else{
			log.error("{} - Unable to acquire locks for TX {}.",
					Thread.currentThread().getName(), currentTx.left);
		}
	}
	
	private MsgWrapper findWrapper(LinkedList<MsgWrapper> operations, String rowKey) {
		for(MsgWrapper op : operations){
			if(op.getOpId().equals(rowKey)){
				operations.remove(op);
				return op;
			}
		}
		return null;
	}

	private void handleSingleMessage(MsgWrapper msgObj, boolean disableDlm){
		Boolean[] canSynchronize;
		Metamodel mm = new Metamodel();
		try {
			deserializer.deserialize(mm, msgObj.getContent());
		
			Set<String> cfs = mm.getColumns().keySet();
			for(String tbl : cfs){
				try{
					
					MetamodelOperationWrapper metamodelOperation = new MetamodelOperationWrapper(msgObj.getOperation(), mm);
					if(!disableDlm){
						// get the lock for the operation from the LockManager
						boolean acquired=false;
						while(!acquired){
							try{
								acquired=entriesLockManager.acquireLock(tbl, Integer.parseInt(metamodelOperation.getLockId()), 1L, TimeUnit.SECONDS);
							}catch (IllegalStateException e){
								try {
									Thread.sleep(100);
								} catch (InterruptedException ex) {
									ex.printStackTrace();
								}
							}
						}
					}
					// check the partition Status. If NOT_MIGRATED we would need a lock on the partition 
					// which should be released after the status changed back from synchronized to not_migrated
					//(done inside the coordinator in method canSynchronize and released in finish_sync) 
					
					//check on which component we can propagate the operation
					//internally waits if the status is UNDER_MIGRATION
					canSynchronize = coordinator.canSynchronize(new Integer(mm.rowKey), tbl);
					
					if(canSynchronize!=null){
						log.debug("{} - Can synchronize PK {}/{} on SRC:{}, DST:{}. Properly propagating...",
								Thread.currentThread().getName(),
								tbl, mm.rowKey,
								canSynchronize[0]!=null && canSynchronize[0] ? "true" : "false",
								canSynchronize[1]!=null && canSynchronize[1] ? "true" : "false");
						
						boolean opRes = doOperation(metamodelOperation, canSynchronize);
						if(opRes)
							sendNotification(msgObj, canSynchronize, mm.rowKey, tbl, OperationResult.SUCCESS);
						else
							sendNotification(msgObj, canSynchronize, mm.rowKey,tbl, OperationResult.FAIL);
						
						if(!disableDlm){
							try {
								boolean released = entriesLockManager.releaseLock(tbl,
										Integer.parseInt(metamodelOperation.getLockId()));
								if(!released)
									log.error("{} - UNABLE to release Operation lock {}/eid:{}.",
											Thread.currentThread().getName(),
											mm.rowKey,
											tbl,
											metamodelOperation.getLockId());
							} catch (CacheLockException e) {
								log.error("{} - Unable to release lock on entry {}. Possible DEADLOCK!",
										Thread.currentThread().getName(), mm.rowKey);
							}
						}
						coordinator.notifyFinishSync(new Integer(mm.getRowKey()), tbl);
					}else{
						log.error("{} - CanNOT synchronize on PK "+mm.rowKey+". Dropping....",
								Thread.currentThread().getName());
						sendNotification(msgObj, canSynchronize, mm.rowKey,tbl, OperationResult.FAIL);
					}
				} catch(LockedException e){
					log.debug("Queries are locked. Waiting ....");
				} catch (InterruptedException e) {
					log.error("{} - Barrier error. Stack Trace:\n{}",
							Thread.currentThread().getName(),e);
					Boolean[] other = new Boolean[2];
					other[0]=false;other[1]=false;
					srcRunnable.rollback(mm);
					dstRunnable.rollback(mm);
					sendNotification(msgObj, other, mm.rowKey, tbl, OperationResult.FAIL);
				} catch (BrokenBarrierException e) {
					log.error("{} - Barrier error. Stack Trace:\n{}",
							Thread.currentThread().getName(),e);
					Boolean[] other = new Boolean[2];
					other[0]=false;other[1]=false;
					srcRunnable.rollback(mm);
					dstRunnable.rollback(mm);
					sendNotification(msgObj, other, mm.rowKey, tbl, OperationResult.FAIL);
				} 
			}
		} catch (TException e1) {
			log.error("Couldn't deserialize message from Kafka", e1);
			Boolean[] other = new Boolean[2];
			other[0]=false;other[1]=false;
			StringBuilder sb = new StringBuilder("Column families: ");
			for(String tbl:mm.getColumnFamilies())
				sb.append(tbl+".");
			sendNotification(msgObj, other, mm.rowKey,
					sb.substring(0, sb.length()-1), OperationResult.FAIL);
		} 
	}
	
	private void sendNotification(MsgWrapper msgObj, Boolean[] canSynchronize, String pk, String tbl, OperationResult res){
		if(msgObj==null || canSynchronize==null || tbl==null || res==null)
			throw new IllegalArgumentException("Paramethers cannot be null");
		if(dmqsNotificationManager!=null){
			OperationResponse response = new OperationResponse(
					msgObj.getOpId(), tbl, 
					canSynchronize[0].booleanValue(), canSynchronize[1].booleanValue(),
					msgObj.getOperation().name(), res, msgObj.getTimestamp());
			if(res.equals(OperationResult.SUCCESS)){
				dmqsNotificationManager.sendAck(msgObj.getInstanceId(),
						msgObj.getTxId(), 
						response);
			}else if(res.equals(OperationResult.FAIL)){
				dmqsNotificationManager.sendNack(msgObj.getInstanceId(),
						msgObj.getTxId(),
						response);
			}
		}
	}

	private boolean doOperation(MetamodelOperationWrapper metamodelOperation, Boolean[] canSynchronize) throws InterruptedException, BrokenBarrierException{
		//each Database Thread has its own queue containing the next operation to propagate
		//if the given operation can be propagated by the respective thread, the operation is added to its queue.
		
		boolean returnValue=true;
		
		if(canSynchronize[0]!=null && canSynchronize[0]){
			srcQueue.add(metamodelOperation);
		}
		
		if(canSynchronize[1]!=null && canSynchronize[1]){
			dstQueue.add(metamodelOperation);
		}
		
		//awakening waiting DatabaseRunnable threads (they're waiting for the startBarrier to be released)
		startBarrier.await();
		//... while the threads decide if and how to propagate their operation,
		//lets reset the first cyclic barrier so that threads will wait in next cycle (see DatabaseRunnable class).
		startBarrier.reset();
		//... after all threads have performed their operation they will be blocked in the endBarrier;
		//by calling await threads can continue their loop, i.e., waiting on the startBarrier again.
		endBarrier.await();
		//endBarrier needs to be reset for the next cycle in the loop
		endBarrier.reset();
		
		Metamodel srcFailed = srcQueueRollback.poll();
		if(srcFailed!=null){
			dstRunnable.rollback(srcFailed);
			log.warn("{} - Source database failed propagating operation {}/{}. Rolling back on the Destination database",
					Thread.currentThread().getName(),
					srcFailed.columnFamilies.get(0),srcFailed.rowKey);
			returnValue=false;
		}
		
		Metamodel dstFailed = dstQueueRollback.poll();
		if(dstFailed!=null){
			srcRunnable.rollback(dstFailed);
			log.warn("{} - Destination database failed propagating operation {}/{}. Rolling back on the Source database",
					Thread.currentThread().getName(),
					dstFailed.columnFamilies.get(0),dstFailed.rowKey);
			returnValue=false;
		}
		
		return returnValue;
	}
	
	/**
	 * @param coordinator the coordinator to set
	 */
	public void setCoordinator(ICoordinator coordinator) {
		this.coordinator = coordinator;
	}

	public void disconnectCoordinator() {
		if(coordinator!=null)
			coordinator.disconnect();
	}
	
	/**
	 * @param queryInterpreter the queryInterpreter to set
	 */
	public void setQueryInterpreter(IQueryInterpreter queryInterpreter) {
		this.queryInterpreter = queryInterpreter;
	}

	/**
	 * Sets the databases involved in the synchronization process.
	 * The database in position 0 should be the SOURCE database.
	 * The database in position 1 should be the DESTINATION database.
	 * The databases are set both in the AbstractListener and in the respective {@link DatabaseRunnable}.
	 * @param databasePair the database to set
	 */
	public void setDatabase(IDatabase[] databasePair) {
		log.info("{} - Database pair changed from {},{} to {},{}",
				Thread.currentThread().getName(),
				this.databasePair != null && this.databasePair[0] != null ? this.databasePair[0].getName() : "NULL",
				this.databasePair != null && this.databasePair[1] != null ? this.databasePair[1].getName() : "NULL",
				databasePair != null && databasePair[0] != null ? databasePair[0].getName() : "NULL",
				databasePair != null && databasePair[1] != null ? databasePair[1].getName() : "NULL");
		this.databasePair = databasePair;
		
		if(this.srcRunnable!=null)
			this.srcRunnable.setDatabase(databasePair[0]);
		else
			log.error("{} - Couldn't set database {} in Source DatabaseRunnable",
							Thread.currentThread().getName(), databasePair[0].getName());
		
		if(this.dstRunnable!=null)
			this.dstRunnable.setDatabase(databasePair[1]);
		else
			log.error("{} - Couldn't set database {} in Destination DatabaseRunnable",
					Thread.currentThread().getName(), databasePair[1].getName());
	}
	
	/**
	 * @param entriesLockManager the entriesLockManager to set
	 */
	public synchronized void setEntriesLockManager(ILockManager<Integer> entriesLockManager) {
		this.entriesLockManager = entriesLockManager;
	}

	public void setDMQsNotificationManager(AbstractNotificationManager<OperationResponse, String> notificationManager) {
		this.dmqsNotificationManager = notificationManager;
	}

	public void setDisableDlm(boolean disableDlm) {
		this.disableDlm = disableDlm;
	}

	public abstract void shutdown();
	
	@Override
	public void close() throws IOException {
		if(srcRunnable!=null)
			srcRunnable.stopThread();
		if(dstRunnable!=null)
			dstRunnable.stopThread();
	}
}
