/**
 * 
 */
package it.polimi.hegira.listener;

import java.util.Arrays;

import it.polimi.hegira.utils.CLI;
import it.polimi.hegira.utils.Constants;

/**
 * Factory class which creates the proper commit-log listener.
 * @author Marco Scavuzzo
 */
public class ListenersFactory {
	public static AbstractListener createListener(ListenerNames listenerId, CLI cli){
		switch(listenerId){
			case KAFKA:
				return new KafkaHLconsumer(cli.zkConnectString, Constants.CONSUMER_GROUPS.ISOLATION.name(),
						Constants.SYNC_TOPIC);
			case NEWKAFKA:
				KafkaNewConsumer cons = new KafkaNewConsumer(Constants.CONSUMER_GROUPS.hegira.name(), 
						Arrays.asList(Constants.SYNC_TOPIC), cli.kafkaString);
				cons.setDisableDlm(cli.disableDlm);
				return cons;
		}
		return null;
	}
	
	public enum ListenerNames {
		KAFKA, NEWKAFKA
	}
}
