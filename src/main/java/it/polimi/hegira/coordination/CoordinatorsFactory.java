/**
 * 
 */
package it.polimi.hegira.coordination;

import it.polimi.hegira.utils.CLI;
import it.polimi.hegira.utils.PropertiesManager;

/**
 * Factory class which creates the proper lock-manager client (e.g. ZooKeeper client)
 * @author Marco Scavuzzo
 */
public class CoordinatorsFactory {
	public static ICoordinator createCoordinator(CoordinatorNames coordinatorId, CLI cli){
		switch(coordinatorId){
			case ZOOKEEPER:
				if(getZkServerVersion()<2)
					return ZKcoordinator.getInstance(cli.zkConnectString, cli.migration_id);
				else
					return ZKcoordinator2.getInstance(cli.zkConnectString, cli.migration_id);
			default:
				return null;
		}
	}
	
	public enum CoordinatorNames {
		ZOOKEEPER
	}
	
	private static int getZkServerVersion(){
		String zkSerVersStr = PropertiesManager.getCredentialsProperty("zkServer.version");
		if(zkSerVersStr==null)
			return 1;
		try{
			int zkSerVers = Integer.parseInt(zkSerVersStr);
			return zkSerVers;
		}catch(NumberFormatException e){
			return 1;
		}
	}
}
