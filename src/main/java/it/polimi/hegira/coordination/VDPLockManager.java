/**
 * 
 */
package it.polimi.hegira.coordination;

import java.io.IOException;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import it.polimi.hegira.zkWrapper.VDPmigrationStatus;
import it.polimi.hegira.zkWrapper.ZKserver2;

/**
 * @author Marco Scavuzzo
 *
 */
public class VDPLockManager {
	//Key=Path, Value=SharedVDPLock
	private ConcurrentHashMap<String,SharedVDPLock> vdplocks;
	private ZKserver2 zKserver;
	private Logger log = LoggerFactory.getLogger(VDPLockManager.class);
	private Integer _lockReuseThreshold;
	
	public VDPLockManager(ZKserver2 zKserver, Integer expectedVDPs, Integer lockReuseThreshold) {
		this.zKserver=zKserver;
		vdplocks=new ConcurrentHashMap<String,SharedVDPLock>(expectedVDPs!=null && expectedVDPs>0?expectedVDPs:10);
		this._lockReuseThreshold=lockReuseThreshold;
	}
	
	public boolean acquire(String tableName, String vdpId){
		SharedVDPLock svdpl = vdplocks.get(getPath(tableName, vdpId));
		if(svdpl==null){
			svdpl=new SharedVDPLock(tableName, vdpId, _lockReuseThreshold);
			vdplocks.put(getPath(tableName, vdpId), svdpl);
		}
		svdpl.shouldWait();
		boolean acquired = svdpl.acquire();
		vdplocks.put(getPath(tableName, vdpId), svdpl);
		return acquired;
	}
	
	
	/**
	 * Acquires an exclusive lock for the given VDP and gets the corresponding {@link VDPmigrationStatus}, 
	 * which gets stored in a cache. If the lock was already acquired by another local thread, and not released,
	 * the {@link VDPmigrationStatus} is got from the cache, otherwise, if no lock is already in place,
	 * the {@link VDPmigrationStatus} is taken from Zookeeper.
	 * 
	 * @param tableName The name of the table.
	 * @param vdpId The VDP id within the given table.
	 * @return the corresponding {@link VDPmigrationStatus} obtained either from Zookeeper or from the cache. 
	 * @throws IOException 
	 * @throws ClassNotFoundException 
	 */
	public VDPmigrationStatus acquireAndGet(String tableName, String vdpId) throws ClassNotFoundException, IOException{
		SharedVDPLock svdpl = vdplocks.get(getPath(tableName, vdpId));
		if(svdpl==null){
			svdpl=new SharedVDPLock(tableName, vdpId, _lockReuseThreshold);
			vdplocks.put(getPath(tableName, vdpId), svdpl);
		}
		svdpl.shouldWait();
		VDPmigrationStatus ms = svdpl.acquireAndGet(tableName, vdpId);
		vdplocks.put(getPath(tableName, vdpId), svdpl);
		return ms;
	}
	
	
	/**
	 * Stores a {@link VDPmigrationStatus} either in the local cache or, remotely, on Zookeeper.
	 * The choice is based on the fact whether the lock on the given VDP was already released or if
	 * it is still kept by some local thread.
	 * Before the lock is actually released on Zookeeper the last {@link VDPmigrationStatus} is stored inside Zookeeper. 
	 * @param ms The {@link VDPmigrationStatus} to set.
	 * @param tableName The name of the table.
	 * @param vdpId The VDP id for the given table.
	 * @throws Exception 
	 */
	public void setAndRelease(VDPmigrationStatus ms, String tableName, String vdpId) throws Exception{
		SharedVDPLock svdpl = vdplocks.get(getPath(tableName, vdpId));
		if(svdpl==null)
			return;
		svdpl.setAndRelease(ms, tableName, vdpId);
		vdplocks.put(getPath(tableName, vdpId), svdpl);
	}
	
	public void release(String tableName, String vdpId){
		SharedVDPLock svdpl = vdplocks.get(getPath(tableName, vdpId));
		if(svdpl==null)
			return;
		svdpl.release();
		vdplocks.put(getPath(tableName, vdpId), svdpl);
	}
	
	private String getPath(String tableName, String vdpId){
		return tableName+"/"+vdpId;
	}

	private boolean bulletProofZKset(VDPmigrationStatus updatedStatus, String vdpid, String tblName) throws Exception{
		int retries = 3;
		boolean done = false;
		do{
			if(retries<3){
				System.out.println(Thread.currentThread().getName()+" retrying...");
				Thread.sleep(100);
			}
			done = zKserver.setFreshMigrationStatus(tblName, vdpid, updatedStatus);
			retries--;
		}while(!done && retries>0);
		
		return done;
	}
	
	public class SharedVDPLock {
		private String tableName, vdpId;
		protected AtomicInteger queuingForLockCounter, reusedLockCounter;
		private Integer reuseThreshold;
		private static final int defaultReuseThreasholod = 10;
		private VDPmigrationStatus ms = null;
		
		private Object monitor;
		
		public SharedVDPLock(String tableName, String vdpId, Integer reuseThreshold){
			if(tableName==null || vdpId==null)
				throw new IllegalArgumentException();
			this.reuseThreshold =
					reuseThreshold!=null && reuseThreshold>0 ? reuseThreshold : defaultReuseThreasholod;
			this.tableName=tableName;
			this.vdpId = vdpId;
			queuingForLockCounter = new AtomicInteger();
			reusedLockCounter = new AtomicInteger(this.reuseThreshold);
			monitor = new Object();
		}
		
		private void shouldWait(){
			int reused = reusedLockCounter.get();
			
			if(reused<=0){
//				log.debug("{} - Waiting before acquiring VDP{}/{} from zookeeper",
//						Thread.currentThread().getName(), tableName, vdpId);
				try {
					synchronized(monitor){
						monitor.wait();
					}
				} catch (InterruptedException e) {}
			}
		}
		
		private synchronized boolean acquire(){
			int queuing = queuingForLockCounter.get();
			int reused = reusedLockCounter.get();
			log.debug("{} - Acquiring VDP{}/{} queuingForLockCounter:{}, reusedLockCounter:{}",
					Thread.currentThread().getName(), tableName, vdpId,
					queuing, reused);
			if(queuing==0){
				log.debug("{} - Acquiring VDP{}/{} from zookeeper",
						Thread.currentThread().getName(), tableName, vdpId);
				
				try{
					int i=0;
					while(!zKserver.acquireLock(tableName, vdpId+"")){
						if(i%10 == 0)
							log.warn(Thread.currentThread().getName()+
									" Cannot acquire lock on table: "+tableName+"/"+vdpId+". Retrying({})!",
									i);
						i++;
						Thread.sleep(1);
					}
				} catch (Exception e1) {
					log.error("{} - Error acquiring lock on table {}.\n Stack Trace: ",
							Thread.currentThread().getName(),
							tableName,
							e1);
					return false;
				}
			}
			queuing = queuingForLockCounter.incrementAndGet();
			reusedLockCounter.decrementAndGet();
			
			return true;
		}
		
		
		/**
		 * Acquires an exclusive lock for the given VDP and gets the corresponding {@link VDPmigrationStatus}, 
		 * which gets stored in a cache. If the lock was already acquired by another local thread, and not released,
		 * the {@link VDPmigrationStatus} is got from the cache, otherwise, if no lock is already in place,
		 * the {@link VDPmigrationStatus} is taken from Zookeeper.
		 * 
		 * @param tableName The name of the table.
		 * @param vdpId The VDP id within the given table.
		 * @return the corresponding {@link VDPmigrationStatus} obtained either from Zookeeper or from the cache. 
		 * @throws IOException 
		 * @throws ClassNotFoundException 
		 */
		public synchronized VDPmigrationStatus acquireAndGet(String tableName, String vdpId) throws ClassNotFoundException, IOException{
			if(acquire()){
						if(ms==null || queuingForLockCounter.get()<=1){
							log.debug("{} - Getting VDPmigrationStaus {}/{} from Zookeeper",
									Thread.currentThread().getName(), tableName, vdpId);
							//synchronized ms?
							ms = zKserver.getFreshMigrationStatus(tableName, vdpId+"", null);
						}
			}
			return ms;
		}
		
		private synchronized void release(){
			int queuing = queuingForLockCounter.decrementAndGet();
			log.debug("{} - Releasing VDP{}/{} queuingForLockCounter:{}",
					Thread.currentThread().getName(), tableName, vdpId,
					queuing);
			if(queuing<=0){
//				log.debug("{} - Releasing VDP{}/{} from zookeeper",
//						Thread.currentThread().getName(), tableName, vdpId);
				try{
					zKserver.releaseLock(tableName, vdpId);
					reusedLockCounter.set(reuseThreshold);
					log.debug("{} - After release of VDP{}/{} reusedLockCounter reset to {}",
							Thread.currentThread().getName(), tableName, vdpId,
							reuseThreshold);
					synchronized(monitor){
						monitor.notifyAll();
//						log.debug("{} - Waking up threads waiting for VDP{}/{}",
//								Thread.currentThread().getName(), tableName, vdpId);
					}
				}catch(Exception ae){};
			}	
		}
		
		/**
		 * Stores a {@link VDPmigrationStatus} either in the local cache or, remotely, on Zookeeper.
		 * The choice is based on the fact whether the lock on the given VDP was already released or if
		 * it is still kept by some local thread.
		 * Before the lock is actually released on Zookeeper the last {@link VDPmigrationStatus} is stored inside Zookeeper. 
		 * @param ms The {@link VDPmigrationStatus} to set.
		 * @param tableName The name of the table.
		 * @param vdpId The VDP id for the given table.
		 * @throws Exception 
		 */
		public synchronized void setAndRelease(VDPmigrationStatus ms, String tableName, String vdpId) throws Exception{
			if(ms!=null)
				this.ms=ms;
			
			//if, before releasing the lock, there's just a thread holding the lock
			//then we should set the VDPmigrationStatus in Zookeeper and then release the lock
			if(queuingForLockCounter.get()<=1){
				log.debug("{} - Setting the VDPmigrationStaus {}/{} in Zookeeper",
						Thread.currentThread().getName(), tableName, vdpId);
				if(ms!=null)
					bulletProofZKset(ms, vdpId, tableName);
				else
					bulletProofZKset(this.ms, vdpId, tableName);
			}
			release();
		}
	}
}
