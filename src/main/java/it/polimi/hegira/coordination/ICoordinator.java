package it.polimi.hegira.coordination;

import it.polimi.hegira.exceptions.LockedException;
import it.polimi.hegira.zkWrapper.util.SubscriberType;

/**
 * 
 * @author Marco Scavuzzo
 *
 */
public interface ICoordinator {
	/**
	 * Tells if the component can synchronize an entity contained in the specified VDP.
	 * If it can, it updates the state machine on the generic commit-log and returns true.
	 * There could be three reasons not to succeed:
	 * 	1) Hegira is migrating that VDP
	 *  2) The component requesting to synchronize is of type dst and the VDP has not yet been migrated.
	 *  3) A query lock is in place.
	 * In the first and second case, false is returned.
	 * In the third case, a LockException is thrown.
	 * @param vdpId The VDP id.
	 * @param tblName The name of the table.
	 * @param type The type of subscriber that tries to synchronize.
	 * @return An array containing the evaluation asserting if it is possible to synchronize in the Source and Destination databases respectively.
	 * @throws LockedException Queries are locked.
	 */
	public Boolean[] canSynchronize(int vdpId, String tblName) throws LockedException;
	
	/**
	 * Tells if the component can synchronize an entity, given its primary key.
	 * The method automatically determines the VDPid for the given entity and calls
	 * {@link it.polimi.hegira.coordination.ICoordinator#canSynchronize(int, String, SubscriberType)} 
	 * @param primaryKey The primary key of the entity.
	 * @param tblName The name of the table.
	 * @param type The type of subscriber that tries to synchronize.
	 * @return An array containing the evaluation asserting if it is possible to synchronize in the Source and Destination databases respectively.
	 * @throws LockedException Queries are locked.
	 */
	public Boolean[] canSynchronize(Integer primaryKey, String tblName) throws LockedException;
	
	/**
	 * Notifies all Hegira components, by means of the generic lock-manager, 
	 * that the synchronization has been completed.
	 * @param vdpId The VDP id.
	 * @param tblName The name of the table.
	 * @return true if it succeeded to notify, false otherwise.
	 */
	public boolean notifyFinishSync(int vdpId, String tblName);
	
	/**
	 * Notifies all Hegira components, by means of the generic lock-manager, 
	 * that the synchronization has been completed.
	 * @param primaryKey The primary key of the entity.
	 * @param tblName The name of the table.
	 * @return true if it succeeded to notify, false otherwise.
	 */
	public boolean notifyFinishSync(Integer primaryKey, String tblName);
	
	public void disconnect();
	
	public Integer getDataMigrationId();
}
