/**
 * 
 */
package it.polimi.hegira.coordination;

import java.util.HashMap;

import org.apache.curator.framework.recipes.locks.InterProcessMutex;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import it.polimi.hegira.exceptions.LockedException;
import it.polimi.hegira.utils.Constants;
import it.polimi.hegira.utils.DlmCache;
import it.polimi.hegira.utils.PropertiesManager;
import it.polimi.hegira.vdp.VdpUtils;
import it.polimi.hegira.zkWrapper.ZKserver2;
import it.polimi.hegira.zkWrapper.MigrationStatus.VDPstatus;
import it.polimi.hegira.zkWrapper.VDPmigrationStatus;
import it.polimi.hegira.zkWrapper.exception.OutOfSnapshotException;
import it.polimi.hegira.zkWrapper.statemachine.State;
import it.polimi.hegira.zkWrapper.statemachine.StateMachine;
import it.polimi.hegira.zkWrapper.util.SubscriberType;

/**
 * Class encapsulating the logic to coordinate with other Hegira components.
 * That is, it interacts with ZooKeeper (ZKserver V2) trying to properly set the MigrationStatus.
 * @author Marco Scavuzzo
 *
 */
public class ZKcoordinator2 implements ICoordinator {
	private static String zkConnectString;
	private static int mId;
	private static Logger log = LoggerFactory.getLogger(ZKcoordinator.class);
	private int vdPsize;
	private VDPcache2 cache;
	private DlmCache<String, InterProcessMutex> vdplockcache;
	private ZKserver2 zKserver = null;
	
	private static ZKcoordinator2 instance = null;
	private static Object instance_lock = new Object();
	
	private HashMap<Integer, StateMachine> snapshotCache;
	private VDPLockManager vdplm;
	
	/**
	 * Creates a new ZKcoordinator instance
	 * @param type The type of subscriber using the instance.
	 * @param vdpId The VDP the instance will operate on.
	 * @param tblName The table the instance will operate on.
	 * @param zkConnectString The ZooKeeper installation to connect with.
	 */
	private ZKcoordinator2(String zkCS, int migration_id) {
		zkConnectString = zkCS;
		mId=migration_id;
		//building a VDPcache whose initial size is 6, load factor 1
		//and maximum tables in the cache 5.
		this.cache = new VDPcache2(6,1.0f,5);
		connect();
		initParameters();
	}
	
	public static ZKcoordinator2 getInstance(String zkCS, int migration_id){
		if(zkCS==null)
			throw new IllegalArgumentException("Zookeeper connect string can't be null");
		synchronized(instance_lock){
			if(instance == null && zkConnectString==null){
				instance = new ZKcoordinator2(zkCS, migration_id);
			//connection string has changed
			} else if(!zkCS.equals(zkConnectString)){
				instance = new ZKcoordinator2(zkCS, migration_id);
			}
		}
		return instance;
	}
	
	private boolean isConnected(){
		return zKserver == null ? false : true;	
	}
	
	private void connect(){
		if(!isConnected())
			zKserver = new ZKserver2(zkConnectString);
	}
	
	public void disconnect(){
		if(isConnected())
			zKserver.close();
	}
	
	/**
	 * We want the VDP size (exponent p) to be initialized at object creation, 
	 * in order not to make constant requests to ZooKeeper for this (~static) value.
	 */
	private void initParameters(){
		if(!isConnected()) connect();
		try {
			this.vdPsize = zKserver.getVDPsize();
		} catch (Exception e) {
			e.printStackTrace();
		} 
		
		//We don't want to delete cache values if just some parameter varied.
		if(vdplockcache==null){
			vdplockcache = new DlmCache<String, InterProcessMutex>();
		}
		
		if(vdplm==null){
			String lockReuse = PropertiesManager.getProperty(Constants.ZK_PATH, Constants.LOCK_REUSE);
			Integer lockReuseNr=null;
			if(lockReuse!=null)
				lockReuseNr=Integer.parseInt(lockReuse);
			vdplm=new VDPLockManager(zKserver, 10, lockReuseNr);
		}
	}
	
	private boolean bulletProofZKset(VDPmigrationStatus updatedStatus, int vdpid, String tblName) throws Exception{
		int retries = 3;
		boolean done = false;
		do{
			if(retries<3){
				System.out.println(Thread.currentThread().getName()+" retrying...");
				Thread.sleep(300);
			}
			//log.debug(Thread.currentThread().getName()+
			//		" updating ZooKeeper with "+updatedStatus.getVDPstatus(vdpid).getCurrentState()+
			//		" for vdp "+vdpid);
			done = zKserver.setFreshMigrationStatus(tblName, vdpid+"", updatedStatus);
			retries--;
		}while(!done && retries>0);
		
		return done;
	}

	
	private boolean isOutOfSnapshot(String tblName, Integer VDPid){
		try {
			if(snapshotCache==null)
				snapshotCache = zKserver.getFreshVDPs(tblName);
			return !snapshotCache.containsKey(VDPid);
		} catch (Exception e) {
			log.error("{} - Error getting the list of VDPs:\n",
					Thread.currentThread().getName(),e);
		}
		return false;
	}
	
	/**
	 * Tells if the component can synchronize an entity contained in the specified VDP.
	 * If it can, it updates the state machine on the generic commit-log and returns true.
	 * There could be three reasons not to succeed:
	 * 	1) Hegira is migrating that VDP
	 *  2) The component requesting to synchronize is of type dst and the VDP has not yet been migrated.
	 *  3) A query lock is in place.
	 * In the second case, false is returned.
	 * In the first and third case, it waits until no lock is in place.
	 * @param vdpId The VDP id.
	 * @param tblName The name of the table.
	 * @return <b>true</b> if it is possible to synchronize, <b>false</b> otherwise.
	 * @throws LockedException never thrown currently.
	 */
	@Override
	public Boolean[] canSynchronize(int vdpId, String tblName)
			throws LockedException {
		boolean tableLocked=false;
		Boolean[] returnValue = new Boolean[2];
		//log.debug("{} - canSynchronize VDPID: {}?",Thread.currentThread().getName(),vdpId);
		//checking the cache in order to reduce requests to ZooKeeper.
		if(isOutOfSnapshot(tblName, vdpId)){
			returnValue[0]=true;
			returnValue[1]=true;
			return returnValue;
		}
		
		try {
			if(cache.isMigrated(vdpId+"", tblName)){
				returnValue[0]=true;
				returnValue[1]=true;
				return returnValue;
			}
		} catch (OutOfSnapshotException | NullPointerException e2) {}
		//---------//
		
		if(!isConnected()) connect();
		/*//boolean isLocked = true;
		try {
			//do{
			//	isLocked = zKserver.isLocked(null);
			//}while(isLocked);
			
			//trying to acquire mutex on a specific table
			int i=0;
			while(!zKserver.acquireLock(tblName, vdpId+"")){
				if(debug && i%10 == 0)
					log.warn(Thread.currentThread().getName()+
							" Cannot acquire lock on table: "+tblName+"/"+vdpId+". Retrying({})!",
							i);
				i++;
				Thread.sleep(50);
			}
			tableLocked=true;
		} catch (Exception e1) {
			log.error("{} - Error acquiring lock on table {}.\n Stack Trace: ",
					Thread.currentThread().getName(),
					tblName,
					e1);
		}*/
		
		/*//V2
		tableLocked=vdplm.acquire(tblName, vdpId+"");
		tableLocked=true;
		**/
		try {	
			/*//V2
			VDPmigrationStatus currentStatus = zKserver.getFreshMigrationStatus(tblName, vdpId+"", null);
			*/
			//V3
			VDPmigrationStatus currentStatus = vdplm.acquireAndGet(tblName, vdpId+"");
			tableLocked=true;//
			
			VDPstatus syncVDP = currentStatus.syncVDP(SubscriberType.SOURCE);
			//log.debug("{} - status after calling synchVDP: "+syncVDP.name(),
			//		Thread.currentThread().getName());
			
			/*//V2
			if(syncVDP.name().equals(VDPstatus.SYNC.name())){
				bulletProofZKset(currentStatus, vdpId, tblName);
			}
			
			try{
				//zKserver.releaseLock(tblName, vdpId+"");
				vdplm.release(tblName, vdpId+"");
				tableLocked=false;
			}catch(Exception ae){};
			**/
			
			//V3
			try{
				vdplm.setAndRelease(currentStatus, tblName, vdpId+"");
				tableLocked=false;
			}catch(Exception ae){};//
			
			if(syncVDP.equals(VDPstatus.NOT_MIGRATED)){
				//should never happen...
				returnValue[0] = true;
				returnValue[1] = false;
				log.error("{} - Error! The status was of VDP {}/{} was not changed for some reason!!",
						Thread.currentThread().getName(),
						tblName, vdpId);
				return returnValue;
			} else if(syncVDP.equals(VDPstatus.UNDER_MIGRATION)) {
				
				Thread.sleep(30);
				//log.debug("{} ====> Recursively calling canSynchronize("+vdpId+","+tblName+")",
				//		Thread.currentThread().getName());
				return canSynchronize(vdpId, tblName);
			} else if(syncVDP.equals(VDPstatus.MIGRATED)) {
				returnValue[0]=true;
				returnValue[1]=true;
				
				return returnValue;
			} else if(syncVDP.equals(VDPstatus.SYNC)) {
				returnValue[0] = true;
				returnValue[1] = false;
				
				return returnValue;
			} else {
				returnValue[0] = false;
				returnValue[1] = false;
				
				log.error("{} - Unknown", Thread.currentThread().getName());
				return returnValue;
			}
			
		} catch (OutOfSnapshotException e) {
			try {
				if(tableLocked){
					//zKserver.releaseLock(tblName, vdpId+"");
					/*//V2
					vdplm.release(tblName, vdpId+"");*/
					//V3
					vdplm.setAndRelease(null, tblName, vdpId+"");//
					tableLocked=false;
				}
			} catch (Exception e1) {
				log.error(Thread.currentThread().getName()+
						" - Cannot release lock on table: "+tblName+"/"+vdpId);
			}
			returnValue[0]=true;
			returnValue[1]=true;
			return returnValue;
		} catch (Exception e) {
			log.error("{} - Caught generic exception!\nStack trace:\n",
					Thread.currentThread().getName(), e);
			if(tableLocked){
				try {
					if(!isConnected()) connect();
					
					//zKserver.releaseLock(tblName, vdpId+"");
					/*//V2
					vdplm.release(tblName, vdpId+"");*/
					//V3
					vdplm.setAndRelease(null, tblName, vdpId+"");//
					tableLocked=false;
				} catch (Exception e1) {
					log.error(Thread.currentThread().getName()+
							" - Cannot release lock on table: "+tblName+"/"+vdpId);
				}
			}
		} finally {
			if(tableLocked){
				if(!isConnected()) connect();
				
				try {
					//zKserver.releaseLock(tblName, vdpId+"");
					/*//V2
					vdplm.release(tblName, vdpId+"");*/
					//V3
					vdplm.setAndRelease(null, tblName, vdpId+"");
				} catch (Exception e1) {
					//log.error(Thread.currentThread().getName()+
					//		" - Cannot release lock on table: "+tblName);
				}
			}
		}
		return returnValue;
	}
	
	@Override
	public Boolean[] canSynchronize(Integer primaryKey, String tblName)
			throws LockedException {
		//if the subscriber was started before partitions were created (i.e., Tests)
		if(vdPsize<=0){
			initParameters();
		}
		int vdpId = VdpUtils.getVDP(primaryKey, vdPsize);
		//log.debug("PK: {} -> VDPID: {}. vdpSize: {}",primaryKey,vdpId,vdPsize);
		return canSynchronize(vdpId, tblName);
	}

	@Override
	public boolean notifyFinishSync(int vdpId, String tblName) {
		//log.debug("{} - notifyFinishSync VDPID: {}?", Thread.currentThread().getName(), vdpId);
		/*  Checking the cache in order to reduce requests to ZooKeeper.
			It is not necessary to lock/notify anything if the VDP's status contained in the cache
			is MIGRATED. Actually it is only necessary if 1) the status is NOT_MIGRATED,
			2) the listener has to check whether the VDP's status is UNDER_MIGRATION
		*/
		boolean tableLocked=false;
		if(isOutOfSnapshot(tblName, vdpId)){
			return true;
		}
		
		try {
			if(cache.isMigrated(vdpId+"", tblName))
				return true;
		} catch (OutOfSnapshotException | NullPointerException e2) {}
		
		
		try {
			if(!isConnected()) connect();
			
			/*int i=0;
			//trying to acquire mutex on a specific table
			while(!zKserver.acquireLock(tblName, vdpId+"")){
				if(debug && i%100 == 0)
					log.warn(Thread.currentThread().getName()+
							" Cannot acquire lock on table: "+tblName+"/"+vdpId+". Retrying({})!",
							i);
				i++;
				Thread.sleep(50);
			}
			tableLocked=true;*/
			
			/*//V2
			tableLocked=vdplm.acquire(tblName, vdpId+"");
			tableLocked=true;
			
			VDPmigrationStatus currentStatus = zKserver.getFreshMigrationStatus(tblName, vdpId+"",null);
			*/
			//V3
			VDPmigrationStatus currentStatus = vdplm.acquireAndGet(tblName, vdpId+"");
			tableLocked=true;
			//
			boolean returnValue=false;
			if(currentStatus.getVDPstatus().getCurrentState().equals(State.SYNC)){
				currentStatus.finish_syncVDP();
				/*//V2
				returnValue = bulletProofZKset(currentStatus, vdpId, tblName);*/
				//V3
				try{
					vdplm.setAndRelease(currentStatus, tblName, vdpId+"");
					tableLocked=false;
					returnValue = true;
				}catch(Exception ae){};//
			}else{
				//V3
				try{
					//zKserver.releaseLock(tblName, vdpId+"");
					vdplm.release(tblName, vdpId+"");
					tableLocked=false;
				}catch(Exception ae){};//
				returnValue = true;
			}
			
			/*//V2//releasing mutex
			try{
				//zKserver.releaseLock(tblName, vdpId+"");
				vdplm.release(tblName, vdpId+"");
				tableLocked=false;
			}catch(Exception ae){};*/
			
			
			//updating the cache
			cache.put(tblName, vdpId+"", currentStatus);
			
			return returnValue;
		} catch (OutOfSnapshotException e) {
			if(tableLocked){
				try {
					//zKserver.releaseLock(tblName, vdpId+"");
					/*//V2
					vdplm.release(tblName, vdpId+"");*/
					//V3
					vdplm.setAndRelease(null, tblName, vdpId+"");//
					tableLocked=false;
					return true;
				} catch (Exception e1) {
					log.error(Thread.currentThread().getName()+
							" - Cannot release lock on table: "+tblName+"/"+vdpId);
					return false;
				}
			}
		} catch (Exception e) {
			log.error("{} - Caught generic exception!\nStack trace:\n",
					Thread.currentThread().getName(), e);
		} finally {
			if(tableLocked){
				if(!isConnected()) connect();
				try {
					//zKserver.releaseLock(tblName, vdpId+"");
					/*//V2
					vdplm.release(tblName, vdpId+"");*/
					//V3
					vdplm.setAndRelease(null, tblName, vdpId+"");//
					return true;
				} catch (Exception e1) {
					//log.error(Thread.currentThread().getName()+
					//		" - Cannot release lock on table: "+tblName);
				}
			}
		}
		return false;
	}

	@Override
	public boolean notifyFinishSync(Integer primaryKey, String tblName) {
		//if the subscriber was started before partitions were created (i.e., Tests)
		if(vdPsize<=0){
			initParameters();
		}
		int vdpId = VdpUtils.getVDP(primaryKey, vdPsize);
		//log.debug("PK: {} -> VDPID: {}. vdpSize: {}",primaryKey,vdpId,vdPsize);
		return notifyFinishSync(vdpId, tblName);
	}

	@Override
	public Integer getDataMigrationId() {
		return new Integer(mId);
	}
}
