package it.polimi.hegira.coordination;

import it.polimi.hegira.utils.LRUCache;
import it.polimi.hegira.zkWrapper.MigrationStatus;
import it.polimi.hegira.zkWrapper.MigrationStatus.VDPstatus;
import it.polimi.hegira.zkWrapper.exception.OutOfSnapshotException;
import it.polimi.hegira.zkWrapper.statemachine.State;
import it.polimi.hegira.zkWrapper.statemachine.StateMachine;

public class VDPcache{
	private LRUCache<String, MigrationStatus> cache;
	
	@Deprecated
	public VDPcache() {
		this.cache = new LRUCache<String, MigrationStatus>();
	}
	
	public VDPcache(int initialCapacity, float loadFactor, int cacheSize) {
		this.cache = new LRUCache<String, MigrationStatus>(initialCapacity, loadFactor, cacheSize);
	}
	
	/**
	 * Puts a whole migration status, for a given table, in the cache.
	 * If the cache previously contained a mapping for the table, the old value is replaced.
	 * @param tblName The name of the table
	 * @param ms The migration status
	 * @return the previous value associated with tableName, or null if there was no mapping for key. (A null return can also indicate that the map previously associated null with key.)
	 */
	public MigrationStatus put(String tblName, MigrationStatus ms) {
		synchronized (cache) {
			return cache.put(tblName, ms);
		}
	}
	
	/**
	 * Updates a VDP's status in the cache if the table it belongs is already cached, otherwise it throws a {@link NullPointerException}.
	 * @param tblName The table name.
	 * @param vdpId The VDP id.
	 * @param status	 The new VDP's status
	 * @return The previous value associated with vdpId, or null if there was no mapping for the vdpId.
	 * @throws NullPointerException if the given VDP wasn't already contained in the cache!!
	 */
	public VDPstatus put(String tblName, int vdpId, VDPstatus status) throws NullPointerException{
		synchronized (cache) {
			StateMachine sm = cache.get(tblName).getVDPs().put(vdpId, new StateMachine(State.valueOf(status.name())));
			if(sm!=null)
				return VDPstatus.valueOf(sm.getCurrentState().name());
			else
				return null;
		}
	}
	/**
	 * Gets the VDP's status.
	 * @param tblName The table name
	 * @param vdpId The VDP id
	 * @return The retrieved VDP status
	 * @throws OutOfSnapshotException in case a VDP, which is not part of the current snapshot, as stored in the coordination system, has been requested.
	 */
	public VDPstatus get(String tblName, int vdpId) throws OutOfSnapshotException{
		return VDPstatus.valueOf(cache.get(tblName).getVDPstatus(vdpId).getCurrentState().name());
	}
	
	/**
	 * Tells if a given VDP has already been migrated, according to the local cache, 
	 * WITHOUT making an explicit request to the coordination system.
	 * (Because, if its status is already MIGRATED it cannot be changed, thus is useless to make an 
	 * explicit request!)
	 * @param vdpId The VDP id.
	 * @param tblName The name of the table.
	 * @return <b>true</b> if it is possible to synchronize, <b>false</b> otherwise.
	 * @throws OutOfSnapshotException Exception returned in case a VDP, which is not part of the current snapshot, as stored in the coordination system, has been requested.
	 */
	public boolean isMigrated(int vdpId, String tblName) throws OutOfSnapshotException{
		return cache.get(tblName).getVDPstatus(vdpId).getCurrentState().name().equals(State.MIGRATED.name()) ? true : false;
	}
}
