package it.polimi.hegira.coordination;

import it.polimi.hegira.utils.LRUCache;
import it.polimi.hegira.zkWrapper.MigrationStatus.VDPstatus;
import it.polimi.hegira.zkWrapper.VDPmigrationStatus;
import it.polimi.hegira.zkWrapper.exception.OutOfSnapshotException;
import it.polimi.hegira.zkWrapper.statemachine.State;
import it.polimi.hegira.zkWrapper.statemachine.StateMachine;

public class VDPcache2 {
	private LRUCache<String, VDPmigrationStatus> cache;
	
	@Deprecated
	public VDPcache2() {
		this.cache = new LRUCache<String, VDPmigrationStatus>();
	}
	
	public VDPcache2(int initialCapacity, float loadFactor, int cacheSize) {
		this.cache = new LRUCache<String, VDPmigrationStatus>(initialCapacity, loadFactor, cacheSize);
	}
	
	/**
	 * Puts a whole migration status, for a given table, in the cache.
	 * If the cache previously contained a mapping for the table, the old value is replaced.
	 * @param tblName The name of the table
	 * @param ms The migration status
	 * @return the previous value associated with tableName, or null if there was no mapping for key. (A null return can also indicate that the map previously associated null with key.)
	 */
	public VDPmigrationStatus put(String tblName, String vdpId, VDPmigrationStatus ms) {
		synchronized (cache) {
			return cache.put(tblName+vdpId, ms);
		}
	}
	
	/**
	 * Updates a VDP's status in the cache if the table it belongs is already cached, otherwise it throws a {@link NullPointerException}.
	 * @param tblName The table name.
	 * @param vdpId The VDP id.
	 * @param status	 The new VDP's status
	 * @return The previous value associated with vdpId, or null if there was no mapping for the vdpId.
	 * @throws NullPointerException if the given VDP wasn't already contained in the cache!!
	 */
	public VDPstatus put(String tblName, String vdpId, VDPstatus status) throws NullPointerException{
		synchronized (cache) {
			VDPstatus returnValue=null;
			VDPmigrationStatus vdpMigrationStatus = cache.get(tblName+vdpId);
			VDPmigrationStatus prevVDPms = null;
			try {
				prevVDPms = new VDPmigrationStatus(vdpMigrationStatus);
				returnValue = VDPstatus.valueOf(prevVDPms.getVDPstatus().getCurrentState().name());
			}catch(NullPointerException | OutOfSnapshotException e){}
			
			cache.get(tblName+vdpId).setStateMachine(new StateMachine(State.valueOf(status.name())));
			
			return returnValue;
		}
	}
	/**
	 * Gets the VDP's status.
	 * @param tblName The table name
	 * @param vdpId The VDP id
	 * @return The retrieved VDP status
	 * @throws OutOfSnapshotException in case a VDP, which is not part of the current snapshot, as stored in the coordination system, has been requested.
	 */
	public VDPstatus get(String tblName, String vdpId) throws OutOfSnapshotException{
		return VDPstatus.valueOf(cache.get(tblName+vdpId).getVDPstatus().getCurrentState().name());
	}
	
	/**
	 * Tells if a given VDP has already been migrated, according to the local cache, 
	 * WITHOUT making an explicit request to the coordination system.
	 * (Because, if its status is already MIGRATED it cannot be changed, thus is useless to make an 
	 * explicit request!)
	 * @param vdpId The VDP id.
	 * @param tblName The name of the table.
	 * @return <b>true</b> if it is possible to synchronize, <b>false</b> otherwise.
	 * @throws OutOfSnapshotException Exception returned in case a VDP, which is not part of the current snapshot, as stored in the coordination system, has been requested.
	 */
	public boolean isMigrated(String vdpId, String tblName) throws OutOfSnapshotException{
		return cache.get(tblName+vdpId).getVDPstatus().getCurrentState().name().equals(State.MIGRATED.name()) ? true : false;
	}
}
