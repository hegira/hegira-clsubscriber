/**
 * 
 */
package it.polimi.hegira.coordination;

import java.util.concurrent.TimeUnit;

import org.apache.curator.framework.recipes.locks.InterProcessMutex;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import it.polimi.hegira.exceptions.LockedException;
import it.polimi.hegira.zkWrapper.statemachine.State;
import it.polimi.hegira.zkWrapper.util.SubscriberType;
import it.polimi.hegira.utils.DlmCache;
import it.polimi.hegira.vdp.VdpUtils;
import it.polimi.hegira.zkWrapper.MigrationStatus;
import it.polimi.hegira.zkWrapper.MigrationStatus.VDPstatus;
import it.polimi.hegira.zkWrapper.ZKserver;
import it.polimi.hegira.zkWrapper.dlm.VDPlockManager;
import it.polimi.hegira.zkWrapper.exception.OutOfSnapshotException;

/**
 * Class encapsulating the logic to coordinate with other Hegira components.
 * That is, it interacts with ZooKeeper trying to properly set the MigrationStatus.
 * @author Marco Scavuzzo
 */
public class ZKcoordinator implements ICoordinator{
	private static String zkConnectString;
	private static int mId;
	private static Logger log = LoggerFactory.getLogger(ZKcoordinator.class);
	private int vdPsize;
	private VDPcache cache;
	private DlmCache<String, InterProcessMutex> vdplockcache;
	private static boolean debug = true;
	private ZKserver zKserver = null;
	
	private static ZKcoordinator instance = null;
	private static Object instance_lock = new Object();
	
	private boolean paranoid_mode = false;
	
	/**
	 * Creates a new ZKcoordinator instance
	 * @param type The type of subscriber using the instance.
	 * @param vdpId The VDP the instance will operate on.
	 * @param tblName The table the instance will operate on.
	 * @param zkConnectString The ZooKeeper installation to connect with.
	 */
	private ZKcoordinator(String zkCS, int migration_id) {
		zkConnectString = zkCS;
		mId=migration_id;
		//building a VDPcache whose initial size is 6, load factor 1
		//and maximum tables in the cache 5.
		this.cache = new VDPcache(6,1.0f,5);
		connect();
		initParameters();
	}
	
	public static ZKcoordinator getInstance(String zkCS, int migration_id){
		if(zkCS==null)
			throw new IllegalArgumentException("Zookeeper connect string can't be null");
		synchronized(instance_lock){
			if(instance == null && zkConnectString==null){
				instance = new ZKcoordinator(zkCS, migration_id);
			//connection string has changed
			} else if(!zkCS.equals(zkConnectString)){
				instance = new ZKcoordinator(zkCS, migration_id);
			}
		}
		return instance;
	}
	
	private boolean isConnected(){
		return zKserver == null ? false : true;	
	}
	
	private void connect(){
		if(!isConnected())
			zKserver = new ZKserver(zkConnectString);
	}
	
	public void disconnect(){
		if(isConnected())
			zKserver.close();
	}
	
	/**
	 * We want the VDP size (exponent p) to be initialized at object creation, 
	 * in order not to make constant requests to ZooKeeper for this (~static) value.
	 */
	private void initParameters(){
		if(!isConnected()) connect();
		try {
			this.vdPsize = zKserver.getVDPsize();
		} catch (Exception e) {
			e.printStackTrace();
		} 
		
		//We don't want to delete cache values if just some parameter varied.
		if(vdplockcache==null){
			vdplockcache = new DlmCache<String, InterProcessMutex>();
		}
	}
	
	private boolean bulletProofZKset(MigrationStatus updatedStatus, int vdpid, String tblName) throws Exception{
		int retries = 3;
		boolean done = false;
		do{
			if(retries<3){
				System.out.println(Thread.currentThread().getName()+" retring...");
				Thread.sleep(300);
			}
			//log.debug(Thread.currentThread().getName()+
			//		" updating ZooKeeper with "+updatedStatus.getVDPstatus(vdpid).getCurrentState()+
			//		" for vdp "+vdpid);
			done = zKserver.setFreshMigrationStatus(tblName, updatedStatus);
			retries--;
		}while(!done && retries>0);
		
		return done;
	}

	/**
	 * Tells if the component can synchronize an entity contained in the specified VDP.
	 * If it can, it updates the state machine on the generic commit-log and returns true.
	 * There could be three reasons not to succeed:
	 * 	1) Hegira is migrating that VDP
	 *  2) The component requesting to synchronize is of type dst and the VDP has not yet been migrated.
	 *  3) A query lock is in place.
	 * In the second case, false is returned.
	 * In the first and third case, it waits until no lock is in place.
	 * @param vdpId The VDP id.
	 * @param tblName The name of the table.
	 * @return <b>true</b> if it is possible to synchronize, <b>false</b> otherwise.
	 * @throws LockedException never thrown currently.
	 */
	@Override
	public synchronized Boolean[] canSynchronize(int vdpId, String tblName)
			throws LockedException {
		boolean tableLocked=false;
		Boolean[] returnValue = new Boolean[2];
		//log.debug("{} - canSynchronize VDPID: {}?",Thread.currentThread().getName(),vdpId);
		//checking the cache in order to reduce requests to ZooKeeper.
		try {
			if(cache.isMigrated(vdpId, tblName)){
				returnValue[0]=true;
				returnValue[1]=true;
				return returnValue;
			}
		} catch (OutOfSnapshotException | NullPointerException e2) {}
		//---------//
		
		if(!isConnected()) connect();
		boolean isLocked = true;
		try {
			do{
				isLocked = zKserver.isLocked(null);
			}while(isLocked);
			
			//trying to acquire mutex on a specific table
			int i=0;
			while(!zKserver.acquireLock(tblName)){
				if(debug && i%10 == 0)
					log.warn(Thread.currentThread().getName()+
							" Cannot acquire lock on table: "+tblName+". Retrying({})!",
							i);
				i++;
				Thread.sleep(100);
			}
			tableLocked=true;
		} catch (Exception e1) {
			log.error("{} - Error acquiring lock on table {}.\n Stack Trace: ",
					Thread.currentThread().getName(),
					tblName,
					e1);
		}
		
		
		
		try {	
			MigrationStatus currentStatus = zKserver.getFreshMigrationStatus(tblName, null);
			
			if(paranoid_mode){
				int tentatives = 1;
				//Check if the current state is NOT_MIGRATED
				//if it is try to acquire the lock for this VDP in a blocking way
				InterProcessMutex vdpMutex = null;
				VDPlockManager vdplm = VDPlockManager.getInstance(zkConnectString, ""+mId);
				
				do{
					if(tentatives>1)
						currentStatus = zKserver.getFreshMigrationStatus(tblName, null);
					log.debug("{} - Trying to acquire VDPlock {}/{}",
							Thread.currentThread().getName(),
							tblName, vdpId);
					
					try{
						vdpMutex = vdplm.acquireLock(tblName, vdpId, 1500, TimeUnit.MILLISECONDS);
					}catch(IllegalStateException ise){
						vdpMutex=null;
						if(tentatives<=1)
							try{zKserver.releaseLock(tblName);}catch(Exception ae){};
						log.debug("{} - Deadlock detected while trying to acquire VDPlock {}/{}",
								Thread.currentThread().getName(),
								tblName,vdpId);
						tentatives++;
					}
					
					if(vdpMutex!=null){
						log.debug("{} - Acquired VDPlock {}",
								Thread.currentThread().getName(),
								tblName+vdpId);
						vdplockcache.putLock(tblName+vdpId, vdpMutex);
					}
				}while(currentStatus.getVDPstatus(vdpId).getCurrentState().equals(State.NOT_MIGRATED) && 
						vdpMutex==null);
				
				if(tentatives>1){
					//the lock on table was released to prevent deadlocks! Hence, it should be reacquired.
					int i=1;
					while(!zKserver.acquireLock(tblName)){
						if(debug && i%10 == 0)
							log.warn(Thread.currentThread().getName()+
									" Cannot acquire lock on table: "+tblName+". Retrying({})!",
									i);
						i++;
						Thread.sleep(100);
					}
				}//
			}
			//log.debug("{} - canSynchronize? currentStatus for VDP "+vdpId+": "+currentStatus.getVDPstatus(vdpId).getCurrentState(),
			//		Thread.currentThread().getName());
			currentStatus = zKserver.getFreshMigrationStatus(tblName, null);
			VDPstatus syncVDP = currentStatus.syncVDP(vdpId, SubscriberType.SOURCE);
			//log.debug("{} - status after calling synchVDP: "+syncVDP.name(),
			//		Thread.currentThread().getName());
			try{
				zKserver.releaseLock(tblName);
				tableLocked=false;
			}catch(Exception ae){};
			
			if(syncVDP.equals(VDPstatus.NOT_MIGRATED)){
				//should never happen...
				returnValue[0] = true;
				returnValue[1] = false;
				log.error("{} - Error! The status was of VDP {}/{} was not changed for some reason!!",
						Thread.currentThread().getName(),
						tblName, vdpId);
				return returnValue;
			} else if(syncVDP.equals(VDPstatus.UNDER_MIGRATION)) {
				
				Thread.sleep(30);
				//log.debug("{} ====> Recursively calling canSynchronize("+vdpId+","+tblName+")",
				//		Thread.currentThread().getName());
				return canSynchronize(vdpId, tblName);
			} else if(syncVDP.equals(VDPstatus.MIGRATED)) {
				returnValue[0]=true;
				returnValue[1]=true;
				
				return returnValue;
			} else if(syncVDP.equals(VDPstatus.SYNC)) {
				returnValue[0] = true;
				returnValue[1] = false;
				
				return returnValue;
			} else {
				returnValue[0] = false;
				returnValue[1] = false;
				
				log.error("{} - Unknown", Thread.currentThread().getName());
				return returnValue;
			}
			
		} catch (OutOfSnapshotException e) {
			try {
				if(tableLocked)
					zKserver.releaseLock(tblName);
			} catch (Exception e1) {
				log.error(Thread.currentThread().getName()+
						" - Cannot release lock on table: "+tblName);
			}
			returnValue[0]=true;
			returnValue[1]=true;
			return returnValue;
		} catch (Exception e) {
			log.error("{} - Caught generic exception!\nStack trace:\n",
					Thread.currentThread().getName(), e);
			if(tableLocked){
				try {
					if(!isConnected()) connect();
					
					zKserver.releaseLock(tblName);
				} catch (Exception e1) {
					log.error(Thread.currentThread().getName()+
							" - Cannot release lock on table: "+tblName);
				}
			}
		} finally {
			if(tableLocked){
				if(!isConnected()) connect();
				
				try {
					zKserver.releaseLock(tblName);
				} catch (Exception e1) {
					//log.error(Thread.currentThread().getName()+
					//		" - Cannot release lock on table: "+tblName);
				}
			}
		}
		return returnValue;
	}
	
	@Override
	public Boolean[] canSynchronize(Integer primaryKey, String tblName)
			throws LockedException {
		//if the subscriber was started before partitions were created (i.e., Tests)
		if(vdPsize<=0){
			initParameters();
		}
		int vdpId = VdpUtils.getVDP(primaryKey, vdPsize);
		//log.debug("PK: {} -> VDPID: {}. vdpSize: {}",primaryKey,vdpId,vdPsize);
		return canSynchronize(vdpId, tblName);
	}

	@Override
	public synchronized boolean notifyFinishSync(int vdpId, String tblName) {
		//log.debug("{} - notifyFinishSync VDPID: {}?", Thread.currentThread().getName(), vdpId);
		/*  Checking the cache in order to reduce requests to ZooKeeper.
			It is not necessary to lock/notify anything if the VDP's status contained in the cache
			is MIGRATED. Actually it is only necessary if 1) the status is NOT_MIGRATED,
			2) the listener has to check whether the VDP's status is UNDER_MIGRATION
		*/
		boolean tableLocked=false;
		
		try {
			if(cache.isMigrated(vdpId, tblName))
				return true;
		} catch (OutOfSnapshotException | NullPointerException e2) {}
		
		
		try {
			if(!isConnected()) connect();
			
			int i=0;
			//trying to acquire mutex on a specific table
			while(!zKserver.acquireLock(tblName)){
				if(debug && i%100 == 0)
					log.warn(Thread.currentThread().getName()+
							" Cannot acquire lock on table: "+tblName+". Retrying({})!",
							i);
				i++;
				Thread.sleep(100);
			}
			tableLocked=true;
			MigrationStatus currentStatus = zKserver.getFreshMigrationStatus(tblName, null);
			VDPstatus statusAfterFinish = currentStatus.finish_syncVDP(vdpId);
			boolean returnValue = bulletProofZKset(currentStatus, vdpId, tblName);
			//releasing mutex
			try{
				zKserver.releaseLock(tblName);
				tableLocked=false;
			}catch(Exception ae){};
			
			if(paranoid_mode){
				//remove the lock on the VDP, if any. 
				//should be necessary only if the result of the method finish_syncVDP is NOT_MIRGATED
				log.debug("{} - Checking whether any VDPlock ({}/{}) should be released.\n"
						+ "statusAfterFinish={} == {}",
						Thread.currentThread().getName(),
						tblName, vdpId,
						statusAfterFinish.name(),
						VDPstatus.NOT_MIGRATED.name());
				if(statusAfterFinish.name().equals(VDPstatus.NOT_MIGRATED.name())){
					InterProcessMutex removedLock = vdplockcache.removeLock(tblName+vdpId);
					if(removedLock!=null){
						VDPlockManager.getInstance(zkConnectString, ""+mId)
							.releaseLock(removedLock);
						log.debug("{} - Removed VDP lock on {}",
								Thread.currentThread().getName(),tblName+vdpId);
					}else{
						log.warn("{} - The mutex removed from the cache was null!! I cannot release any associated VDP lock",
								Thread.currentThread().getName());
					}
				}else{
					log.debug("{} - Apparently the VDPstatus was not {}",
						Thread.currentThread().getName(),
						VDPstatus.NOT_MIGRATED);
				}
			}
			//TODO: can be done asynchronously
			//updating the cache
			cache.put(tblName, currentStatus);
			
			return returnValue;
		} catch (OutOfSnapshotException e) {
			if(tableLocked){
				try {
					zKserver.releaseLock(tblName);
					return true;
				} catch (Exception e1) {
					log.error(Thread.currentThread().getName()+
							" - Cannot release lock on table: "+tblName);
					return false;
				}
			}
		} catch (Exception e) {
			log.error("{} - Caught generic exception!\nStack trace:\n",
					Thread.currentThread().getName(), e);
		} finally {
			if(tableLocked){
				if(!isConnected()) connect();
				try {
					zKserver.releaseLock(tblName);
					return true;
				} catch (Exception e1) {
					//log.error(Thread.currentThread().getName()+
					//		" - Cannot release lock on table: "+tblName);
				}
			}
		}
		return false;
	}

	@Override
	public boolean notifyFinishSync(Integer primaryKey, String tblName) {
		//if the subscriber was started before partitions were created (i.e., Tests)
		if(vdPsize<=0){
			initParameters();
		}
		int vdpId = VdpUtils.getVDP(primaryKey, vdPsize);
		//log.debug("PK: {} -> VDPID: {}. vdpSize: {}",primaryKey,vdpId,vdPsize);
		return notifyFinishSync(vdpId, tblName);
	}

	@Override
	public Integer getDataMigrationId() {
		return new Integer(mId);
	}

}