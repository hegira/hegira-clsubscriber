/**
 * 
 */
package it.polimi.hegira;

import java.io.Closeable;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.curator.utils.CloseableUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import it.polimi.hegira.coordination.CoordinatorsFactory;
import it.polimi.hegira.coordination.CoordinatorsFactory.CoordinatorNames;
import it.polimi.hegira.discovery.DiscoveryClient;
import it.polimi.hegira.discovery.IResolutionService;
import it.polimi.hegira.coordination.ICoordinator;
import it.polimi.hegira.dlm.ILockManager;
import it.polimi.hegira.dlm.LockManagerFactory;
import it.polimi.hegira.dlm.ZooKeeperLockManager;
import it.polimi.hegira.dlm.LockManagerFactory.LockManagerNames;
import it.polimi.hegira.listener.AbstractListener;
import it.polimi.hegira.listener.ListenersFactory;
import it.polimi.hegira.listener.ListenersFactory.ListenerNames;
import it.polimi.hegira.notifications.AbstractNotificationManager;
import it.polimi.hegira.notifications.NotificationsFactory;
import it.polimi.hegira.notifications.NotificationsFactory.NotificationManagerNames;
import it.polimi.hegira.notifications.OperationResponse;
import it.polimi.hegira.persistence.DatabasesFactory;
import it.polimi.hegira.persistence.IDatabase;
import it.polimi.hegira.queries.IQueryInterpreter;
import it.polimi.hegira.queries.QueryInterpretersFactory;
import it.polimi.hegira.queries.QueryInterpretersFactory.QueryInterpreterNames;
import it.polimi.hegira.utils.CLI;

/**
 * Used for centralized management of internal factories.
 * @author Marco Scavuzzo
 *
 */
public class EnvironmentCreator {
	private transient Logger log = LoggerFactory.getLogger(EnvironmentCreator.class);
	
	private static EnvironmentCreator instance;
	private final List<AbstractListener> listeners;
	private ICoordinator coordinator;
	private IQueryInterpreter queryInterpreter;
	private IDatabase[] databasePair;
	private ILockManager<Integer> lockManager;
	
	private static boolean dmqsResponse = false;
	private static boolean getHttpResponse = false;
	private AbstractNotificationManager<OperationResponse, String> dmqsNotificationManager;
	
	private EnvironmentCreator(CLI cli) {
		this.listeners = new ArrayList<AbstractListener>(cli.thread_no);
		for(int i=0; i<cli.thread_no; i++)
			listeners.add(ListenersFactory.createListener(ListenerNames.NEWKAFKA, cli));
		this.coordinator = CoordinatorsFactory.createCoordinator(CoordinatorNames.ZOOKEEPER, cli);
		this.queryInterpreter = QueryInterpretersFactory.createInterpreter(QueryInterpreterNames.METAMODEL);
		this.databasePair = DatabasesFactory.createDatabasePair(cli.dbName_src, cli.dbName_dst);
		this.lockManager = LockManagerFactory.createLockManager(LockManagerNames.ZOOKEEPER, cli);
		
		if(dmqsResponse){
			Map<String, String> options = new HashMap<String,String>(1);
			//telling the notification manager whether to expect responses to (n)acks
			options.put("getHTTPresponse", getHttpResponse+"");
			if(getHttpResponse)
				log.debug("telling the notification manager to expect responses to (n)acks");
			else
				log.debug("telling the notification manager NOT to expect responses to (n)acks");

			//getting the resolution service instance
			DiscoveryClient.init(cli.zkConnectString);
			try {
				@SuppressWarnings("resource")
				IResolutionService resolutionService = DiscoveryClient.getInstance();
				this.dmqsNotificationManager = 
						NotificationsFactory.createNotificationManager(
								NotificationManagerNames.DMQs, 
								resolutionService, 
								options);
			} catch (Exception e) {
				log.error("Error connecting to the service discovery system",e);
				System.exit(-1);
			}
		}
		
		for(AbstractListener listener : listeners){
			//injecting the dependencies
			listener.setCoordinator(this.coordinator);
			listener.setQueryInterpreter(this.queryInterpreter);
			listener.setDatabase(this.databasePair);
			listener.setEntriesLockManager(this.lockManager);
			listener.setDMQsNotificationManager(this.dmqsNotificationManager);
		}
	}
	
	public static EnvironmentCreator getInstance(CLI cli){
		//double locking mechanism
		if(instance == null){
			synchronized(EnvironmentCreator.class){
				if(instance == null){
					instance =  new EnvironmentCreator(cli);
				}
			}
		}
		return instance;
	}
	
	public static void init(boolean dmqsRes, boolean getHttpRes){
		dmqsResponse=dmqsRes;
		getHttpResponse=getHttpRes;
	}

	/**
	 * @return the listeners
	 */
	public List<AbstractListener> getListeners() {
		return listeners;
	}

	public void close() throws Exception {
		if(lockManager!=null){
			if(lockManager instanceof ZooKeeperLockManager){
				CloseableUtils.closeQuietly((ZooKeeperLockManager) lockManager);
			}
		}
		
		if(coordinator!=null){
			coordinator.disconnect();
		}
		
		if(dmqsResponse){
				CloseableUtils.closeQuietly(DiscoveryClient.getInstance());
		}
	}
}
