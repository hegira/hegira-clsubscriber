/**
 * 
 */
package it.polimi.hegira.discovery;

/**
 * @author Marco Scavuzzo
 *
 */
public interface IResolutionService {
	/**
	 * Resolves a given identifier into a proper connection string
	 * @param id The id of the resource to resolve
	 * @return The connection string
	 */
	public String resolve(String id);
}
