/**
 * 
 */
package it.polimi.hegira.discovery;

import java.io.Closeable;
import java.io.IOException;
import java.util.List;
import java.util.Scanner;

import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.CuratorFrameworkFactory;
import org.apache.curator.framework.imps.CuratorFrameworkState;
import org.apache.curator.retry.ExponentialBackoffRetry;
import org.apache.curator.utils.CloseableUtils;
import org.apache.curator.x.discovery.ServiceCache;
import org.apache.curator.x.discovery.ServiceDiscovery;
import org.apache.curator.x.discovery.ServiceDiscoveryBuilder;
import org.apache.curator.x.discovery.ServiceInstance;
import org.junit.runners.model.InitializationError;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import it.polimi.hegira.notifications.DMQsNotificationManager;
import it.polimi.hegira.notifications.OperationResponse;
import it.polimi.hegira.notifications.OperationResponse.OperationResult;
import it.polimi.hegira.notifications.OperationResponse.QueryType;

/**
 * @author Marco Scavuzzo
 *
 */
public class DiscoveryClient implements IResolutionService, Closeable {

	private transient static Logger log = LoggerFactory.getLogger(DiscoveryClient.class);
	private static DiscoveryClient instance;
	private static Object instance_lock = new Object();
	private static CuratorFramework client;
	
	private static final String PATH = "/hegira/discovery";
	private static final String serviceName = "kafkaProducers";
	private static String connectString;
	
	private ServiceDiscovery<String> serviceDiscovery;
	private ServiceCache<String> serviceCache;
	private static boolean serviceCacheStarted=false;
	
	private DiscoveryClient(){
		if(connectString==null){
			throw new IllegalStateException();
		}
		
		client = CuratorFrameworkFactory.newClient(connectString,
				new ExponentialBackoffRetry(1000, 3));
		serviceDiscovery = ServiceDiscoveryBuilder
				.builder(String.class)
				.client(client)
				.basePath(PATH)
				.build();
		
		serviceCache = serviceDiscovery.serviceCacheBuilder().name(serviceName).build();
	}
	
	public static DiscoveryClient getInstance() throws Exception{
		if(!isInitialized()){
			throw new InitializationError(DiscoveryClient.class.getCanonicalName()
					+" must first be initialized!");
		}
		
		synchronized(instance_lock){
			instance = instance==null ? new DiscoveryClient() : instance;
			if(!isConnected())
				instance.connect();
		}
		return instance;
	}
	
	public static synchronized void init(String connectString){
		if(connectString==null){
			throw new IllegalArgumentException();
		}
		DiscoveryClient.connectString = connectString;
	}
	
	private static boolean isInitialized(){
		return connectString!=null;
	}
	
	private static boolean isConnected(){
		if(client==null)
			throw new IllegalStateException("CuratorFrameworkClient is null!");
		
		return isCuratorConnected() && serviceCacheStarted;
	}
	
	private static boolean isCuratorConnected(){
		return client.getState().equals(CuratorFrameworkState.STARTED);
	}
	
	private void connect(){
		if(isConnected()){
			log.warn("{} - Already connected.",
					Thread.currentThread().getName());
			return;
		}
		
		while(!isCuratorConnected())
			client.start();
		
		try{
			serviceCache.start();
			serviceCacheStarted=true;
		}catch(Exception e){
			log.error("Cannot start ZooKeeper serviceCache for discovery",e);
			serviceCacheStarted=false;
		}
	}
	
	public void disconnect(){
		if(!isConnected())
			return;
		
		try {
			close();
			log.debug("{} - Disconnected.",
					Thread.currentThread().getName());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
	@Override
	public String resolve(String instanceId) {
		if(instanceId==null)
			throw new IllegalArgumentException("The operation Id cannot be null");
		if(!isCuratorConnected() || serviceDiscovery==null)
			return null;
		try {
			ServiceInstance<String> instance=null;
			if(serviceCacheStarted){
				//log.debug("Getting instances from cache");
				List<ServiceInstance<String>> instances = serviceCache.getInstances();
				for(ServiceInstance<String> ins : instances){
					if(ins.getId().equals(instanceId)){
						instance = ins;
						break;
					}
				}
			}else{
				//inefficient resolution. Prefer using the cache!
				log.debug("Getting instance from ZooKeeper directly");
				instance 
					= serviceDiscovery.queryForInstance(serviceName, instanceId);
			}
			
			if(instance!=null)
				return instance.getAddress()+":"+instance.getPort();
		} catch (Exception e) {
			log.error("Error while finding KafkaProducers",e);
		}
		log.error("KafkaProducer not found!");
		return null;
	}

	@Override
	public void close() throws IOException {
		if(serviceCacheStarted){
			CloseableUtils.closeQuietly(serviceCache);
			serviceCacheStarted=false;
		}
		CloseableUtils.closeQuietly(serviceDiscovery);
		CloseableUtils.closeQuietly(client);
	}
	
	public static void main(String[] args) {
		DiscoveryClient.init("localhost:2181");
		@SuppressWarnings("resource")
		Scanner scanner = null;
		try {
			DiscoveryClient.getInstance();
			scanner = new Scanner(System.in);
			System.out.print("Insert instance ID: ");
			String instanceId = scanner.nextLine();
			if(instanceId!=null) {
				String resolvedIp = DiscoveryClient.getInstance().resolve(instanceId);
				System.out.println("\nResolved Id: "+resolvedIp);
				
				DMQsNotificationManager dmqsNm = new DMQsNotificationManager(DiscoveryClient.getInstance(), true);
				System.out.print("Insert operation ID: ");
				String operationId = scanner.nextLine();
				if(operationId!=null){
					dmqsNm.sendAck(instanceId, operationId, 
							new OperationResponse("pk", "tblName", 
									true, true, 
									QueryType.INSERT, OperationResult.SUCCESS, System.currentTimeMillis()));
					Thread.sleep(1500);
				}else{
					log.error("Operation Id cannot be null");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				DiscoveryClient.getInstance().close();
				if(scanner!=null)
					scanner.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
}
