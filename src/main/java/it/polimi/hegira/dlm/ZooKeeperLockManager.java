/**
 * 
 */
package it.polimi.hegira.dlm;


import it.polimi.hegira.exceptions.CacheLockException;
import it.polimi.hegira.utils.DlmCache;
import it.polimi.hegira.zkWrapper.dlm.LockManager;

import java.io.Closeable;
import java.io.IOException;
import java.util.AbstractMap.SimpleEntry;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.recipes.locks.InterProcessLock;
import org.apache.curator.framework.recipes.locks.InterProcessMultiLock;
import org.apache.curator.framework.recipes.locks.InterProcessMutex;
import org.apache.curator.framework.state.ConnectionState;
import org.apache.curator.framework.state.ConnectionStateListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Marco Scavuzzo
 *
 */
public class ZooKeeperLockManager implements ILockManager<Integer>, Closeable {
	private static final transient Logger log = LoggerFactory.getLogger(ZooKeeperLockManager.class);
	private String connectionString;
	private String dataMigrationId;
	private DlmCache<String, InterProcessLock> entriesLockCache;
	private Boolean paused = false;

	public ZooKeeperLockManager(String connectionString, String dataMigrationId) {
		this.connectionString = connectionString;
		this.dataMigrationId = dataMigrationId;
		entriesLockCache = new DlmCache<String, InterProcessLock>(500);
		//Listens for ZooKeeper connection loss or other changes
		LockManager.getInstance(connectionString, dataMigrationId).addConnectionStateListener(
				new DlmConnectionListener(this));
	}
	
	
	@Override
	public synchronized boolean acquireLock(String tableName, Integer entryToLock, long time,
			TimeUnit unit) {
		if(entryToLock==null)
			throw new IllegalArgumentException();
		needTosleep();
		InterProcessMutex mutex = LockManager.getInstance(connectionString, dataMigrationId)
			.acquireLock(tableName, entryToLock, time, unit);
		try{
			entriesLockCache.putLock(tableName+entryToLock, mutex);
		}catch(IllegalArgumentException ex){
			return false;
		}
		return mutex!=null;
	}
	
	@Override
	public synchronized boolean acquireLocks(String txId, HashMap<String, SimpleEntry<String,Integer>> tbls_Ids, long time, 
			TimeUnit unit){
		if(tbls_Ids==null || txId==null)
			throw new IllegalArgumentException();
		needTosleep();
		InterProcessMultiLock multiLock = LockManager.getInstance(connectionString, dataMigrationId)
			.acquireLocks(txId, tbls_Ids, time, unit);
		try{
			entriesLockCache.putLock("TX"+txId, multiLock);
		}catch(IllegalArgumentException ex){
			return false;
		}
		return multiLock!=null;
	}

	@Override
	public synchronized boolean releaseLock(String tableName, Integer lockedEntry) throws CacheLockException {
		if(lockedEntry==null)
			throw new IllegalArgumentException();
		needTosleep();
		if(!entriesLockCache.containsLock(tableName+lockedEntry))
			throw new CacheLockException("Entry "+lockedEntry+" is not present in the cache");
		boolean released = LockManager.getInstance(connectionString, dataMigrationId)
			.releaseGenericLock(entriesLockCache.getLock(tableName+lockedEntry));
		boolean removed = entriesLockCache.removeLock(tableName+lockedEntry)!=null ? true : false;
		return released && removed;
	}
	
	@Override
	public synchronized boolean releaseLocks(String txId) throws CacheLockException{
		if(txId==null)
			throw new IllegalArgumentException();
		needTosleep();
		if(!entriesLockCache.containsLock("TX"+txId))
			throw new CacheLockException("Transaction "+txId+" is not present in the cache");
		boolean released = LockManager.getInstance(connectionString, dataMigrationId)
			.releaseGenericLock(entriesLockCache.getLock("TX"+txId));
		boolean removed = entriesLockCache.removeLock("TX"+txId)!=null ? true : false;
		return released && removed;
	}

	private synchronized void setPaused(boolean isPaused){
		this.paused = isPaused;
	}
	
	/**
	 * Typically used when the connection to ZooKeeper is lost, in order to prevent inconsistencies.
	 */
	private void needTosleep(){
		long i = 1;
		while(paused){
			try {
				wait(200);
				if(i%10==0) log.info("{} - Lock manager is paused!",
						Thread.currentThread().getName());
			} catch (InterruptedException e) {}
			if(i==Long.MAX_VALUE) i=0;
			i++;
		}
	}

	private class DlmConnectionListener implements ConnectionStateListener  {
		
		private ZooKeeperLockManager instance;
		
		public DlmConnectionListener(ZooKeeperLockManager instance){
			this.instance = instance;
		}

		@Override
		public void stateChanged(CuratorFramework client, ConnectionState newState) {
			switch (newState) {
		        case SUSPENDED:
		            log.error("Connection suspended");
		            setPaused(true);
		            break;
		        case LOST:
		            log.error("Connection lost");
		            //setStopped(true);
		            setPaused(true);
		            break;
		        default:
		            log.info("Connection event: {}", newState.name());
		            if(paused){
		            		reAcquireLocks();
		            		unpauseWorkers();
		            }
		            break;
			}
			
		}
		
		private void unpauseWorkers(){
			setPaused(false);
			try {
				if(instance!=null)
					instance.notifyAll();
			} catch(Exception e){}
		}
		
		private synchronized void reAcquireLocks(){
			for(String key : entriesLockCache.getKeySet()){
				try {
					entriesLockCache.getLock(key).acquire();
				} catch (Exception e) {
					log.error("{} - Unable to re-acquire lock {}",
							Thread.currentThread().getName(), key);
				}
			}
		}
		
	}

	@Override
	public void close() throws IOException {
		LockManager.getInstance(connectionString, dataMigrationId).disconnect(dataMigrationId);
	}
}
