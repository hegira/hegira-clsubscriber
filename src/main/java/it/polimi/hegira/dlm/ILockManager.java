/**
 * 
 */
package it.polimi.hegira.dlm;

import it.polimi.hegira.exceptions.CacheLockException;

import java.util.AbstractMap.SimpleEntry;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

/**
 * @author Marco Scavuzzo
 *
 */
public interface ILockManager<T> {
	/**
	 * Acquires a lock on the entryToLock
	 * @param tableName
	 * @param entryToLock
	 * @param time
	 * @param unit
	 * @return true if acquired
	 */
	public boolean acquireLock(String tableName, T entryToLock, long time, TimeUnit unit);
	
	/**
	 * Tries to atomically acquire locks for the provided resources. Either all or none of them are acquired.
	 * @param txId The id of the transaction
	 * @param tbls_Ids A Map whose key is the concatenation of the tableName and of the resourceId and whose value contains a {@link SimpleEntry} having as Key the table name and as value the resource Id (sequentialId) to lock
	 * @param time The amount of time to wait for acquiring the exclusive lock.
	 * @param unit The measurement unit for the time parameter.
	 * @return true if acquired
	 */
	public boolean acquireLocks(String txId, HashMap<String, SimpleEntry<String,T>> tbls_Ids, long time, TimeUnit unit);
	
	/**
	 * Releases a lock, if already acquired, on a lockedEntry
	 * @param lockedEntry The locked entry
	 * @return true if released
	 * @throws CacheLockException 
	 */
	public boolean releaseLock(String tableName, T lockedEntry) throws CacheLockException;
	
	/**
	 * Releases locks on multiple entries that were acquired atomically
	 * @param txId The transaction identifier
	 * @return true if released
	 * @throws CacheLockException 
	 */
	public boolean releaseLocks(String txId) throws CacheLockException;
}
