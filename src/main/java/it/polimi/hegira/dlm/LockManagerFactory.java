/**
 * 
 */
package it.polimi.hegira.dlm;

import it.polimi.hegira.utils.CLI;

/**
 * @author Marco Scavuzzo
 *
 */
public class LockManagerFactory {
	@SuppressWarnings("rawtypes")
	public static ILockManager createLockManager(LockManagerNames name,
			CLI cli){
		switch (name) {
		case ZOOKEEPER:
			return new ZooKeeperLockManager(cli.zkConnectString, ""+cli.migration_id);
		default:
			return null;
		}
	}
	
	public enum LockManagerNames {
		ZOOKEEPER;
	}
}
