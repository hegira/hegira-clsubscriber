/**
 * 
 */
package it.polimi.hegira.notifications;

import it.polimi.hegira.discovery.IResolutionService;

/**
 * @author Marco Scavuzzo
 *
 */
public abstract class AbstractNotificationManager<T,I> {
	protected IResolutionService resolutionService;
	
	public AbstractNotificationManager(IResolutionService resolutionService){
		this.resolutionService = resolutionService;
	}
	
	public abstract void sendAck(String serviceId, I operationId, T message);
	public abstract void sendNack(String serviceId, I operationId, T message);
}
