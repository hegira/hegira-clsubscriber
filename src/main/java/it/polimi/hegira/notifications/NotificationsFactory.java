/**
 * 
 */
package it.polimi.hegira.notifications;

import java.util.Map;

import it.polimi.hegira.discovery.IResolutionService;

/**
 * @author Marco Scavuzzo
 *
 */
public class NotificationsFactory<T, I> {
	@SuppressWarnings("unchecked")
	public static <T, I> AbstractNotificationManager<T, I> createNotificationManager(
			NotificationManagerNames nmId,
			IResolutionService resolutionService,
			Map<String, String> options){
		switch(nmId){
			case DMQs:
				if(options!=null && options.get("getHTTPresponse")!=null){
					String resp = options.get("getHTTPresponse");
					return (AbstractNotificationManager<T, I>) 
							new DMQsNotificationManager(resolutionService,
									resp.toLowerCase().equals("true") ? true : false);
				}else{
					return (AbstractNotificationManager<T, I>) 
						new DMQsNotificationManager(resolutionService, false);
				}
			default:
				return null;
		}
		
	}

	public enum NotificationManagerNames {
		DMQs
	}
}
