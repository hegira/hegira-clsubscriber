package it.polimi.hegira.notifications;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jcabi.aspects.Async;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;

import it.polimi.hegira.discovery.IResolutionService;
import it.polimi.hegira.notifications.OperationResponse.OperationResult;

/**
 * @author Marco Scavuzzo
 *
 */
public class DMQsNotificationManager extends AbstractNotificationManager<OperationResponse, String>{
	private static final transient Logger log = LoggerFactory.getLogger(DMQsNotificationManager.class);
	private boolean getHTTPresponse;
	
	public DMQsNotificationManager(IResolutionService resolutionService, boolean getHTTPresponse) {
		super(resolutionService);
		this.getHTTPresponse = getHTTPresponse;
	}

	@Override
	public void sendAck(String serviceId, String operationId, OperationResponse message) {
		send(serviceId, operationId, message);
	}

	@Override
	public void sendNack(String serviceId, String operationId, OperationResponse message) {
		send(serviceId, operationId, message);
	}

	
	private void send(String serviceId, String operationId, OperationResponse message){
		if(resolutionService==null){
			log.error("The resolution service cannot be null!");
			throw new IllegalArgumentException("The resolution service cannot be null!");
		}
		
		if(serviceId==null || operationId==null || message==null){
			log.error("Arguments cannot be null!");
			throw new IllegalArgumentException("Arguments cannot be null!");
		}
		
		String connectString = getConnectString(serviceId);
		StringBuilder sb = new StringBuilder("http://"+connectString);
		sb.append("/response");
		
		if(message.getResult().equals(OperationResult.SUCCESS))
			sb.append("/ack");
		else
			sb.append("/nack");
		
		Client client = Client.create();
		
		Future<ClientResponse> asyncResponse = client.asyncResource(sb.toString())
			.queryParam("opId", operationId)
			.queryParam("pk", message.getPrimaryKey())
			.queryParam("tblName", message.getTblName())
			.queryParam("src", message.isSRC()+"")
			.queryParam("dst", message.isDST()+"")
			.queryParam("opType", message.getqType().name())
			.queryParam("timestamp", message.getTimestamp()+"")
			.type(MediaType.APPLICATION_JSON)
			.post(ClientResponse.class);
		log.debug("{} - Sent notification for opId {}/{}/{}", Thread.currentThread().getName(), 
				operationId, message.getPrimaryKey(), message.getTimestamp());
		if(getHTTPresponse)
			logResponseAsync(asyncResponse, serviceId, operationId, message);
	}
	
	@Async
	private void logResponseAsync(Future<ClientResponse> asyncResponse, String serviceId, String operationId, OperationResponse message){
		long timeout=2;
		TimeUnit unit=TimeUnit.SECONDS;
		try {
			ClientResponse response = asyncResponse.get(timeout, unit);
			if(response.getStatus()<200 || response.getStatus()>=205){
				log.error("{} - Received wrong response ({}) from HTTP server for operation {}/{}", 
						Thread.currentThread().getName(),
						response.getStatus(),
						operationId, message.getPrimaryKey());
				throw new RuntimeException(Thread.currentThread().getName()+" - Failed: HTTP error code: "
					     + response.getStatus()
					     +" for "+ (message.getResult().equals(OperationResult.SUCCESS) ? "ACK" : "NACK")
					     +" notification to "+serviceId+" for operation "+operationId+"/"+message.getPrimaryKey());
			}
			
			String entity = response.getEntity(String.class);
			log.info("{} - Got response: {} from {} for opId: {}/{} to {} message", 
					Thread.currentThread().getName(),
					entity, 
					serviceId,
					operationId, message.getPrimaryKey(),
					message.getResult().equals(OperationResult.SUCCESS) ? "ACK" : "NACK");
		} catch (InterruptedException | ExecutionException e) {
			log.error("Unable to get response from {} to {} notification for operation {}/{}",
					serviceId,
					message.getResult().equals(OperationResult.SUCCESS) ? "ACK" : "NACK",
					operationId, message.getPrimaryKey());
		} catch (TimeoutException e) {
			log.error("HTTP response, from {} to {} DMQ notification for operation {}/{}, timed-out ({}{}) ",
					serviceId,
					message.getResult().equals(OperationResult.SUCCESS) ? "ACK" : "NACK",
					operationId, message.getPrimaryKey(),
					timeout,
					unit.toString());
		}
	}
	
	private String getConnectString(final String serviceId){
		String connectString = resolutionService.resolve(serviceId);
		if(connectString==null){
			log.error("Error resolving the serviceId: {}. The resolved id cannot be null!",
					serviceId);
			throw new IllegalArgumentException("The resolution service cannot be null!");
		}
		return connectString;
	}
}
