package it.polimi.hegira.exceptions;

public class CacheLockException extends Exception {

	private static final long serialVersionUID = -4435431063538681428L;

	public CacheLockException() {
		super();
	}

	public CacheLockException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public CacheLockException(String message, Throwable cause) {
		super(message, cause);
	}

	public CacheLockException(String message) {
		super(message);
	}

	public CacheLockException(Throwable cause) {
		super(cause);
	}
	
}
