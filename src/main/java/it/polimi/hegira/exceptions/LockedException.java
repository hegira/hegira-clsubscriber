/**
 * 
 */
package it.polimi.hegira.exceptions;

/**
 * Exception thrown in case of a lock in a ZK object.
 * @author Marco Scavuzzo
 *
 */
public class LockedException extends Exception {

	/**
	 * 
	 */
	public LockedException() {
	}

	/**
	 * @param message
	 */
	public LockedException(String message) {
		super(message);
	}

	/**
	 * @param cause
	 */
	public LockedException(Throwable cause) {
		super(cause);
	}

	/**
	 * @param message
	 * @param cause
	 */
	public LockedException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * @param message
	 * @param cause
	 * @param enableSuppression
	 * @param writableStackTrace
	 */
	public LockedException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

}
