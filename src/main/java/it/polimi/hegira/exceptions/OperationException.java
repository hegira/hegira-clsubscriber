/**
 * 
 */
package it.polimi.hegira.exceptions;

import it.polimi.hegira.models.MetamodelOperationWrapper;
import it.polimi.hegira.persistence.IDatabase;

/**
 * Thrown when a operation on a database could not be performed
 * @author Marco Scavuzzo
 *
 */
public class OperationException extends Exception {
	private static final long serialVersionUID = 7446282968494473114L;
	private String databaseName;
	private MetamodelOperationWrapper operation;
	
	public OperationException(IDatabase db, MetamodelOperationWrapper op){
		super();
		if(db!=null)
			this.databaseName=db.getName();
		else
			this.databaseName="Unspecified";
		
		this.operation=op;
	}
	
	public OperationException(IDatabase db){
		super();
		if(db!=null)
			this.databaseName=db.getName();
		else
			this.databaseName="Unspecified";
	}
	
	public void setOperation(MetamodelOperationWrapper op){
		this.operation = op;
	}
	
	public String getDatabaseName(){
		return databaseName;
	}
	
	public MetamodelOperationWrapper getOperation(){
		return operation;
	}
	
	@Override
	public String toString() {
		StringBuilder msg = new StringBuilder("Database: "+databaseName+" ");
		if(operation!=null){
			msg.append("Operation: "+operation.getOperation().name());
			msg.append(" PK: "+operation.getMmEntity().columnFamilies.get(0));
			msg.append("/"+operation.getMmEntity().rowKey);
		}
		return msg.toString()+"\n"+super.toString();
	}
}
