/**
 * 
 */
package it.polimi.hegira.exceptions;

/**
 * @author Marco Scavuzzo
 *
 */
public class UnsupportedQueryException extends Exception {

	/**
	 * 
	 */
	public UnsupportedQueryException() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param message
	 */
	public UnsupportedQueryException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param cause
	 */
	public UnsupportedQueryException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param message
	 * @param cause
	 */
	public UnsupportedQueryException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param message
	 * @param cause
	 * @param enableSuppression
	 * @param writableStackTrace
	 */
	public UnsupportedQueryException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

}
