/**
 * 
 */
package it.polimi.hegira.queries;

import java.io.IOException;

import org.apache.log4j.Logger;

import it.polimi.hegira.hegira_metamodel.Metamodel;
import it.polimi.hegira.hegira_metamodel.sync.MsgWrapper;
import it.polimi.hegira.utils.DefaultSerializer;

/**
 * @author Marco Scavuzzo
 *
 */
public class MetamodelInterpreter implements IQueryInterpreter {
	private static Logger log = Logger.getLogger(MetamodelInterpreter.class);
	/* (non-Javadoc)
	 * @see it.polimi.hegira.queries.IQueryInterpreter#parseQuery(java.lang.String)
	 */
	@Override
	public Metamodel parseQuery(String query) {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see it.polimi.hegira.queries.IQueryInterpreter#createMessage(java.lang.Object)
	 */
	@Override
	public MsgWrapper createMessage(Object query) {
		if(query instanceof MsgWrapper){
			return (MsgWrapper) query;
		} else if(query instanceof byte[]){
			//log.debug("Got a byte[]. Deserializing....");
			try {
				return (MsgWrapper) DefaultSerializer.deserialize((byte[]) query);
			} catch (ClassNotFoundException | IOException e) {
				log.error("Couldn't deserialize the given query!!", e);
			}
		}
		log.error("unrecognized query, returning null");
		return null;
	}

}
