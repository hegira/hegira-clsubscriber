/**
 * 
 */
package it.polimi.hegira.queries;

import java.io.Serializable;

import it.polimi.hegira.hegira_metamodel.Metamodel;

/**
 * 
 * @author Marco Scavuzzo
 *
 */
public class Message implements Serializable{
	private static final long serialVersionUID = 1L;
	private Integer primaryKey;
	private int vdpId;
	
	private Metamodel metamodel;
	private QueryType type;
	
	public Message(Metamodel metamodel, QueryType type){
		this.primaryKey = Integer.parseInt(metamodel.getRowKey());
		this.metamodel = metamodel;
		this.type = type;
	}
	
	public Message(String oql){
		
	}

	/**
	 * @return the primaryKey
	 */
	public Integer getPrimaryKey() {
		return primaryKey;
	}

	/**
	 * @return the vdpId
	 */
	public int getVdpId() {
		return vdpId;
	}

	/**
	 * @return the metamodel
	 */
	public Metamodel getMetamodel() {
		return metamodel;
	}

	/**
	 * @return the type
	 */
	public QueryType getType() {
		return type;
	}
}
