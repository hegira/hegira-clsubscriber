/**
 * 
 */
package it.polimi.hegira.queries;

/**
 * Factory class which creates the proper commit-log query interpreter.
 * @author Marco Scavuzzo
 *
 */
public class QueryInterpretersFactory {
	public static IQueryInterpreter createInterpreter(QueryInterpreterNames interpreterId){
		switch(interpreterId){
			case OQL:
				//TODO: fix when the proper constructors will be created
				return new OQLInterpreter();
			case METAMODEL:
				return new MetamodelInterpreter();
			default:
				return null;
		}
	}
	
	public enum QueryInterpreterNames {
		OQL, METAMODEL
	}
}
