package it.polimi.hegira.queries;

import it.polimi.hegira.hegira_metamodel.Metamodel;
import it.polimi.hegira.hegira_metamodel.sync.MsgWrapper;

/**
 * 
 * @author Marco Scavuzzo
 *
 */
public interface IQueryInterpreter {
	/**
	 * Receives a query (as extracted from the commit-log) and parses it to produce
	 * the corresponding Metamodel object.
	 * @param query The query, as extracted from the commit-log.
	 * @return a Metamodel object.
	 */
	public Metamodel parseQuery(String query);
	
	/**
	 * Creates a {@link MsgWrapper} object, easily parsable by the generic Coordinator. 
	 * @param query
	 * @return
	 */
	public MsgWrapper createMessage(Object query);
}
