/**
 * 
 */
package it.polimi.hegira.queries;

import it.polimi.hegira.hegira_metamodel.Metamodel;
import it.polimi.hegira.hegira_metamodel.sync.MsgWrapper;
import it.polimi.hegira.queries.QueryType;

/**
 * @author Marco Scavuzzo
 *
 */
public class OQLInterpreter implements IQueryInterpreter {

	/**
	 * 
	 */
	public OQLInterpreter() {
		// TODO Auto-generated constructor stub
	}

	/* (non-Javadoc)
	 * @see it.polimi.hegira.queries.IQueryInterpreter#parseQuery(java.lang.String)
	 */
	@Override
	public Metamodel parseQuery(String query) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public MsgWrapper createMessage(Object query) {
		// TODO Fake implementation, change
		if(String.class.isInstance(query))
			return new MsgWrapper();
		else 
			return null;
	}

}
