package it.polimi.hegira.coordination;

import static org.junit.Assert.*;
import it.polimi.hegira.zkWrapper.MigrationStatus;
import it.polimi.hegira.zkWrapper.MigrationStatus.VDPstatus;
import it.polimi.hegira.zkWrapper.exception.OutOfSnapshotException;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * @author Marco Scavuzzo
 *
 */
public class VDPcacheTest {

	VDPcache cache;
	
	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		cache = new VDPcache();
	}

	/**
	 * Test method for {@link it.polimi.hegira.coordination.VDPcache#put(java.lang.String, it.polimi.hegira.zkWrapper.MigrationStatus)}.
	 */
	@Test
	public void testPutStringMigrationStatus() {
		int  no_vdps = 5;
		String tblName = "prova";
		
		assertNull(putMigrationStatuses(tblName));
		try {
			assertNotNull(cache.get(tblName, no_vdps-1));
		} catch (OutOfSnapshotException e) {
			fail("OutOfSnapshotException");
		}
		
	}
	private MigrationStatus putMigrationStatuses(String tableName){
		int lastSeqNr = 50, no_vdps = 5;
		MigrationStatus ms = new MigrationStatus(lastSeqNr, no_vdps);
		return cache.put(tableName, ms);
	}

	/**
	 * Test method for {@link it.polimi.hegira.coordination.VDPcache#put(java.lang.String, int, it.polimi.hegira.zkWrapper.MigrationStatus.VDPstatus)}.
	 */
	@Test
	public void testPutStringIntVDPstatus() {
		int  no_vdps = 5;
		String tblName = "prova1";
		putMigrationStatuses(tblName);
		//previous status should be not migrated
		assertEquals(VDPstatus.NOT_MIGRATED, cache.put(tblName, no_vdps-1, VDPstatus.MIGRATED));
		
		//new status should be retrieved
		try {
			assertEquals(VDPstatus.MIGRATED, cache.get(tblName, no_vdps-1));
		} catch (OutOfSnapshotException e) {
			fail("OutOfSnapshotException");
		}
	}

	@Test(expected=NullPointerException.class)
	public void testUpdateUncachedVDPstatus() {
		int vdpId = 0;
		cache.put("prova4", vdpId , VDPstatus.UNDER_MIGRATION);
	}

	/**
	 * Test method for {@link it.polimi.hegira.coordination.VDPcache#isMigrated(int, java.lang.String)}.
	 */
	@Test
	public void testIsMigrated() {
		int  no_vdps = 5;
		String tblName = "prova1";
		testPutStringIntVDPstatus();
		try {
			assertTrue(cache.isMigrated(no_vdps-1, tblName));
		} catch (OutOfSnapshotException e) {
			fail("OutOfSnapshotException");
		}
	}
	
	@Test(expected=NullPointerException.class)
	public void testSimpleEviction(){
		VDPcache cache2 = new VDPcache(3, 1, 3);
		cache2.put("prova1", new MigrationStatus(10, 1));
		cache2.put("prova2", new MigrationStatus(20, 1));
		cache2.put("prova3", new MigrationStatus(30, 1));
		cache2.put("prova4", new MigrationStatus(40, 1));
		
		try {
			//it should be there
			assertNotNull(cache2.get("prova2", 0));
			
			//should generate a cache miss, i.e., NullPointerException
			cache2.get("prova1", 0);
		} catch (OutOfSnapshotException e) {
			e.printStackTrace();
		}
	}

	@Test(expected=NullPointerException.class)
	public void testLRUEviction() throws OutOfSnapshotException{
		VDPcache cache2 = new VDPcache(3, 1, 3);
		cache2.put("prova1", new MigrationStatus(10, 1));
		cache2.put("prova2", new MigrationStatus(20, 1));
		cache2.put("prova3", new MigrationStatus(30, 1));
		assertNotNull(cache2.get("prova1", 0));
		cache2.put("prova4", new MigrationStatus(40, 1));
		
		//should generate a cache miss, i.e., NullPointerException
		//because "prova2" was the least recently accessed entry in the cache
		cache2.get("prova2", 0);
		
	}
}
